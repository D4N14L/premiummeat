<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use URL;
use Auth;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */


    // protected $redirectTo = '/home';

    protected function authenticated(Request $request, $user)
    {

      if(session()->has('atTheCheckout'))
      {
        session()->forget('atTheCheckout');
        return redirect()->route('payment.pay.after.login');
      }
      if($user->hasRole('admin'))
      {
        Auth::logout();
        return redirect()->route('login')->withErrors(['email' => 'wrong credentials','password' => 'wrong credentials']);
      }
      if(count(session()->get('cart')) > 0 )
      {
        return redirect()->route('user.getUserManager');
      }
      else
      {
        return redirect(URL::to('/#products'));

      }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Area;
use App\Suburb;
use Toastr;
class ServiceAreaController extends Controller
{
    public function index(){
      return view('admin.areas.index');
    }
    public function addCity(Request $request){
      $validator = Validator::make($request->toArray(),[
        'cityName' => 'required|min:2|unique:areas'
      ],[
        'cityName.required' => "City Name Is Required",
        'cityName.min' => "City Name Must Be Greater Than 2",
        'cityName.unique' => "City With This Name Already Exists",
      ]);
      if($validator->fails()){
        return response()->json(['status' => 500,'errMsg' => $validator->messages()->first()]);
      }else {
        Area::unguard();
        $area = Area::create($request->only(['cityName']));
        Area::reguard();
        if($area){
          return response()->json(['status' => 200,'city' => $area]);

        }else{
          return response()->json(['status' => 500,'errMsg' => 'Unexpected Error Occured While Saving City']);
        }
      }
    }

    public function addSuburb(Request $request){
      $validator = Validator::make($request->all(),[
        'suburbName' => 'required|min:2|unique:suburbs',
        'areas_id' => 'required'
      ],[
        'suburbName.required' => "Suburb Name Is Required",
        'suburbName.min' => "Suburb Name Must Be Greater Than 2",
        'suburbName.unique' => "Suburb With This Name Already Exists",
      ]);

      if($validator->fails()){
        return response()->json(['status' => 500,'errMsg' => $validator->messages()->first()]);
      }else {

        if($request->has('areas_id')){
          if($request->areas_id <=0){
            return response()->json(['status' => 500,'errMsg' => 'Please Select City Name']);
          }
        }else {
          return response()->json(['status' => 500,'errMsg' => 'Please Select City Name']);

        }
        Suburb::unguard();
        $suburb = Suburb::create($request->only(['areas_id','suburbName']));
        Suburb::reguard();
        if($suburb){
            return response()->json(['status' => 200,'suburb' => $suburb]);
        }else {
          return response()->json(['status' => 500,'errMsg' => 'Unexpected Error Occured While Saving Suburb']);
        }

      }
    }
    public function deleteArea($id){
      $area = Area::findOrFail($id);
      Suburb::where('areas_id','=',$area->id)->delete();
      $area->delete();
      Toastr::success('City Deleted Successfully');
      return redirect()->back();
    }

    public function updateArea(Request $request){
      try {
        $area = Area::findOrFail($request->area_id);
        $area->cityName = $request->areaName;
        $area->save();
        Toastr::success('City Name Updated Successfully');

      } catch (\Exception $e) {
        Toastr::error('An Unexpected Error Occured While Updating City Name');
      }
      return redirect()->back();

    }

    public function deleteSuburb($id){
      try {
        $suburb = Suburb::findOrFail($id);
        $suburb->delete();
        Toastr::success('Suburb Deleted Successfully');
      } catch (\Exception $e) {
        Toastr::error('An Unexpected Error Occured While Deleting Suburbs');

      }
      return redirect()->back();

    }

    public function updateSuburb(Request $request){
      if($request->has('suburbCityNames')){
        if($request->suburbCityNames <= 0) {
          Toastr::error('Please Select City In Order To Update ');
          return redirect()->back();
        }
      }else {
        Toastr::error('Please Select City In Order To Update ');
        return redirect()->back();
      }
      try {
        $suburb = \App\Suburb::findOrFail($request->suburub_id);
        $suburb->suburbName =  $request->suburbName;
        $suburb->areas_id = $request->suburbCityNames;
        $suburb->save();
        Toastr::success('Suburb Updated Successfully');
      } catch (\Exception $e) {
        Toastr::error('An Unexpected Error Occured While Updating Suburbs');

      }






      return redirect()->back();





    }





}

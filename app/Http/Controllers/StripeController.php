<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\OrderInfo;
use App\Product;
use Auth;
use Toastr;
use Cartalyst\Stripe\Stripe;
class StripeController extends Controller
{
      //Payment From Request

      public function getPayForm(Request $request)
      {
        if(!$request->has('timeslot')){
          Toastr::error('<b style="word-wrap:break-word;font-size:12px">Please, Select Timeslot</b>');
          session()->flash('failed','<b style="word-wrap:break-word;font-size:12px">Select Timeslot</b>');
          return redirect()->back();
        }else {
          session()->put('current_timeslot_for_order',$request->timeslot);
        }

        if(!$request->has('orderDate')){
          Toastr::error('<b style="word-wrap:break-word;font-size:12px">Please, Select Date to Deliver</b>');
          session()->flash('failed','<b style="word-wrap:break-word;font-size:12px">Please, Select Date to Deliver</b>');
          return redirect()->back();
        }else {
          session()->put('orderDate',$request->orderDate);
        }


        $total = $request->only('total')['total'];

        session()->put('total',(float) $total);
        session()->put('delivery',(int) $request->delivery);
          if(Auth::guest())
          {
            session()->flash('failed','<b style="word-wrap:break-word;font-size:12px">Please login or register for payment</b>');
            session()->put('atTheCheckout',true);
            return redirect()->route('login');
          }
          return view('orders.paymentForm');

      }

      public function getPayFormAfterLogin()
      {
        return view('orders.paymentForm');
      }

      //Payment Request From Stipe Api

      public function paymentRequest(Request $request)
      {
        try {
          $stripe = new Stripe('sk_test_zRfEiWeHdZa5kaw2qtRJcjLJ');
          $card = str_replace(' ','',(string) $request->number);
          $token = $stripe->tokens()->create([
            'card' => [
              'number' => $card ,
              'exp_month' => (int)explode('/',$request->expiry)[0],
              'exp_year' => (int)explode('/',$request->expiry)[1],
              'cvc' => $request->cvc,
            ],
          ]);
          if(!isset($token['id']))
          {
            session()->flash('failed','credit card error detected, please try again');
            return redirect()->route('checkout');
          }
          $customer_id = "";
          if(Auth::user()->isCustomer == 0)
          {
            $customer = $stripe->customers()->create([
              'email' => Auth::user()->email,
              'source' => $token['id']
            ]);
            $customer_id = $customer['id'];
            $user = \App\User::find(Auth::user()->id);
            $user->isCustomer = 1;
            $user->stripeCustomerID = $customer_id;
            $user->save();
          }
          // $charge = $stripe->charges()->create([
          //     'customer' => $customer_id,
          //     'currency' => 'NZD',
          //     'amount' => (float) session()->get('total'),
          //     'description' => 'Payment was done by the cusomer '.Auth::user()->name.' and email address is '.Auth::user()->email.'.who belongs to.'.Auth::user()->address.' . You can contact him here. '.Auth::user()->landline.'.',
          // ]);
            $order = new Order();
            $order->customer_id = Auth::user()->id;
            $order->order_status = 0;
            $order->timeslotsSelection = session()->get('current_timeslot_for_order');
            $order->order_date = session()->get('orderDate');
            $order->suburb = \App\Suburb::where('id','=',$request->suburb_id)->first()->suburbName;
            $order->total_amount = (float) session()->get('total');
            if($request->has('streetaddress'))
            {
              $order->address = $request->streetaddress;
            }
            if($request->has('city')){
              $order->city = \App\Area::where('id','=',\App\Suburb::where('id','=',$request->suburb_id)->first()->areas_id)->first()->name;
            }
            $order->isDelivery = session()->get('delivery');
            if($request->has('zipcode'))
            {
              $order->zipcode = $request->zipcode;
            }

            $order->save();

            foreach (session()->get('cart') as $item) {
              $orderInfo = new OrderInfo();
              $orderInfo->product_id = $item[0];
              $orderInfo->order_id = $order->id;
              $orderInfo->amount = $item['priceMade'];
              $orderInfo->quantity = (float) $item['quantity'];
              $orderInfo->service_type_option =  json_encode($item['serviceOption']);
              $orderInfo->additional_information = $item['description'];
              $orderInfo->save();
            }
            session()->forget('cart');
            session()->forget('current_timeslot_for_order');
            session()->forget('delivery');
            session()->forget('total');
            session()->flash('success',"Your Order Has Been Placed,  &nbsp;Thanks For Dropping By!");
            return redirect()->to('/#products');

          }

       catch (\Exception $e) {
            session()->flash('failed',$e->getMessage());
            return redirect()->route('checkout');
        } catch(\Cartalyst\Stripe\Exception\CardErrorException $e){
          session()->flash('failed',"Unable to find card for the current customer");
          return redirect()->route('checkout');
        } catch(\Cartalyst\Stripe\Exception\MissingParameterException $e){
          session()->flash('failed',$e->getMessage());
          return redirect()->route('checkout');
        }
        }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
class InventoryController extends Controller
{
    public function browse()
    {
        $products =   Product::orderBy('id','desc')->paginate(10);
        return view('admin.inventory.browse')->with('products',$products);
    }

    public function addQuantity(Request $request)
    {
      $product = Product::findOrFail($request->product_id);
      $prevQuantity = $product->inventory->quantity;
      $newQauntity = (int)$prevQuantity + (int)$request->value;
      $product->inventory->quantity = $newQauntity;
      $product->inventory->timestamps = false;
      $product->inventory->save();
      $remaining_quantity = $product->inventory->quantity - $product->inventory->sold;
      return response()->json(['message' => '200','quantity' => $product->inventory->quantity, 'remaining' => $remaining_quantity]);
    }

    public function manageStatus(Request $request)
    {
      // dd($request->all());
    

    }

    public function removeQuantity(Request $request)
    {
      $product = Product::findOrFail($request->product_id);
      $prevQuantity = $product->inventory->quantity;
      $newQauntity = (int)$prevQuantity - (int)$request->value;
      $product->inventory->quantity = $newQauntity;
      $product->inventory->timestamps = false;
      $product->inventory->save();
      $remaining_quantity = $product->inventory->quantity - $product->inventory->sold;
      return response()->json(['message' => '200','quantity' => $product->inventory->quantity, 'remaining' => $remaining_quantity]);

    }

}

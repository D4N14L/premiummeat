<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Toastr;
class CartController extends Controller
{
    public function addToCart(Request $request)
    {
      try {

            if($request->quantity <= 0 && $request->quantity > 100)
            {
              return response()->json(['status' => '420']);
            }
              session()->push('cart',array($request->product_id,'quantity' => $request->quantity,'serviceOption' => $request->serviceOptionsSelection,'description' => $request->additionalDescription,'timeslotSelection' => $request->timeslotsSelection,'priceMade' => $request->priceMade));
              return response()->json(['status' => '200','cartSize' => count(session()->get('cart'))]);
                } catch (\Exception $e) {
                  return response()->json(['status' => '500']);
                }

    }

    public function checkout()
    {
	if(count(session()->get('cart')) <= 0 )
	{
    session()->flash('failed','The cart is empty');
		return redirect()->back();
	}

  $cart = session()->get('cart');
  $total = (float) 0.00;
  foreach ($cart as $item) {
    $total +=  $item['priceMade'];
  }
    $minimumAmount = (float) \App\Miscellaneous::first()->minimumAmount;
    if($total < (float) $minimumAmount )
    {
      session()->flash('failed','Please Shop More Than <b>$'.$minimumAmount.'</b>');
      return redirect()->back();
    }
    else
    {
      return view('orders.checkout')
      ->with('cart',$cart)
      ->with('total',$total);

    }

    }


    public function deleteSingleOrder(Request $request)
    {
      try {
        $cart = session()->get('cart');
        foreach ($cart as $key => $value) {
          if($request->product_id == $value[0])
          {
              unset($cart[$key]);
              break;
          }
        }
        session()->put('cart',$cart);
        return response()->json(['status' => '200']);

      } catch (\Exception $e) {

        return response()->json(['status' => '404']);

      }

    }


}

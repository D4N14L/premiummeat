<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Referal;
use Hash;
use Validator;
use Auth;
class ReferalController extends Controller
{
      public function register($referer_id)
      {

        if(!Auth::guest())
        {
          return redirect('/user/manager');
        }
          $referer_id = str_replace('.','/',$referer_id);
          $user = User::where('referral_link','=',$referer_id)->first();
          if(empty($user) || !$user || $user->count() == 0)
          {
            return view('error404');
          }
          return view('referal.register')->with('referer_id',$user->id);
      }
      public function submit(Request $request)
      {
        // dd($request->all());
        $userCheck = User::findOrFail($request->referer_id);

        if(empty($userCheck) || !$userCheck)

        {

          return view('404');

        }
        // dd($request->all());

        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8|confirmed',
            'ethnic' => 'required',
            'landline' => 'required',
            'isAgreementSigned' => 'required'
        ]);
        if($validator->fails())
        {
         return redirect()->back()->withErrors($validator->messages());
        }
        $user = User::create([

            'name' => $request->name,

            'email' => $request->email,

            'password' => $request->password,

            'address' => $request->address,

            'ethnic' => $request->ethnic,

            'password' => Hash::make($request->password),

            'landline' => $request->landline,

            'isAgreementSigned' => $request->isAgreementSigned,

            'referral_link'  =>   Hash::make($request->email.$request->name)

        ]);

        $referal = new Referal();
        $referal->referer_id = $request->referer_id;
        $referal->referal_id = $user->id;
        $referal->save();
        return redirect('/user/manager');


      }
}

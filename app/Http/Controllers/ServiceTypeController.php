<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServiceType;
use App\ServiceTypeOptions;
class ServiceTypeController extends Controller
{
    public function browser()
    {
      //
      $serviceTypes = ServiceType::orderBy('id','desc')
                                                       ->paginate( 10 );
      //
      return view('admin.serviceType.browse')
                                            ->with('serviceTypes',$serviceTypes);
    }

    public function store(Request $request)
    {
      if($request->is_edit == 0)
      {
        //
        $serviceType = new ServiceType( );
        //
        $serviceType->name = $request->name;
        //
        $serviceType->cat_id = $request->cat_id;
        //
        $serviceType->timestamps = false;
        //
        $serviceType->save();
        //
        return redirect()
                        ->route('serviceType.browse');
      }
      else
      {
        //
        $serviceType = ServiceType::find($request->service_id);
        //
        $serviceType->name = $request->name;
        //
        $serviceType->cat_id = $request->cat_id;
        //
        $serviceType->timestamps = false;
        //
        $serviceType->save();
        //
        return redirect()
                        ->route('serviceType.browse');

      }
    }

    public function destroy($id)
    {
      $serviceTypes = ServiceType::find($id);
      $order = \App\OrderInfo::where('service_type_option','=',$serviceTypes->id)->update(['service_type_option' => 0]);;
      $serviceTypes->delete();
      $serviceTypesOptions = ServiceTypeOptions::where('service_type_id','=',$serviceTypes->id)->delete( );

      return redirect()->route('serviceType.browse');
    }
}

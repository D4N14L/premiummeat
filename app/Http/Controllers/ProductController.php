<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Validator;
use App\Inventory;
use App\ProductCategory;
use App\OrderInfo;
use App\ProductPieces;
use Toastr;
class ProductController extends Controller
{

    public function byCategory($id)
    {
      $products = Product::where('cat_id','=',$id)->orderBy('arrange','asc')->get();
      $cat = ProductCategory::where('id','=',$id)->first();
      return view('products.products')->with('products',$products)->with('cat',$cat);
    }

    /*
    |--------------------------------------------------------------------------
    | Admin Side Code
    |--------------------------------------------------------------------------
    |This is admin side code
    |
    |
    |
    |
    */

    public function chickenCategory()
    {
      return view('products.chicken');
    }


    public function greenChickenCategory()
    {
      return view('products.greenChicken');
    }

    public function browse()
    {
      $products = Product::where('cat_id','=',\App\ProductCategory::first()->id)->orderBy('arrange','asc')->get();
      return view('admin.products.browse')->with('products',$products)->with('cat',\App\ProductCategory::first()->name);
    }

    public function browseByCategory($category)
    {
      $products = Product::where('cat_id','=',$category)->orderBy('arrange','asc')->get();
      $category = ProductCategory::findOrFail($category);
      return view('admin.products.browseByCategory')->withProducts($products)->with('cat',$category);
    }

    public function create()
    {
      return view('admin.products.create');
    }

    public function manageStatus(Request $request)
    {
        try {
          $product = Product::findOrFail($request->product_id);
          $tosend = "";
          if($request->status == 0)
          {
            $product->status = 1;
            $tosend = 1;
          }
          if($request->status == 1)
          {
            $product->status = 0;
            $tosend = 0;
          }
           $product->save();
          return response()->json(['message' => '200','status' => $tosend]);
        } catch (\Exception $e) {
          return response()->json(['message' => '404','status' => $tosend]);
        }

    }

    public function store(Request $request)
    {
//      dd($request->all());

      /**
      * Check Valiadtion For Incoming POST Request Using ::Validation
      */


     $validator = Validator::make($request->all(),[
         'name'   => 'required',
          'cat_id' => 'required',
          'image'  => 'required',
          'description' => 'required',
          'sellingUnit_id' => 'required',
         'timeslot' => 'required',
          'price' => 'required'
      ]);

      /**
      * If Validation Test Passess
      */



     if(!($validator->fails()))
     {
       try {


         /**
         * Add New Product On Validation Pass
         */

         $product = new Product();
         $product->name = $request->name;
         $product->description = $request->description;
         $product->cat_id = $request->cat_id;
         $product->sellingUnit_id = $request->sellingUnit_id;
         $product->timeslot = $request->timeslot;
         $product->price = (float) $request->price;

           if($request->has('image'))
           {
             $image = $request->file('image');
             $imageName = time().'.'.$image->getClientOriginalExtension();
             $newImage = \Image::make($request->file('image'));
             $newImage->resize(1366,850);
             $newImage->save(public_path('assets/products/'.$imageName),30);
             $product->image = $imageName;
           }
           if($request->has('avg_weight_piece')){
             $product->avg_weight = $request->avg_weight_piece;
           }
         $product->save();


         /**
         * Create A New Instance Of Inventory
         */


         $inventory = new Inventory();
         $inventory->product_id = $product->id;
         $inventory->save();

         /**
         * Product Pieces Average Weight Add If Avialable
         */


         /**
         * Return Success Toaster On Product Success To Be Added
         */

         Toastr::success('Product Added Successfully!');
         return redirect()->route('products.browse');


        } catch (\Exception $e) {

          /**
          * Return Error Toaster On Product Failed To Be Added
          */

          Toastr::error('Failed To Add Product, Please Try Again!');
          return redirect()->back();

        }

      }
      else
     {
       Toastr::error($validator->messages()->first());
     }

    }
    public function edit($id)
    {
          $product = Product::findOrFail($id);
          return view('admin.products.edit')->withProduct($product);
    }

    public function update(Request $request)
    {
          $product = Product::findOrFail($request->product_id);
          $product->name = $request->name;
          $product->description = $request->description;
          $product->cat_id = $request->cat_id;
          $product->sellingUnit_id = $request->sellingUnit_id;
          // $product->selling_unit_amount = $request->selling_unit_amount;
          $product->timeslot = $request->timeslot;
          $product->price = $request->price;
          if($request->has('image'))
          {
            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $newImage = \Image::make($request->file('image'));
            $newImage->resize(1366,850);
            $newImage->save(public_path('assets/products/'.$imageName),30);
            // $image->move(public_path('assets/products'),$imageName);
            $product->image = $imageName;
          }

          if($request->has('avg_weight_piece')){
            $product->avg_weight = $request->avg_weight_piece;
          }

          $product->save();


















          return redirect()->route('products.browse');
    }

    public function delete($id)
    {
          try {
            $product = Product::findOrFail($id);
            $inventory = Inventory::where('product_id','=',$product->id)->delete();
            $OrderInfo = OrderInfo::where('product_id','=',$product->id)->delete();
            $product->delete();
            Toastr::success('Product Deleted Successfully!');
            return redirect()->route('products.browse');

          } catch (\Exception $e) {
            Toastr::error('Undexpected Error Ocuured While Deleting This Product!');
            return redirect()->back();

          }

    }

    public function managePosition(Request $request)
    {

      try {
        foreach ($request->positions as $value) {
          \App\Product::where('id',$value[0])->update(['arrange' => $value[1]]);
        }
        return response()->json(['status' => '200']);
      } catch (\Exception $e) {
        return response()->json(['status' => '500']);
      }


    }

}

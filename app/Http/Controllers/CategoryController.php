<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductCategory;
use URL;
use Toastr;
class CategoryController extends Controller
{
  public function create()
  {
    $category = ProductCategory::orderBy('id','desc')->paginate(10);
    return view('admin.category.create')->withCategory($category);
  }

  public function store(Request $request)
  {

    $isEdit = $request->isEdit;

    try {
      if($isEdit == "0")
      {

        $category = new ProductCategory();
        $category->timestamps = false;
        $category->name = $request->name;
        $image = $request->file('image');
        $photoName = time().'.'.$image->getClientOriginalExtension();
        $image->move(public_path().'/assets/category',$photoName);
        $category->image = $photoName;
        $category->save();

      }
      else
      {

        $category = ProductCategory::findOrFail($request->productId);
        $category->timestamps = false;
        $category->name = $request->name;
        if($request->has('image'))
        {
          $image = $request->file('image');
          $photoName = time().'.'.$image->getClientOriginalExtension();
          $image->move(public_path('assets/category'),$photoName);
          $category->image = $photoName;
        }
        $category->save();

      }
      Toastr::success("Category Created Succesfully");
      session()->flash('success','Category Created Succesfully');
    } catch (\Exception $e) {
      Toastr::error("There was a problem while creating or editing this category");
      session()->flash('failed','Failed to create');

    }

    return redirect()->back();
  }



  public function delete($id)
  {

    $category = ProductCategory::findOrFail($id);
    $product = \App\Product::where('cat_id','=',$category->id)->delete();
    $category->delete();
     Toastr::success('Category Deleted Successfully!');
     session()->flash('success','Category Deleted Successfully!');
     return redirect()->back();

        // try{
        //
        // }
        // catch(\Exception $e)
        // {
        //   Toastr::error('Category Deleted Successfully!');
        //   session()->flash('failed','Category Deleted Successfully!');
        //   return redirect()->back();
        //
        // }

  }
}

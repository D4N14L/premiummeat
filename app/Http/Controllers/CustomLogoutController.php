<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class CustomLogoutController extends Controller
{
    public function logout()
    {
          Auth::logout();

          $this->middleware('PreventRedirectBack');
          return redirect()->route('voyager.login');
    }
}

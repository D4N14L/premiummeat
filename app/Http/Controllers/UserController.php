<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;
use Validator;
use Toastr;
class UserController extends Controller
{
    public function browse()
    {
      $user = User::orderBy('id','desc')->paginate(10);
      return view('admin.users.browse')->withUsers($user);
    }

    public function getUserManager()
    {
      return view('userManager/v2/index');
    }

    public function changePassword(Request $request)
    {
      try {
        $user  = User::findOrFail(Auth::user()->id);
        $user->password = Hash::make($request->password);
        Toastr::success('Password Changed Succesffully');
        $user->save();
        Toastr::success('Password Changed Succesffully');
        return redirect()->back();
      } catch (\Exception $e) {
        Toastr::error('An Occured While Changing UserName');
        return redirect()->back();
      }


    }

    public function emailVerification($verification_token)
    {
      $user = User::where('verification_token','=',$verification_token)->first();
      if($user->exists())
      {
        try {
          $user->isVerified = 1;
          $user->verification_token = NULL;
          $user->save();
          Toastr::success('Email Verified Succesffully!');
          return redirect()->route('user.getUserManager');
        } catch (\Exception $e) {
          Toastr::error('An Error Occured While Verifying Email');
          return redirect()->route('user.getUserManager');

        }


      }
      else
      {
        Toastr::error('Verification Token Expired!');
        return redirect()->route('user.getUserManager');
      }

    }
    public function updateProfile(Request $request)
    {
      if(!$request->has('password'))
      {
        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:50',
            'address' => 'required|string|max:100',
            'landline' => 'required|max:15',
            'ethnic' => 'required',
            'suburb_id' => 'required',
        ],[
          'suburb_id.required' => 'Choose City & Suburb',
        ]);

        if($validator->fails()){
          session()->forget('failed');
          session()->flash('failed',$validator->messages()->first());
          return redirect()->back()->withInput();
        }
        User::unguard();
        $user = User::findOrFail(Auth::user()->id)->update($request->except('_token'));
        User::reguard();

        if($user){
          Toastr::success('Profile Updated Successfully');
          session()->flash('success','Profile Updated Successfully');
          return redirect()->back();
        }else
        {
          Toastr::success('Profile Updated Successfully');

          session()->flash('failed','Unable to updated profile');
          return redirect()->back();

        }
      }
      else
      {

        $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:50',
            'address' => 'required|string|max:100',
            'landline' => 'required|max:15',
            'ethnic' => 'required',
            'password' => 'required|confirmed|min:6',
            'suburb_id' => 'required',
        ],[
          'suburb_id.required' => 'Choose City & Suburb',
        ]);

        if($validator->fails()){

          Toastr::error($validator->messages()->first());
          session()->forget('failed');
          session()->flash('failed',$validator->messages()->first());
          return redirect()->back()->withInput();
        }
        $data = $request->expcet('_token');
        $data['password'] = Hash::make($data['password']);
        User::unguard();
        $user = User::findOrFail(Auth::user()->id)->update($data);
        User::reguard();

        if($user){
          Toastr::success('Profile Updated Successfully');
          // session()->flash('success','Profile Updated Successfully');
          return redirect()->back();
        }else
        {
          Toastr::error('Unable to updated profile');
          session()->forget('failed');
          session()->flash('failed','Unable to updated profile');
          return redirect()->back();

        }
      }


    }
    public function getEditForm(){
      return view('userManager.v2.edit');
    }
















    // public function destroy($id)
    // {
    //
    //
    //
    //
    //
    //
    //
    //
    //   try {
    //     $user = \App\User::findOrFail($id);
    //     $orders = \App\Order::where('customer_id','=',$user->id)->get();
    //     foreach ($orders as $order) {
    //       $ordersInfo = \App\OrderInfo::where('order_id','=',$order->id)->delete();
    //     }
    //     $orders->delete();
    //
    //
    //     $user->delete();
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //
    //   } catch (\Exception $e) {
    //     Toastr::error('There was an error while deleting user!');
    //     return redirect()->back();
    //   }
    //
    // }









}

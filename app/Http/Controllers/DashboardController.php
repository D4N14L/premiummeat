<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductCategory;
class DashboardController extends Controller
{

  public function __construct()
  {
      $this->middleware('auth');
  }

    public function dashboard()
    {
      $category = ProductCategory::all();
     return view('admin.dashboard.dashboard')->with('category',$category);
    }

}

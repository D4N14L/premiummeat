<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SellingUnit;
use App\Product;
class SellingUnitController extends Controller
{
    public function browse()
    {
        $sellingUnit = SellingUnit::orderBy('id','desc')
                                    ->paginate(10);
        return view('admin.sellingUnits.browse')
                                    ->with('sellingUnit',$sellingUnit);
    }

    public function store(Request $request)
    {
      if($request->isEdit == '0')
      {
        $SellingUnit = new SellingUnit();
        $SellingUnit->timestamps = false;
        $SellingUnit->name = $request->name;
        $SellingUnit->save();
      }
      else
      {
        $SellingUnit = SellingUnit::find($request->sellingUnit_id);
        $SellingUnit->name = $request->name;
        $SellingUnit->timestamps = false;

        $SellingUnit->save();
      }

        return redirect()->back();

    }

    public function destroy($id)
    {
        $sellingUnit = SellingUnit::find($id);
        $products = Product::where('sellingUnit_id','=',$id)->delete();

        SellingUnit::destroy($id);
        return redirect()
               ->back();
    }
}

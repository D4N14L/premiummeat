<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\OrderInfo;
use PDF;
use Toastr;
use Cartalyst\Stripe\Stripe;

class OrderController extends Controller
{
    public function index()
    {
      $order = Order::where('order_status','=',0)->orderBy('id','desc')->paginate(20);
      return view('admin.order.index')->with('order',$order);
    }
    // list charged orders

     public function chargedOrders(){
      $order = Order::where('order_status','=',1)->orderBy('id','desc')->paginate(20);
      return view('admin.order.charged')->with('order',$order);
    }
    public function generateReport($id)
    {
      $OrderInfo = OrderInfo::where('order_id','=',$id)->get()->toArray();
      $Order = Order::where('id','=',$id)->first();
      $customer = $Order->User();
      $orders = ['customer' => $customer->toArray() ,'order' => $Order->toArray(),'orderDetails' => $OrderInfo];
      $pdf = PDF::loadView('admin.order.report',compact('orders',$orders));
      return $pdf->stream();
      // $pdf->download(\Carbon\Carbon::now()->format('Y-M-D'));
      // return $pdf->stream();
    }

    public function details($id)
    {
      $OrderInfo = OrderInfo::where('order_id','=',$id)->get();
      return view('admin.order.detail')->with('OrderInfo',$OrderInfo);
    }

    public function markStatus($id)
    {
      $order = Order::findOrFail($id);
      $order->order_status = 1;
      $order->save();
      return redirect()->back();
    }


    // customer charge

     public function customerCharge($customer_id,$amount,$order_id)
    {
      try {
        $stripe = new Stripe('sk_test_zRfEiWeHdZa5kaw2qtRJcjLJ');
        $charge = $stripe->charges()->create([
            'customer' => $customer_id,
            'currency' => 'NZD',
            'amount' => (float) $amount
        ]);
        if($charge['status'] == "succeeded"){
          $order = Order::findOrFail($order_id);
          $order->order_status = 1;
          $order->save();
          session()->flash('success','Amount '.$amount.' Has been charged Successfully from customer!');
          return redirect()->back();
        }
      } catch (\Exception $e) {
          session()->flash('failed',$e->getMessage());
        return redirect()->back();

      }

    }



    public function manageDetails(Request $request)
    {
        try {
          $OrderInfo = OrderInfo::findOrFail($request->order_detail_id);
          $OrderInfo->extra_quantity =  (float) $request->extraQuantity;
          $OrderInfo->amount = (float) $request->totalAmount;
          $OrderInfo->save();
          $Order = Order::findOrFail($request->currentOrderID);
          $Order->total_amount = (float) $request->totalAmount;
          $Order->save();
          return response()->json(['status' => 200]);
        } catch (\Exception $e) {
          return response()->json(['status' => 500]);
        }

    }












}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TimeSlot;
use App\GeneralTimeslot;
use DB;
use Toastr;
class TimeSlotController extends Controller
{
    public function browse()
    {
      $morningTimeSlot = TimeSlot::where('shift','=','morning')->get();
      $eveningTimeSlot = TimeSlot::where('shift','=','evening')->get();
      $generalTimeslot = DB::select('SELECT *  FROM timeslots as tm JOIN general_timeslot as gtm  WHERE gtm.timeslot_id = tm.id ORDER BY gtm.id DESC');
      return view('admin.timeslots.browse')
             ->with('morningTimeSlot',$morningTimeSlot)
             ->with('eveningTimeSlot',$eveningTimeSlot)
             ->with('generalTimeslot',$generalTimeslot);
    }
    public function store(Request $request)
    {

      if($request->isEdit == '0')
      {
          if(TimeSlot::where('shift','=',$request->type)->get()->count() > 0)
          {
            Toastr::error('1 '. $request->type .'Slot Already Exists!', $title = 'Error', $options = []);
          }
          else
          {
            $timeslot = new TimeSlot();
            $timeslot->shift = $request->type;
            $timeslot->startTime = $request->startTime;
            $timeslot->endTime = $request->endTime;
            $timeslot->timestamps = false;
            $timeslot->save();

            $generalTimeslot = new GeneralTimeslot();
            $generalTimeslot->timeslot_id = $timeslot->id;
            $generalTimeslot->timestamps = false;
            $generalTimeslot->save();

          }
            return redirect()->back();
    }
    else
    {
            $timeslot = TimeSlot::findOrFail($request->editTimeSlot);
            $timeslot->startTime = $request->startTime;
            $timeslot->endTime = $request->endTime;
            $timeslot->timestamps = false;
            $timeslot->save();
            Toastr::success('Timeslot Updated Successfully!', $title = 'success', $options = []);
            return redirect()->back();

    }





  }

    //AJAX Routes

    public function manageGeneralState(Request $request)
    {
      try {

        $general = GeneralTimeslot::findOrFail($request->general_id);
        if($request->status == "disable")
        {
          $general->isActive = 0;
        }
        else
        {
          $general->isActive = 1;
        }
        $general->timestamps = false;
        $general->save();
        return response()->json(['message' => '200']);


      } catch (\Exception $e) {
        return response()->json(['message' => '500']);
      }

    }


}

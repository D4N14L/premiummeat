<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use URL;
class ApiController extends Controller
{
    public function getProducts()
    {
      try {
        $data = \App\Product::all();
        if(!is_null($data))
        {
          foreach ($data as &$d) {
            $d->image = URL::to('/').'/'.'assets/products/'.$d->image;
          }
          return response()->json(['status' => '200','products' => $data]);

        }else
        {
          return response()->json(['status' => '404','message' => 'No Products found!']);

        }


      } catch (\Exception $e) {
        return response()->json(['status' => '402','message' => 'An unknown error occured!']);

      }








    }
    public function getProductById($id)
    {
      try {

        $data = \App\Product::where('id','=',$id)->first();
        if(!is_null($data))
        {
          $data->image = URL::to('/').'/'.'assets/products/'.$data->image;
          return response()->json(['status' => '200','products' => $data]);
        }
        else
        {
          return response()->json(['status' => '404','message' => 'No Products found!']);

        }

      } catch (\Exception $e) {
        return response()->json(['status' => '402','message' => 'An unknown error occured!']);

      }

    }






    public function getCategories()
    {
      try {
        $data = \App\ProductCategory::all();
        if(!is_null($data) )
        {
          foreach ($data as &$d) {
            $d->image = URL::to('/').'/'.'assets/category/'.$d->image;
          }
          return response()->json(['status' => '200','categories' => $data]);
        }
        else
        {
          return response()->json(['status' => '404','message' => 'No Categories found!']);
        }


      } catch (\Exception $e) {
        return response()->json(['status' => '402','message' => 'An unknown error occured!']);
      }

    }


    public function getCategoriesById($id)
    {
      try {
        $data = \App\ProductCategory::where('id','=',$id)->first();
        if(!is_null($data))
        {
          $data->image = URL::to('/').'/'.'assets/category/'.$data->image;
          return response()->json(['status' => '200','categories' => $data]);
        }
        else
        {
          return response()->json(['status' => '404','message' => 'No category found!']);
        }
      } catch (\Exception $e) {
        return response()->json(['status' => '402','message' => 'An unknown error occured!']);

      }

    }










}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Suburb;
class WelcomeController extends Controller
{
    public function welcome()
    {
        return view('welcome');
    }

    public function getSuburbByAreaId(Request $request)
    {
      $suburb = Suburb::where('areas_id','=',$request->area_id)->get();
      return response()->json($suburb);
    }

}

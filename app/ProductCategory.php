<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
class ProductCategory extends Model
{
    protected $table = 'product-category';

    public function products()
    {
      return $this->hasMany(Product::class,'cat_id');
    }

}

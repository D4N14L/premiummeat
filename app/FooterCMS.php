<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FooterCMS extends Model
{
    protected $table = "footer_cms";
}

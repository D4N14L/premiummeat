<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPieces extends Model
{
    protected $table = "product_pieces";
}

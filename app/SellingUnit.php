<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;
class SellingUnit extends Model
{
    protected $table = 'SellingUnits';

    public function products()
    {
      return $this->hasMany(Product::class,'sellingUnit_id');
    }
}

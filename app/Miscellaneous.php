<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Miscellaneous extends Model
{
    protected $table = 'miscellaneous';
    public $timestamps = false;
    protected $fillable = ['deliveryCharges','minimumAmount'];
}

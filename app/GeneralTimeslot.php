<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\TimeSlot;
class GeneralTimeslot extends Model
{
    protected $table = "general_timeslot";

    public function getTimeSlot()
    {
      return TimeSlot::where('id','=',$this->timeslot_id)->first();
    }
}

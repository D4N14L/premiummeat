<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ServiceType;
class ServiceTypeOptions extends Model
{
    protected $table = "service_type_options";
    protected $fillable = ['name','service_type_id'];

    public function getServiceType()
    {
      return ServiceType::where('id','=',$this->service_type_id)->first();
    }

public $timestamps = false;
}

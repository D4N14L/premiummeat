<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeSlot extends Model
{
    protected $table = "timeslots";

    public function product()
    {
      return $this->belongsTo(Product::class);
    }
}

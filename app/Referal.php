<?php

namespace App;
use App\User;
use App\Order;;
use Illuminate\Database\Eloquent\Model;

class Referal extends Model
{
    protected $table = "referal";

    public function info()
    {
      return User::where('id','=',$this->referal_id)->first();
    }
    public function getLastOrder()
    {
      $order=   Order::where('customer_id','=',$this->referal_id)->orderBy('created_at','desc')->first();      // return ;
      if(empty($order))
      {
        return '<i style="color:red"> No Orders </i>';
      }
      else
      {
        return $order->created_at->diffForHumans();
      }
    }
    public function getReferalInfo()
    {
      return \App\User::where('id','=',$this->referal_id)->first();
    }

}

<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;
class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','ethnic','referral_link','address','landline','verification_token','suburb_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    public function getNumberOfReferals()
    {
      $referal = \App\Referal::where('referer_id',$this->id)->get();
      if(!empty($referal))
      {
        return $referal->count();
      }
      else
      {
        return 0;
      }
    }

    public function getRefarals()
    {
      return \App\Referal::where('referer_id',$this->id)->get();
    }

      public function getLastOrderDate()
    {
      $order= \App\Order::where('customer_id','=',$this->id)->orderBy('created_at','desc')->first()->created_at;
      if(!empty($order))
      {
        return $order;
      }
      else
      {
        return null;
      }
    }

    public function getNumberOfOrders()
    {
      return \App\Order::where('customer_id','=',$this->id)->get()->count();
    }
    public function getOrderCompleted()
    {
      return \App\Order::where('customer_id','=',$this->id)->where('order_status','=',1)->get()->count();
    }
    public function amountPaid()
    {
      return is_null(DB::select("SELECT sum(amount) as 'amount_paid' from orders_info WHERE order_id IN ( SELECT id from orders WHERE customer_id = ? )",[$this->id])[0]->amount_paid) ? '0' : DB::select("SELECT sum(amount) as 'amount_paid' from orders_info WHERE order_id IN ( SELECT id from orders WHERE customer_id = ? )",[$this->id])[0]->amount_paid;
    }



}

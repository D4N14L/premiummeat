@extends('layouts.app')

@section('content')
  <?php $backgroundColor = \App\FrontendCMS::get()->first()->backgroundColor; ?>

  <style media="screen">
    body{
      overflow-y: scroll;
    }
    label
    {
      color:{{$backgroundColor}} !important;
    }
    @if(url()->current() == URL::to('/').'/login')
    #logo-main
    {
      display: none;
    }
    @endif
  </style>
<div class="container" style="padding:0px 4px 0px 4px">
    <div class="row justify-content-center" style="padding-top:8%">
        <div class="col-md-8">

            <div class="card" style="margin-top:19px" >
                <div class="card-content"  >
                  <a style="background-color:{{$backgroundColor}} !important" onclick="history.back()"  class="btn btn-floating pink"> <i class="fa fa-arrow-circle-left white-text"></i> </a>

                 <center >
                   <div class="navbar-logo z-depth-2 hide-on-med-and-down">
                   </div>
                 </center>
                   <center><br><br>
                     <div class="card-title pink-text" > <h5 style="font-size:18px;color:{{$backgroundColor}}">{{ __('Login') }} <br>                      </h5>
                       <div class="divider pink" style="width:2%;background-color:{{$backgroundColor}} !important">

                       </div>
                    </div>
                  </center>

                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="row">

                            <div class="col m10 l10 s10 offset-s1 offset-l1 offset-m1 input-field">
                              <label for="email" class="col-sm-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row" style="margin-top:-40px">

                            <div class="col m10 l10 s10 offset-s1 offset-l1 offset-m1  input-field">
                              <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" style="color:red" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                                                <div class="row">
                                                  <div class="col m10 l10 s10 offset-s1 offset-l1 offset-m1">
                                                    <input  type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                    <label for="remember"><span class="pink-text">{{ __('Remember Me') }}</span></label>

                                                  </div>
                                                </div>






                          <div class="row" style="margin-bottom:-14px;" >
                            <div class="col m10 l10 s10 offset-s1 offset-l1 offset-m1">
                              <button  type="submit" style="border-radius:0px;margin-top:1px;margin-bottom:5px;background-color:{{$backgroundColor}} !important" class="pink white-text btn">
                                  {{ __('Login') }}
                              </button>
                              <a class="btn green left" style="margin-top:1px;margin-bottom:5px;border-radius:0px;font-size:14px;margin-right:4px;" href="{{ route('password.request') }}">
                                Forgot Password?
                              </a>

                            </div>
                          </div>

                          <br>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

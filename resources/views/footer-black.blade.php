<div class="section footer-last"  data-anchor="footer-last">
        <center>
            {{-- <div class="premium-logo"></div> --}}<br><br><br><br>
            <div class="navbar-logo" style="width:100px;height:100px;border:5px solid white;"></div>
        </center>

        <div  class="opx" style="padding:100px;padding-left:25px;padding-right:25px">
                <div class="row">
                        <div class="col s12 m6 l6">
                        <p style="color:white;text-align:justify" >
                              {{ \App\FooterCMS::where('id','=','1')->first()->description }}
                        </p>
                        </div>
                            <div class="col s12 m5 l5 offset-m1 offset-l1">
                                    <ul class="white-text center" style="text-align:left;font-size:12px">
                                            <li>
                                                 <i class="fa fa-building fa-fw"></i>{{ \App\FooterCMS::where('id','=','1')->first()->address }}</li>
                                            <li>
                                                 <i class="fa fa-phone fa-fw"></i>{{ \App\FooterCMS::where('id','=','1')->first()->phone }}</li>
                                            <li>
                                               <i class="fa fa-fax fa-fw"></i>{{ \App\FooterCMS::where('id','=','1')->first()->landline }}</li>
                                            <li>
                                                <i class="fa fa-envelope fa-fw"></i>{{ \App\FooterCMS::where('id','=','1')->first()->email }}</li>
                                        </ul>

                            </div>

                    </div>
        </div>

    </div>

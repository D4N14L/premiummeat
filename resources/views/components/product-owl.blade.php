<div class="card owl-card-custom">
        <div class="card-image">
                <img style="object-fit:cover" src="{{ URL::to('/assets/products/'.$img) }}" alt="">
        <span class="card-title">{{ $name }}</span>
              </div>
        <div class="card-content">
                <span  class="left-align">
                        {{ $category }}
                    </span>
                    <span style="display:block" class="right-align">
                            <b>$ {{ $price }} NZ</b>
                        </span>

        </div>
    </div>

<div class="col s12 m4 l4">
  <div class="card">
          <div class="card-image">
                  <img style="object-fit:cover" src="{{ URL::to('/assets/products/'.$product->image) }}" alt="">
          <span class="card-title">{{ $product->name }}</span>
          <button onclick="addToCart(this)" data-price="{{ $price }}" data-product-id="{{ $product->id }}" class="btn btn-floating halfway-fab waves-effect orange waves-light" style="line-height:42px;right:-2%"> <i class="white-text fa fa-shopping-cart"></i> </button>
                </div>
          <div class="card-content" style="padding-bottom:40px">
                  <span  class="left" style="font-size:14px;font-weight:bold">
                          {{ $product->category->name }}
                      </span>
                      <span style="display:block" class="right">
                              <b>$ {{ $product->price }} NZ</b>
                        </span>
                        <hr>
              <div class="row">
                  <div class="col-sm-4 col-md-4 col-lg-4">
                    <h4>Delivery Time</h4>
                    <p>
                      {{ $product->timeslot }}
                    </p>
                  </div>
              </div>
          </div>
      </div>

</div>

<script type="text/javascript">
  function addToCart($el)
  {
    let product_id = $el.getAttribute('data-product-id')
    let price = $el.getAttribute('data-price')
  }



</script>

  @extends('layouts.app')


  @section('content')
    <?php $backgroundColor = \App\FrontendCMS::get()->first()->backgroundColor; ?>
    <?php $admin_logo_img = Voyager::setting('site.logo'); ?>

    <script type="text/javascript" src="{{ URL::to('/js/axios.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.js"></script>
    @include('styles')

    <style media="screen">

    /* large screens handler for estimated price */










      /* card header grid */

      .card-top-grid{
        display: grid;
        grid-template-areas: "logo quantity btn-modal"
      }
      .card-top-grid .logo {
        grid-area: logo;
      }
      .card-top-grid .add-quantity-title{
        grid-area: quantity
      }
      .card-top-grid .closed-btn-modal{
        grid-area: btn-modal
      }



      /* counter grid */

      .counter-grid{
        display: grid;
        grid-template-areas: "decrementer quantity-display incrementer";
        align-items:center;
        justify-content: center;
      }
      .counter-grid .decrementer{
        grid-area: decrementer;
        margin-right:15px;
        margin-top:-25px;
      }
      .counter-grid .quantity-display{
        grid-area:quantity-display;
        margin-left:10px;
      }

      .counter-grid .incrementer{
        grid-area:incrementer;
        margin-left:15px;
        margin-top:-25px;
      }


      /* inner  grid container */

      .innerCardGrid{
        display: grid;
        grid-template-areas: "counter-grid calculator serviceType"
      }
      .innerCardGrid .counter-grid{
        grid-area: counter-grid;
      }
      .innerCardGrid .calculator{
        grid-area: calculator;
        margin-top:-15px;
        align-self: center !important ;
        justify-content: flex-end !important;
      }
      .innerCardGrid .serviceType{
        grid-area: serviceType;
        margin-top:-15px;
      }




      @media screen and (max-width:768px){
          .card-top-grid{
            grid-template-areas: ". quantity btn-modal"
          }
          .innerCardGrid{
              grid-template-areas:
              "counter-grid"
              "calculator"
              "serviceType";
          }
          .
          .innerCardGrid .counter-grid{
            align-self: center;
            margin-left:20%;
          }
          .innerCardGrid .serviceType{
            align-self: center;
            align-text:center;
            align-items: center;
            margin-top:15px;
            margin-bottom:50px;
          }
          .card-top-grid .logo {
            /* width: 85px !important;
            height: auto !important; */
            display: none;
          }
          .decrementer{
            width: 80%;
            margin-right: 25px;
          }
          .counter-grid .incrementer{
            width: 110%;
            margin-left:9.5px;
          }
          .notify{
            word-wrap: break-word !important;

            width:100% !important;
            display: block !important;
          }

      }



    </style>

















  <div class="container">
    <div class="row">

      <center>
        <div class="">
          <br><br>
          <p class="flow-text pink-text" style="color:{{$backgroundColor}} !important">
            <strong style="font-weight:800">{{ ucfirst($cat->name) }}</strong> Products
            <div class="divider pink" style="background-color:{{$backgroundColor}} !important;width:15%;height:1px;border-radius:15px"></div>
          </p>
          <img src="{{public_path()}}/assets/halal-label.jpg" style="width:50px;height:50px;border-radius:100%" alt="" />
        </div>
      </center>
    </div>
    <div class="row">
      <div class="col s12 m12 l12">
          <a id="historian" style="background-color:{{$backgroundColor}} !important" onclick="history.back()" class="btn btn-floating pink left"> <i class="fa fa-arrow-circle-left white-text"></i> </a>
      </div>

      @if( $products->count() > 0 )
        <input type="hidden" id="main-cat-name" name="" value="{{ strtolower($products->first()->category->name) }}">
        @foreach ( $products as $product )


            {{--
              * Detecting sellingUnit of currently iterating product
              *
              *
            --}}


            <input type="hidden" id="sellingUnit_of_product-{{$product->id}}" value="{{strtolower($product->sellingUnit->name)}}">



            {{--
              * Average of currently iterating product
              * having sellingunit type of piece
              *
            --}}

            <input type="hidden" id="avg_weight_of_product-{{$product->id}}" value="{{(float)$product->avg_weight}}">






          <?php static $incrementerMaring = 1; ?>

            <div class="col s12 m4 l4" >
              <div class="card" style="width:auto;background:transparent;box-shadow:none" >
                <b style="font-size:12px;position:absolute;top:10px;left:15px;z-index: 1000;background-color: white;color:{{$backgroundColor}} !important;width:auto;border-radius:25px;padding:12px;text-align:left;">NZ $ {{ $product->price }} / {{ strtolower($product->sellingUnit->name) == "piece" ? "Kg": $product->sellingUnit->name }}</b>

                @if($incrementerMaring <= 3)
                  <div class="card-image top3 waves-effect waves-block waves-light" style="height:300px">
                @else
                  <div class="card-image waves-effect waves-block waves-light" style="margin-top:-50px !important;height:300px">
                @endif
                <?php $incrementerMaring++; ?>
                          <img data-target="modal{{$product->id}}" class="modal-trigger" style="object-fit:cover;margin:0px;max-height:85%;min-width:100%;height:85%"  src="{{ URL::to('/assets/products/'.$product->image) }}" alt="">
                          <span class="card-title" style="text-align:left;margin-left:-2px;display:block;float:left;background-color:rgba(0,0,0,0.4);height:110px;position:relative;bottom:36%;width:100%;left:0.5%;margin-right:-20px !">{{ $product->name }} <br>&nbsp;</span>

                            </div>
                      <div class="card-content" style="padding-bottom:0px;padding-top:0px">
                        <button  class="btn z-depth-3 btn-floating halfway-fab waves-effect pink waves-light btn-large modal-trigger" data-target="modal{{$product->id}}" style="line-height:42px;right:-2%;top:64%;right:5%;background-color:white !important"> <i style="color:{{$backgroundColor}}" class=" fa fa-shopping-cart"></i> </button>
                          <div class="row">

                          </div>
                      </div>



                  </div>

                  <div id="modal{{$product->id}}" style="padding:auto 400px"  class="modal">
                    <div class="modal-content" style="margin-bottom:10px" >
                      <div class="card-top-grid">



                          {{-- card logo --}}

                        <img class="logo" src="{{ Voyager::image($admin_logo_img) }}"  style="align-self:center;height:45px;width:auto;" alt="">

                        {{-- card quantity title --}}


                          <center class="add-quantity-title" style="margin-top:-10px;align-self:baseline">
                            <p >
                            <span style="text-align: center;font-size:18px;font-weight:800;display:block;">Add Quantity</span>
                                <div class="divider " style="background-color: {{$backgroundColor}};width:20%;height:2px;border-radius:15px"></div>
                            </p>
                          </center>

                          {{-- card modal close btn --}}

                          <span class="card-title grey-text text-darken-4 modal-close closed-btn-modal" ><i style="color:{{$backgroundColor}} !important" class="fa fa-times-circle right"></i></span>



                      </div>

                        <div style="margin-top:45px;" >
                          <div class="innerCardGrid">
                            <div class="counter-grid">
                              {{-- DECREMENTER --}}






                              <button style="background-color:{{ $backgroundColor }} !important;" type="button"  onmousedown="decrement(this)" data-product-price="{{ $product->price }}"  data-product-id="{{ $product->id }}"  class="btn btn-success btn-flat decrementer white-text btn-small" name="button">
                                <i class="fa fa-minus"></i>
                              </button>






                              {{-- DECREMENTER END --}}







                              {{-- quantity-display --}}

                              <?php $initialValue = 1; ?>

                              @if(ucfirst($cat->name) == 'Whole Bird' || ucfirst($cat->name) == 'Free Range Chicken Whole Bird')
                                <?php $initialValue = 1 ?>
                                  @if(strtolower($product->sellingUnit->name) == "kg")

                                    <input class="quantity-display" oninput="assignQuantity(this)" disabled id="assignQuantity-{{$product->id}}"   data-product-price="{{ $product->price }}" data-product-id="{{ $product->id }}"  type="number" step="0.5" min="0.5" value="1"  style="width:100px;border:1px solid #ccc;text-align:center"  min="1" value="1" max="100"  />&nbsp;&nbsp;
                                  @else
                                    <input class="quantity-display" oninput="assignQuantity(this)" disabled id="assignQuantity-{{$product->id}}"   data-product-price="{{ $product->price }}" data-product-id="{{ $product->id }}"  type="number" step="1" min="1" value="1"  style="width:100px;border:1px solid #ccc;text-align:center"  min="1" value="1" max="100"  />&nbsp;&nbsp;

                                  @endif


                              @else
                                <?php $initialValue = (float)0.5; ?>
                                @if(strtolower($product->sellingUnit->name) == "kg")
                                  <input class="quantity-display" oninput="assignQuantity(this)"   disabled id="assignQuantity-{{$product->id}}" data-product-price="{{ $product->price }}" data-product-id="{{ $product->id }}"  type="number" step="0.5" min="0.5" value="0.5"  style="width:100px;border:1px solid #ccc;text-align:center"  min="1" value="1" max="100"  />&nbsp;&nbsp;
                                @else
                                  <?php $initialValue = 1; ?>
                                  <input class="quantity-display" oninput="assignQuantity(this)"   disabled id="assignQuantity-{{$product->id}}" data-product-price="{{ $product->price }}" data-product-id="{{ $product->id }}"  type="number" step="1" min="1" value="1"  style="width:100px;border:1px solid #ccc;text-align:center"  min="1" value="1" max="100"  />&nbsp;&nbsp;
                                @endif
                              @endif



                              {{-- quantity-display End --}}




                              {{-- INCREMENTER --}}

                              <button style="background-color:{{ $backgroundColor }} !important;" onmousedown="increment(this)"  type="button" data-product-price="{{ $product->price }}"  data-product-id="{{ $product->id }}" class="btn btn-success btn-flat incrementer  white-text btn-small" name="button">
                                <i class="fa fa-plus"></i>
                              </button>

                              {{-- INCREMENTER END --}}

                            </div>

                            <input type="hidden" id="quantity-{{ $product->id }}" name="" value="{{$initialValue}}"  min="{{$initialValue}}"  />
                            @if(strtolower($product->sellingUnit->name) == "pieces" || strtolower($product->sellingUnit->name) == "piece")
                              <input type="hidden" id="priceMade-{{$product->id}}" value="{{ (float) ($product->price * $initialValue) * (float) $product->avg_weight }}">
                            @else
                              <input type="hidden" id="priceMade-{{$product->id}}" value="{{ (float) $product->price * $initialValue }}">
                            @endif
                            <div class="calculator" id="for-large-screens">
                              <center>

                                <p  style="color:{{ $backgroundColor }} !important;font-weight:1000;padding:0px;margin:0 auto ">
                                  <div>
                                    <label  style="color:{{$backgroundColor}};font-family:'Raleway';font-size:14px;font-weight:600" >Estimated Price</label> <br><br>
                                    @if(strtolower($product->sellingUnit->name) == "pieces" || strtolower($product->sellingUnit->name) == "piece")
                                      <span style="color:{{ $backgroundColor }} " id="count-total-{{$product->id}}"> {{ number_format((float) $product->price,2,'.','') }} X  {{$initialValue}} X {{$product->avg_weight}} Kg = $ {{ (float) ((float) number_format((float) $product->price,2,'.','') * (float) $initialValue) * (float) $product->avg_weight }}</span>

                                    @else
                                      <span style="color:{{ $backgroundColor }} " id="count-total-{{$product->id}}"> {{ number_format((float) $product->price,2,'.','') }} X  {{$initialValue}} = $ {{ (float) number_format((float) $product->price,2,'.','') * (float) $initialValue }}</span>

                                    @endif
                                   </div>
                                </p>
                              </center>

                            </div>

                            <div class="input-field serviceType" id="service-type-{{ $product->id }}">
                            <label for="service-type-sub-{{$product->id}}" style="color:{{$backgroundColor}};font-size:14px;font-weight:500" >Choose Service Type</label> <br><br>
                            <select style="border:2px solid rgba(233, 30, 99,0.7);color:white" id="service-type-sub-{{$product->id}}" multiple  data-product-id="{{$product->id}}"   class="service_type" name="service_type">
                            <option value="0" disabled selected>Select Service Type</option>
                            @foreach (\App\ServiceType::where('cat_id','=',$cat->id)->orderBy('name','asc')->get() as $service)
                             <option value="{{ $service->id }}">{{ $service->name }}</option>
                            @endforeach
                            </select>

                            </div>
                          </div>
                          <div >
                          </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="margin-top:-60px;">
                      <center>
                        <button  onclick="addToCart(this)"   data-price="{{ $product->price }}" data-product-id="{{ $product->id }}" class="btn btn-flat waves-effect waves-light white-text modal-close" style="background-color:{{$backgroundColor}} !important">Add To Cart <i class="white-text fa fa-shopping-cart"></i> </button>
                      </center>
                      <center>
                          <p class="card notify" style=";border-radius:25px">
                            <div class="card-content" style="color:{{$backgroundColor}} !important">
                              <i class="fa fa-exclamation-circle"></i>  {{$product->description}}
                            </div>
                        </p>
                        </center>

                    </div>
                  </div>
            </div>

        @endforeach


      @else


        <div class="col s12 m12 l12 card">
          <div class="card-content">
            <p class="pink-text flow-text" style="color:{{$backgroundColor}} !important">
              <i> <i  class="fa fa-warning red-text" style="font-size:32px;color:red"></i> &nbsp;&nbsp;&nbsp;<span style="font-size:18px">No, Products Found For <b>{{ $cat->name }}</b> <sup>Category</sup> </span> </i>
            </p>
          </div>
        </div>


      @endif
    </div>











  </div>
  <script>
  $(document).ready(function(){
    $('.modal').modal();

    // console.log(moment().format('MM'));
    // console.log(new Date())

    $('.dropdown-trigger').dropdown({ alignment:'top' })

    // $('.datepicker').datepicker();
  //   $('.datepicker').pickadate({
  //       selectMonths: true, // Creates a dropdown to control month
  //         selectYears: 15, // Creates a dropdown of 15 years to control year
  //         format: 'dd-mm-yyyy',
  //         min:new Date(moment()),
  //         max:new Date(moment().add(2,'days'))
  //     });
  // });


  //  var datepickerx = document.getElementsByClassName('dumbo');
  // for (var i = 0; i < datepickerx.length; i++) {
  //     datepickerx[i].value = moment().format('D MMMM,YYYY')
});
</script>
  <script type="text/javascript">

  const CATEGORY_NAME  = document.getElementById('main-cat-name').value;
    function calculate($el)
    {
        let productID = $el.getAttribute('data-product-id')
        let productPrice = $el.getAttribute('data-product-price')
        counterPrice(productPrice,productID,$el.value)

    }
    function getServiceOptions($el)
    {
      let serviceType = $el.options[$el.selectedIndex].value;
      let pID = $el.getAttribute('data-product-id')
      let serviceOptionsContainer = document.getElementById(`service-type-options-${pID}`)
      serviceOptionsContainer.style.display = "block"
      let serviceOptions = document.getElementById(`service-type-sub-${pID}`)

      while (serviceOptions.firstChild) {
          serviceOptions.removeChild(serviceOptions.firstChild);
      }
      var noneSelection = document.createElement('option')
      noneSelection.value = '0'
      noneSelection.text = "Service Option"
      noneSelection.selected = true
      serviceOptions.appendChild(noneSelection);
      $('select').trigger('contentChanged')

      var newOption = "";
      // axios.get("{{ URL::to('/') }}/getServiceTypes",{
      //   params:{
      //     id:serviceType
      //   }
      // }).then( function(response){
      //     response.data.forEach(function(st){
      //         newOption = document.createElement('option');
      //         newOption.value = st.id;
      //         newOption.text = st.name;
      //         serviceOptions.appendChild(newOption)
      //         $('select').trigger('contentChanged')
      //         // console.log(newOption)
      //     });
      //
      // })

     }

    function increment($el)
    {
      let productID = $el.getAttribute('data-product-id')
      let price =  parseFloat($el.getAttribute('data-product-price')).toFixed(2)

      let quantityCalculator = document.getElementById(`assignQuantity-${productID}`)
      quantityCalculator.stepUp(1);
      counterPrice(price,productID,quantityCalculator.value)


    }

    function counterPrice(price,product_id,quantity)
    {
      var counter = document.getElementById(`count-total-${product_id}`)
       let priceMade = document.getElementById(`priceMade-${product_id}`)

       let SellingUnitName = document.querySelector(`#sellingUnit_of_product-${product_id}`).value.toLowerCase();


       if(SellingUnitName === "pieces" || SellingUnitName === "piece"){
         let sellingUnitAvgWeight = document.querySelector(`#avg_weight_of_product-${product_id}`).value;
         let calc = parseFloat(  ((parseFloat(price).toFixed(2)) * (parseFloat(quantity).toFixed(2))) * parseFloat(sellingUnitAvgWeight).toFixed(2) ).toFixed(2)
         let calculatedPirce =  `$ ${price} X ${quantity} X ${sellingUnitAvgWeight} Kg = $ ${calc}`;

         priceMade.value = calc;
         counter.innerHTML = calculatedPirce;
         document.getElementById(`quantity-${product_id}`).value = quantity

       }else {

         let calc = parseFloat((parseFloat(price).toFixed(2)) * parseFloat(quantity).toFixed(2)).toFixed(2)
         let calculatedPirce =  '$ ' + price + ' X ' + quantity + ' = $ '+calc;
         priceMade.value = calc;
         counter.innerHTML = calculatedPirce;
         document.getElementById(`quantity-${product_id}`).value = quantity


       }




      // if(quantity.split('.').length == 1)
      // {
      // }
      // if(quantity.split('.').length == 2 && CATEGORY_NAME!="whole bird")
      // {
      //   let total = parseInt(price) * parseFloat(quantity);
      //   counter.innerHTML = `${price} X ${quantity} = $ ${total.toFixed(2)} `
      // }
    }

    function decrement($el)
    {
      let productID = $el.getAttribute('data-product-id')
      let quantityCalculator = document.getElementById(`assignQuantity-${productID}`)
      let price =  parseFloat($el.getAttribute('data-product-price')).toFixed(2)
      quantityCalculator.stepDown(1);
      counterPrice(price,productID,quantityCalculator.value)

    }

    function assignQuantity($el)
    {
      // calculate($el)
    }
    function addToCart($el)
    {

      let product_id = $el.getAttribute('data-product-id');
      let quantity = document.getElementById(`quantity-${product_id}`).value;
      let serviceOption = document.getElementById(`service-type-sub-${product_id}`)
      let serviceOptionsSelection = 0


      if(serviceOption.options.length > 0)
      {
         serviceOptionsSelection = $(`#service-type-sub-${product_id}`).val();
      }else
      {
        serviceOptionsSelection = 0
      }

      let priceMade = document.getElementById(`priceMade-${product_id}`).value



      let additionalDescription = ''

      toastr.options.timeOut = 3000;

      axios.post(`{{URL::to('/')}}/addToCart`,{
        product_id,
        quantity,
        priceMade,
        serviceOptionsSelection,
        additionalDescription,
        _token:"{{ csrf_token() }}"
      }).then(response => {
        if(response.data.status == "200")
        {
          $('.cart-counter').text(response.data.cartSize)
          $('.cart-counter').addClass('animated heartBeat').one('animationend webkitAnimationEnd oAnimationEnd',function(){
              $(this).removeClass('animated heartBeat');
          });
          toastr.success('Added To Cart Successfully!');
        }
        else if(response.data.status == "420")
        {
          toastr.error('Added Quantity Must Be Betweeen 1 and 100','Failed To Add To Cart');
        }
        else if(response.data.status == '422')
        {
          toastr.error(`Please Select The TimeSlot! `);
        }
        else if(response.data.status == '421')
        {

          toastr.error(`Please Choose Service! Which Service Option You Want `);
        }
        else
        {
          toastr.error('Unexpected Error Occured Try Again Later!');
        }
      })
    }

    function moveToCard($el)
    {
      let p_id = $el.getAttribute('data-product-id')
      let elementSelect = $(`#activator-${p_id}`);
      let offset = elementSelect.offset()
      $('html').scrollTop(offset.top)
      elementSelect.trigger('click')
    }


    // $('input[type="range"]').rangeslider({
    //   // Feature detection the default is `true`.
    //     // Set this to `false` if you want to use
    //     // the polyfill also in Browsers which support
    //     // the native <input type="range"> element.
    //     polyfill: false,
    //
    //     // Default CSS classes
    //     rangeClass: 'rangeslider',
    //     disabledClass: 'rangeslider--disabled',
    //     horizontalClass: 'rangeslider--horizontal',
    //     fillClass: 'rangeslider__fill',
    //     handleClass: 'rangeslider__handle',
    //
    //     // Callback function
    //     onInit: function() {
    //       $rangeEl = this.$range;
    //       // add value label to handle
    //       var $handle = $rangeEl.find('.rangeslider__handle');
    //       var handleValue = '<div class="rangeslider__handle__value">' + this.value + '</div>';
    //       $handle.append(handleValue);
    //
    //       // get range index labels
    //       var rangeLabels = this.$element.attr('labels');
    //       rangeLabels = rangeLabels.split(', ');
    //
    //       // add labels
    //       $rangeEl.append('<div class="rangeslider__labels"></div>');
    //       $(rangeLabels).each(function(index, value) {
    //         $rangeEl.find('.rangeslider__labels').append('<span class="rangeslider__labels__label">' + value + '</span>');
    //       })
    //     },
    //
    //     // Callback function
    //     onSlide: function(position, value) {
    //       var $handle = this.$range.find('.rangeslider__handle__value');
    //       $handle.text(this.value);
    //     },
    //
    //     // Callback function
    //     onSlideEnd: function(position, value) {}
    // });

  </script>

  @endsection

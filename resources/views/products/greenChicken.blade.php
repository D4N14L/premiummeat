@extends('layouts.app')


@section('content')
  <?php $backgroundColor = \App\FrontendCMS::get()->first()->backgroundColor; ?>

  <script type="text/javascript" src="{{ URL::to('/js/axios.js') }}"></script>

<div class="container">
  <div class="row" style="padding-top:5%;margin-bottom:0px">

    <center>
      <div class="">
        <br><br>
        <p class="flow-text pink-text" style="color:{{$backgroundColor}} !important">
          Showing Products of <strong style="font-weight:800">Free Range Chicken</strong> Category
          <div class="divider pink" style="background-color:{{$backgroundColor}} !important;width:25%;height:1px;border-radius:15px"></div>
        </p>
      </div>

    </center>
  </div>
  <div class="row">
    <div class="col s12 m12 l12">
        <a style="background-color:{{$backgroundColor}} !important" href="{{ url()->previous() }}" class="btn btn-floating pink left"> <i class="fa fa-arrow-circle-left white-text"></i> </a>
    </div>

  <div class="row">
      <div class="col s12 m6 l6">
          <div class="card">
            <div class="card-image">
                <img style="object-fit:cover" src="{{ URL::to('/assets/category/'.\App\ProductCategory::where('name','=','Green Chicken Whole Bird')->first()->image) }}" alt="">

         </div>
         <div class="card-content">
           <span class="orange white-text z-depth-2" style="width:auto;height:25px;padding-left:12px;padding-right: 12px;border-radius:25px;padding-top:4px;padding-bottom:4px"> Free Range Chicken Whole Bird</span>
             <a style="background-color:{{$backgroundColor}} !important" href="{{ route('user.browse.by.category',['id' => \App\ProductCategory::where('name','=','Green Chicken Whole Bird')->first()->id]) }}" role="button" class="btn  right pink"> View </a>
             <br>
         </div>

          </div>
      </div>
      <div class="col s12 m6 l6">
        <div class="card">
          <div class="card-image">
            <img style="object-fit:cover;" src="{{ URL::to('/assets/category/'.\App\ProductCategory::where('name','=','Green Chicken Cut Pieces')->first()->image) }}" alt="">
          </div>
          <div class="card-content">
            <span class="left orange white-text z-depth-2" style="width:auto;height:25px;padding-left:12px;padding-right: 12px;border-radius:25px;padding-top:2px">
              Free Range Chicken Portions
            </span>
              <a style="background-color:{{$backgroundColor}} !important" href="{{ route('user.browse.by.category',['id' => \App\ProductCategory::where('name','=','Green Chicken Cut Pieces')->first()->id]) }}" role="button" class="btn  right pink"> View  </a>
              <br>
          </div>
        </div>
      </div>

  </div>









</div>


@endsection

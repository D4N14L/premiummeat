<script>
    function changeColor($el)
    {
        let name = $el.getAttribute('data-name');
        let url = `{{ URL::to('/extra/icons/red/${name}.svg') }}`;
        $el.src = url;
    }

    function clearColor($el)
    {
        let name = $el.getAttribute('data-name');
        let url = `{{ URL::to('/extra/icons/${name}.svg') }}`;
        $el.src = url;
    }


    function productPage($el)
    {
      let name = $el.childNodes[1].getAttribute('data-name');
      let url = "{{ URL::to('/product') }}/"+name;
      window.location.href = url;
    }

    $('#custom-owl').owlCarousel({
        margin:4,
        center:false,
        items:5,
        nav:true,
        navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"]
    });

    $('.owl-carousel').owlCarousel({
        margin:5,
        center:false,
        scrollOverflow:true,
        loop:1,
        responsive:{
            400:{
                items:5
            }
        },
        nav:true,
        navText : ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"]

    });


    // function makeActive($el)
    // {
    //   let dataName = $el.getAttribute('data-name');
    //   let menus = document.getElementsByClassName('menu-item-child');
    //   let currentHref = $el.getAttribute('data-href')
    //   for(let i =0; i< menus.length;i++){
    //     if(menus[i].getAttribute('data-name') != dataName)
    //     {
    //       menus[i].className = "pink-text menu-item-child"
    //     }
    //     else
    //     {
    //       menus[i].className = "pink-text menu-item-child active"
    //     }
    //   }
    //
    //   window.location.href = currentHref;
    //
    //
    // }
    $('#fullpage').fullpage({
        controlArrows:false,
        showActiveTooltip:true,
        navigation:true,
        anchors:['carousel', 'products', 'sausages','steak','footer','footer-last'],
        slidesNavigation:true,
        // afterRender:function(){
        //     setInterval(function(){
        //       $.fn.fullpage.moveSlideRight()
        //
        //     },1500)
        // },
        onLeave:function(index,nextIndex,direction){

            if(nextIndex == 7)
            {
              document.getElementsByClassName('navbar-logo')[0].style.display = "none";
            }
            else
            {
              document.getElementsByClassName('navbar-logo')[0].style.display = "block";

            }
            // if(nextIndex == 3 || nextIndex == 4 || nextIndex == 5 || nextIndex == 7)
            // {
            //     document.getElementById('responsive-logo').style.display = 'none';
            // }
            // else
            // {
            //     document.getElementById('responsive-logo').style.display = 'block';
            // }
        }
    });
    function productChangeHandler($el)
    {
      $('.button-collapse').sideNav('hide');

    }


    </script>
  <script>
      (function ($) {
      $(function () {

          $("#left-dots").sideNav({ edge:'right', closeOnClick: true });
          $('.fixed-action-btn').openFAB()
      }); // end of document ready
  })(jQuery); // end of jQuery name space

  </script>

<?php $backgroundColor = \App\FrontendCMS::get()->first()->backgroundColor; ?>

<div  class="section footer-black"  style="background-color:{{$backgroundColor}}" data-anchor="footer">
  <?php
  $footer = \App\Portions::where('id','=',3)->first();
  ?>
  <style media="screen">
    .dynamic-margin
    {
      margin-top:20px;
    }
    .dynamic-font
    {
      font-size:20px;
    }
    @media only screen and (max-width: 768px)
    {
      .dynamic-margin
      {
        margin-top: 60px;
      }
      .dynamic-font
      {
        font-size:14px;
      }
    }
  </style>
        <div class="row" >
            <div class="col s12 m6 l6 dynamic-margin">
                <h5 style="color:white;font-weight:100;font-size:42px;font-family:Great Vibes;text-align:left" >About Us <br>
                  <hr style="background-color:white;height:1px;border:none;width:19%;float:left"><br>
                </h5>

              <p class="flow-text white-text dynamic-font" style="text-align:justify;margin-top:20px;display:block;" id="meat-board-text-font">
                  {{ $footer->body }}
              </p>

            </div>
            <div class="col s12 m6 l6">
                <center>
                        <img src="{{ URL::to('/extra/meat-bord.png') }}" height="400px" id="meat-board"  alt="">

                </center>
            </div>
        </div>
        {{-- <center>
          <a href="{{URL::to('/#footer-last')}}">
            <div class="scroll-down" style="position: absolute;left:45%;top:73%;z-index:100;color:#fff;background-color:rgb(194, 30, 86,0.35);width:120px;height:120px;border-radius:100%;">
                            <svg width="40px" style="margin:0 auto" height="100%"  viewBox="0 0 247 390" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="fill-rule:evenodd;clip-rule:evenodd;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:1.5;">
                                    <path id="wheel" d="M123.359,79.775l0,72.843" style="fill:none;stroke:#fff;stroke-width:20px;"/>
                                    <path id="mouse" d="M236.717,123.359c0,-62.565 -50.794,-113.359 -113.358,-113.359c-62.565,0 -113.359,50.794 -113.359,113.359l0,143.237c0,62.565 50.794,113.359 113.359,113.359c62.564,0 113.358,-50.794 113.358,-113.359l0,-143.237Z" style="fill:none;stroke:#fff;stroke-width:20px;"/>
                            </svg><br>
                            <span style="color:rgb(194, 30, 86);font-size:18px;color:white;text-shadow:0px 0px 1px white" class="white-text">Scroll down</span>

            </div>
          </a>
        </center> --}}

    </div>

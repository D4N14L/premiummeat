@extends('layouts.app')


@section('content')
  <?php $backgroundColor = \App\FrontendCMS::get()->first()->backgroundColor; ?>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <script src="https://cdn.jsdelivr.net/npm/clipboard@2/dist/clipboard.min.js"></script>
  <script src="https://unpkg.com/tippy.js@3/dist/tippy.all.min.js"></script>

  <div class="fixed-action-btn">
     <a class="btn-floating btn-large" href="{{route('user.get.edit.profile')}}" style="background-color:{{$backgroundColor}} !important">
       <i class="fa fa-pencil"></i>
     </a>
   </div>
    <div class="hide-on-large-only">
        <br><br><br><br>
    </div>
  <div class="container" style="padding-top:5%;margin-bottom:0px;">

    <div class="row">
      @if(Auth::user()->isVerified == 0)
        <div class="col s12 m12 l12">
          <div class="card z-depth-2">
            <div class="card-content red" style="background-color:{{$backgroundColor}} !important">
                    <p class="flow-text" style="font-size:14px;color:white;font-weight:bold">
                    <i class="fa fa-envelope" style="font-size:24px"></i> &nbsp;
                     Confirmation mail was sent to  <b>{{ Auth::user()->email }} </b>. &nbsp;Unverified accounts are expired after 2 business days
                    </p>
          </div>


        </div>

      </div><br>
      @endif
      @if(count(session()->get('cart')) > 0)
        <div class="col s12 m12 l12">
          <div class="card z-depth-2">
            <div class="card-content pink" style="background-color:{{$backgroundColor}} !important">
                    <p class="flow-text" style="font-size:14px;color:white;font-weight:bold">
                    <i class="fa fa-shopping-cart" style="font-size:24px"></i> &nbsp;
                    You Have Pending Items In Cart, Please Proceed To Checkout To Grab That Items, <a href="/checkout" style="border-radius:25px;text-transform:none" class="btn white black-text right">Click Here</a>
                    </p>
          </div>

        </div>

        </div>

      @endif
      <div class="col s12 m3 l3">
        <div class="card">
            <div class="card-content">
              <div class="row">
                <div class="col s4 l4 m4">
                  <p >
                    <i class="fa fa-shopping-cart" style="font-size:32px"></i>
                  </p>
                </div>
                <div class="col s8 m8 l8">
                  <p  style="text-align:center">
                    <center>
                      <span style="font-weight:bold">

                        Total Orders
                      </span>
                      <span class="white-text pink" style="background-color:{{$backgroundColor}} !important;width:25px;height:25px;border-radius:100%;padding:2px;display:block;text-align:center">
                        {{ \App\Order::where('customer_id','=',Auth::user()->id)->get()->count() }}
                      </span>

                    </center>
                  </p>

                </div>
              </div>
              <p>
                <i class="fa fa-info-circle grey-text"></i>

                <span style="font-size:12px;" class="grey-text"> Total  Orders</span>
              </p>

            </div>
        </div>

      </div>









      <div class="col s12 m3 l3">
        <div class="card">
            <div class="card-content">
              <div class="row">
                <div class="col s4 l4 m4">
                  <p >
                    <i class="fa fa-users" style="font-size:32px"></i>
                  </p>
                </div>
                <div class="col s8 m8 l8">
                  <p  style="text-align:center">
                    <center>
                      <span style="font-weight:bold">

                        Total Referals
                      </span>
                      <span class="white-text pink" style="background-color:{{$backgroundColor}} !important;width:25px;height:25px;border-radius:100%;padding:2px;display:block;text-align:center">
                        {{ \App\Referal::where('referer_id','=',Auth::user()->id)->get()->count() }}
                      </span>

                    </center>
                  </p>

                </div>
              </div>
              <p>
                <i class="fa fa-info-circle grey-text"></i>

                <span style="font-size:12px;" class="grey-text"> Total  Referals</span>
              </p>

            </div>
        </div>

      </div>









      <div class="col s12 m3 l3">
        <div class="card">
            <div class="card-content">
              <div class="row">
                <div class="col s4 l4 m4">
                  <p >
                    <i class="fa fa-paper-plane" style="font-size:32px"></i>
                  </p>
                </div>
                <div class="col s8 m8 l8">
                  <p  style="text-align:center">
                    <center>
                      <span style="font-weight:bold">
                         Delivered
                      </span>
                      <span class="white-text pink" style="background-color:{{$backgroundColor}} !important;width:25px;height:25px;border-radius:100%;padding:2px;display:block;text-align:center">
                        {{ \App\Order::where('customer_id','=',Auth::user()->id)->where('order_status','=',1)->count() }}
                      </span>

                    </center>
                  </p>

                </div>
              </div>
              <p>
                <i class="fa fa-info-circle grey-text"></i>

                <span style="font-size:12px;" class="grey-text"> Delivered Orders</span>
              </p>

            </div>
        </div>

      </div>



      <div class="col s12 m3 l3">
        <div class="card">
            <div class="card-content">
              <div class="row">
                <div class="col s4 l4 m4">
                  <p >
                    <i class="fa fa-clock" style="font-size:32px"></i>
                  </p>
                </div>
                <div class="col s8 m8 l8">
                  <p  style="text-align:center">
                    <center>
                      <span style="font-weight:bold">
                         Pending
                      </span>
                      <span class="white-text pink" style="background-color:{{$backgroundColor}} !important;width:25px;height:25px;border-radius:100%;padding:2px;display:block;text-align:center">
                        {{ \App\Order::where('customer_id','=',Auth::user()->id)->where('order_status','=',0)->count() }}
                      </span>

                    </center>
                  </p>

                </div>
              </div>
              <p>
                <i class="fa fa-info-circle grey-text"></i>

                <span style="font-size:12px;" class="grey-text"> Delivered Orders</span>
              </p>

            </div>
        </div>

      </div>

      <div class="col s12 m12 l12">
        <div class="card z-depth-2">
          <div class="card-content red" style="background-color:{{$backgroundColor}} !important">
                  <p class="flow-text" style="font-size:14px;color:white;font-weight:bold">
                  <i class="fa fa-arrow-up" style="font-size:24px"></i> &nbsp; <span style="color:white;font-size:16px">Promotion Link</span>
                  <br><br>
                  <p class="z-depth-1" style="padding:12px 24px;border-radius:25px;text-shadow:#000 1px 1px 2px ;font-size:13px;color:white;word-wrap:break-word;display:block" id="referal_link_text">
                    {{URL::to('/')}}/{{ Auth::user()->referral_link }}
                  </p><br><br>
                  <button  type="button" style="color:white !important;background-color:{{$backgroundColor}} !important;border-radius:25px;font-size:bold" id="trigger-copy" data-clipboard-action="copy" data-clipboard-target="#referal_link_text" class="right z-depth-2 btn white black-text" name="button">Click To Copy</button>
                      <br><br>
                  </p>

        </div>


      </div>

      </div><br>









    </div>
    <div class="row card">
      <div class="col s12 m12 l12 card-content">
        <h5 class="pink-text" style="color:{{$backgroundColor}} !important;font-weight:bold">
          Your Orders
        </h5>
        <br>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>  #
              </th>
              <th>
                Status
              </th>
              <th>
                Amount
              </th>
              <th>
                Placed On
              </th>
              <th>
                Time Passed (ago)
              </th>

            </tr>
          </thead>
          <tbody>
            <?php $i=0; ?>
            @foreach (\App\Order::where('customer_id','=',Auth::user()->id)->get() as $order)
              <?php $i++; ?>
              <?php $amount = 0; ?>
              <tr>
                <td class="font-weight-medium">
                  {{ $i }}
                </td>
                <td>
                  <i>{!! $order->order_status == 0 ? '<i style="color:orange">Pending</i>':'<i style="color:green">Completed</i>' !!}</i>
                </td>
                <td>
                  @foreach ($order->OrderInfo() as $orderInfo)
                    <?php $amount += $orderInfo->amount; ?>
                  @endforeach
                    $ {{ $amount }}
                </td>
                <td>
                  {{ $order->created_at->format('l d, F') }}
                </td>
                <td>
                  {{ $order->created_at->diffForHumans() }}
                </td>

              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <div class="row card">
      <div class="col s12 m12 l12 card-content">
        <h5 class="pink-text" style="font-weight:bold;color:{{$backgroundColor}} !important">
          Your Referals
        </h5>
        <br>
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>  #
              </th>
              <th>
                Name
              </th>
              <th>
                Email
              </th>
              <th>
                Last Order
              </th>
              <th>
                Refered On (ago)
              </th>
              <th>
                Refered On (Date)
              </th>

            </tr>
          </thead>
          <tbody>
            <?php $i=1; ?>
    @foreach (\App\Referal::where('referer_id','=',Auth::user()->id)->get() as $referal)
      <tr>
        <td>{{$i}}</td>
        <td>{{'@'.$referal->info()->name}}</td>
        <td>{{ $referal->info()->email }}</td>
        <td>{!! $referal->getLastOrder() !!}</td>
        <td>{{  $referal->info()->created_at->diffForHumans()  }}</td>
        <td>{{  $referal->info()->created_at->format('D M Y')  }}</td>
      </tr>
      <?php $i++; ?>
    @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>




</div>










<script type="text/javascript">
let clipboard =   new ClipboardJS('#trigger-copy')

clipboard.on('success',function(e){
  let tip = tippy('#trigger-copy',{
    content:'Copied!',
    arrow:true,
    arrowType:'round',
    size:'large',
    duration:'100',
    animation:'shift-away'
  })
})

</script>












@endsection


    @include('userManager.header')

    <script src="https://cdn.jsdelivr.net/npm/clipboard@2/dist/clipboard.min.js"></script>
    <script src="https://unpkg.com/tippy.js@3/dist/tippy.all.min.js"></script>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:partials/_sidebar.html -->      <!-- partial -->
      <div class="main-panel" style="width:100% !important;">
        
        <div class="content-wrapper">
          <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
              <div class="card" style="display:none" id="change-password">
                <div class="card-body">
                  <form action="{{ route('change.password') }}" method="post">
                    <a  onclick="hidePasswordChanger()" style="width:25px;height:25px;border-radius:100%;background-color:white">

                      <i class="float-right"  style="color:black;font-size:18px;">&times;</i>
                    </a>

                    <div class="row">
                            {{ csrf_field() }}
                        <div class="col-6 form-group">
                          <input type="text" required style="padding:14px;color:#ddd" name="password" class="form-control" placeholder="Enter New Password" value="">

                        </div>
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary" >Change Password</button>
                        </div>
                </div>

              </form>
              </div>
              </div>

              @if(Auth::user()->isVerified == 0)
                <div class="card"
                style="
                background: #1d4350; /* fallback for old browsers */
      background: -webkit-linear-gradient(to right, #1d4350, #a43931); /* Chrome 10-25, Safari 5.1-6 */
      background: linear-gradient(to right, #1d4350, #a43931); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
      color:white;
      text-shadow: 1px 1px 1px rgba(0,0,0,0.4);
                "
                 >
                  <div class="card-body blue">
                      <div class="row">
                        <div class="col-12">
                          <p>
                          <i class="mdi mdi-email icon-lg"></i>  An Activation & Confirmation Has Beenn Sent to  <b>( {{ Auth::user()->email }} )</b>, Please verify Your Email by checking your inbox. Unverified Account Expires After 2 days of use
                          </p>
                        </div>

                  </div>

                </div>
              </div>

              @endif

              @if(count(session()->get('cart')) > 0)
              <div class="card " style="background-color:#e65251 !important;color:white">
                <div class="card-body">
                    <div class="row">
                      <div class="col-12">
                        <p>
                          You Have Pending Orders In The Cart Which Are Yet To Be Purchased, Please Proceed To Checkout For Payment Of These Products
                        </p>
                         <a role="button" href="{{ route('checkout') }}" class="btn btn-primary pull-right"> <i class="fa fa-shopping-cart"></i> Proceed To Checkout </a>
                      </div>

                </div>

              </div>
            </div>
            @endif
            <br>
              <div class="card"
              style="
              background: #134e5e; /* fallback for old browsers */
               background: -webkit-linear-gradient(to left, #134e5e, #71b280); /* Chrome 10-25, Safari 5.1-6 */
               background: linear-gradient(to left, #134e5e, #71b280); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */                color:white
              "
              >
                <div class="card-body">




                    <div class="row">
                      <div class="col-6">
                          <i class="mdi mdi-arrow-up icon-lg"></i>
                        <span style="font-size:18px;font-weight:100">Promotion Link </span>

                      </div>
                      <div class="col-6">
                        <b id="referal_link_text">{{URL::to('/')}}/refer/{{ str_replace('/','.',Auth::user()->referral_link) }}</b>
                      </div>
                    </div>
                    <button type="button" id="trigger-copy" data-clipboard-action="copy" data-clipboard-target="#referal_link_text" class="float-right btn btn-success" name="button">Click To Copy</button>


                </div>

              </div>














            </div>
          </div><br>
          <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics"
              style="
              background: #7474bf; /* fallback for old browsers */
                background: -webkit-linear-gradient(to left, #7474bf, #348ac7); /* Chrome 10-25, Safari 5.1-6 */
                background: linear-gradient(to left, #7474bf, #348ac7); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */;color:white;
              "
              >
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-cube text-danger icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Total Referrals</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0">{{ \App\Referal::where('referer_id','=',Auth::user()->id)->get()->count() }}</h3>
                      </div>
                    </div>
                  </div>
                  <p class="text-muted mt-3 mb-0" style="color:white !important">
                    <i class="mdi mdi-alert-octagon mr-1"  aria-hidden="true"></i>Discount On Referrals
                  </p>
                </div>
              </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 grid-margin stretch-card">
              <div class="card card-statistics"
              style="
              background: #7474bf; /* fallback for old browsers */
                background: -webkit-linear-gradient(to left, #7474bf, #348ac7); /* Chrome 10-25, Safari 5.1-6 */
                background: linear-gradient(to left, #7474bf, #348ac7); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */;color:white;
              "
              >
                <div class="card-body">
                  <div class="clearfix">
                    <div class="float-left">
                      <i class="mdi mdi-receipt text-warning icon-lg"></i>
                    </div>
                    <div class="float-right">
                      <p class="mb-0 text-right">Orders</p>
                      <div class="fluid-container">
                        <h3 class="font-weight-medium text-right mb-0">{{ \App\Order::where('customer_id','=',Auth::user()->id)->get()->count() }}</h3>
                      </div>
                    </div>
                  </div>
                  <p class="text-muted mt-3 mb-0" style="color:white !important">
                    <i class="mdi mdi-bookmark-outline mr-1"  aria-hidden="true"></i> Your Total Orders
                  </p>
                </div>
              </div>
            </div>
                  </div>
          {{-- <div class="row">
              @include('userManager/weather')
              @include('userManager/performanceCard')
          </div> --}}
          <div class="row">
            <div class="col-md-12 grid-margin">
              <div class="card"
              style="
              background: #feac5e; /* fallback for old browsers */
               background: -webkit-linear-gradient(to left, #feac5e, #c779d0, #4bc0c8); /* Chrome 10-25, Safari 5.1-6 */
               background: linear-gradient(to left, #feac5e, #c779d0, #4bc0c8); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */                color:white !important;
              "
              >
              <?php

                $pendingOrdersAll =  \App\Order::where('customer_id','=',Auth::user()->id)->where('order_status','=',0)->count();
                $completedOrdersAll = \App\Order::where('customer_id','=',Auth::user()->id)->where('order_status','=',1)->count();
                $totalReferalsOrdersAll = \App\Referal::where('referer_id','=',Auth::user()->id)->count();
                $totalOrders = \App\Order::where('customer_id','=',Auth::user()->id)->count();
              ?>
                <div class="card-body">
                  <div class="chart-container row">
                    <center>
                      <h2> <i class="mdi mdi-all-inclusive mr-1"  aria-hidden="true"></i>   Your Analytics  <br>
                       </h2>
                    </center><br><br><br>
                    @if($pendingOrdersAll != 0 || $completedOrdersAll != 0 || $totalReferalsOrdersAll != 0 || $totalOrders != 0)
                      <canvas id="dashboard-area-chart" height="120"></canvas>
                    @else
                      <center>
                        <p id="dashboard-area-chart" style="padding-top:82px;font-size:24px">
                          <i class="mdi mdi-emoticon-sad mr-1"  aria-hidden="true" style="font-size:80px"></i><br>

                            You have No Analytics To Be Displayed Yet! <br> Order Something or Make Referals To Gain Analaytics Ability
                        </p>


                      </center>
                    @endif
                    <input type="hidden" id="pending-orders" name="" value="{{ $pendingOrdersAll }}">
                    <input type="hidden" id="completed-orders" name="" value="{{ $completedOrdersAll }}">
                    <input type="hidden" id="total_referals" name="" value="{{ $totalReferalsOrdersAll }}">
                    <input type="hidden" id="total_orders" name="" value="{{ $totalOrders }}">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Orders</h4>
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th>  #
                          </th>
                          <th>
                            Status
                          </th>
                          <th>
                            Amount
                          </th>
                          <th>
                            Placed On
                          </th>
                          <th>
                            Time Passed (ago)
                          </th>

                        </tr>
                      </thead>
                      <tbody>
                        <?php $i=0; ?>
                        @foreach (\App\Order::where('customer_id','=',Auth::user()->id)->get() as $order)
                          <?php $i++; ?>
                          <?php $amount = 0; ?>
                          <tr>
                            <td class="font-weight-medium">
                              {{ $i }}
                            </td>
                            <td>
                              <i>{!! $order->order_status == 0 ? '<i style="color:orange">Pending</i>':'<i style="color:green">Completed</i>' !!}</i>
                            </td>
                            <td>
                              @foreach ($order->OrderInfo() as $orderInfo)
                                <?php $amount += $orderInfo->amount; ?>
                              @endforeach
                                $ {{ $amount }}
                            </td>
                            <td>
                              {{ $order->created_at->format('l d, F') }}
                            </td>
                            <td>
                              {{ $order->created_at->diffForHumans() }}
                            </td>

                          </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  <h5 class="card-title mb-4">Referrals</h5>
                  <div class="fluid-container">
                    @foreach (\App\Referal::where('referer_id','=',Auth::user()->id)->get() as $referal)
                      <div class="row ticket-card mt-3">
                        <div class="col-md-1">
                          <img class="img-sm rounded-circle mb-4 mb-md-0" src="images/faces/face3.jpg" alt="profile image">
                        </div>
                        <div class="ticket-details col-md-9">
                          <div class="d-flex">
                            <p class="text-dark font-weight-semibold mr-2 mb-0 no-wrap"> {{ '@'.$referal->info()->name }}</p>
                            <p class="mb-0 ellipsis">{{ $referal->info()->email }}</p>
                          </div>
                          {{-- <p class="text-gray ellipsis mb-2">Nulla quis lorem ut libero malesuada feugiat. Proin eget tortor risus. Lorem ipsum dolor sit amet.</p> --}}
                          <div class="row text-gray d-md-flex d-none">
                            <div class="col-8 d-flex">
                              <small class="mb-0 mr-2 text-dark">Last Ordered :</small>
                              <small class="Last-responded mr-2 mb-0 text-dark">{!! $referal->getLastOrder() !!}</small>
                              <small class="Last-responded mr-2 mb-0 text-dark" style="font-wight:bold" >.</small>
                              <small class="mb-0 mr-2 text-dark">Registered</small>
                              <small class="Last-responded mr-2 mb-0 text-dark"><i class="text-success">{{  $referal->info()->created_at->diffForHumans()  }}</i></small>

                            </div>
                          </div>
                        </div>
                        {{-- <div class="ticket-actions col-md-2">
                          <div class="btn-group dropdown">
                            <button type="button" class="btn btn-success dropdown-toggle btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Manage
                            </button>
                            <div class="dropdown-menu">
                              <a class="dropdown-item" href="#">
                                <i class="fa fa-reply fa-fw"></i>Quick reply</a>
                              <a class="dropdown-item" href="#">
                                <i class="fa fa-history fa-fw"></i>Another action</a>
                              <div class="dropdown-divider"></div>
                              <a class="dropdown-item" href="#">
                                <i class="fa fa-check text-success fa-fw"></i>Resolve Issue</a>
                              <a class="dropdown-item" href="#">
                                <i class="fa fa-times text-danger fa-fw"></i>Close Issue</a>
                            </div>
                          </div>
                        </div> --}}
                      </div>
                    @endforeach




                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <footer class="footer">
          <div class="container-fluid clearfix">
            <span class="text-muted d-block text-center text-sm-left d-sm-inline-block">Copyright © 2018
              <a href="http://www.theteksol.com/" target="_blank">DanielOX</a>. All rights reserved.</span>
            <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Hand-crafted & made with
              <i class="mdi mdi-heart text-danger"></i>
            </span>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->

  <!-- plugins:js -->
  <script src="vendors/js/vendor.bundle.base.js"></script>
  <script src="vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="js/off-canvas.js"></script>
  <script src="js/misc.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  {{-- @include(public_path('/user/js/dashboard')) --}}
  <!-- End custom js for this page-->
</body>

</html>

<script type="text/javascript">
(function ($) {
  'use strict';
  $(function () {
    if ($('#dashboard-area-chart').length) {
      let total_pending_orders = document.getElementById('pending-orders').value;
      let total_completed_orders = document.getElementById('completed-orders').value;
      let total_referals = document.getElementById('total_referals').value;
      let total_orders = document.getElementById('total_orders').value;
        // console.log(orderData)
      var lineChartCanvas = $("#dashboard-area-chart").get(0).getContext("2d");
      var data = {
        labels: ['Pending Order','completed Orders','Total Referals','Total Orders'],
        datasets: [{
            data: [total_pending_orders,total_completed_orders,total_referals,total_orders],
            backgroundColor: ['rgba(0, 128, 207, 0.4)','rgba(0, 0, 123, 0.4)','rgba(128, 0, 207, 0.4)','rgba(207, 128, 0, 0.4)'],
            borderWidth: 1,
            fill: true
          }


        ]
      };
      var options = {
        responsive: true,
        maintainAspectRatio: true,
        scales: {
          yAxes: [{
            display: false
          }],
          xAxes: [{
            display: false,
            ticks: {
              beginAtZero: true
            }
          }]
        },
        legend: {
          labels: {
               fontColor: "white",
               fontSize: 18,
           },
          display: true
        },
        elements: {
          point: {
            radius: 3
          }
        },
        layout: {
          padding: {
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
          }
        },
        stepsize: 2
      };
      var lineChart = new Chart(lineChartCanvas, {
        type: 'doughnut',
        data: data,
        options: options
      });
    }
  });
})(jQuery);







// (function ($) {
  // $(function () {
//     if ($('#dashboard-area-chart').length) {
//         // console.log(orderData)
//       var lineChartCanvas = $("#dashboard-area-chart-2").get(0).getContext("2d");
//       var data = {
//         labels: [],
//         datasets: [{
//             label: 'Referal',
//             data: [orderData],
//             backgroundColor: 'rgba(0, 128, 207, 0.4)',
//             borderWidth: 1,
//             fill: true
//           }
//         ]
//       };
//       var options = {
//         responsive: true,
//         maintainAspectRatio: true,
//         scales: {
//           yAxes: [{
//             display: false
//           }],
//           xAxes: [{
//             display: false,
//             ticks: {
//               beginAtZero: true
//             }
//           }]
//         },
//         legend: {
//           display: true
//         },
//         elements: {
//           point: {
//             radius: 3
//           }
//         },
//         layout: {
//           padding: {
//             left: 0,
//             right: 0,
//             top: 0,
//             bottom: 0
//           }
//         },
//         stepsize: 2
//       };
//       var lineChart = new Chart(lineChartCanvas, {
//         type: 'bar',
//         data: data,
//         options: options
//       });
//     }
//   });
// })(jQuery);


let clipboard =   new ClipboardJS('#trigger-copy')

clipboard.on('success',function(e){
  let tip = tippy('#trigger-copy',{
    content:'Copied!',
    arrow:true,
    arrowType:'round',
    size:'large',
    duration:'100',
    animation:'shift-away'
  })
})

// function clickToCopy()
// {
//   // let toBeCopied = document.getElementById('referal_link_text')
// clipboard.on('failed',function(e){
//   console.info('Action',e.action)
// })
//
  //  toBeCopied.select()
  // document.execCommand('copy')
  // console.log(toBeCopied.value)
// }
  function showPasswordChanger()
  {
      document.getElementById('change-password').style.display = "block";
  }

  function hidePasswordChanger()
  {
    document.getElementById('change-password').style.display = "none";
  }


</script>

{{-- <script type="text/javascript">

</script> --}}

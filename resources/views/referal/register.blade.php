@extends('layouts.app')

@section('content')
<style media="screen">
  body{
    overflow-y: scroll;;
  }
  label
  {
    color:#e91e63 !important;

  }
  @if(url()->current() == URL::to('/').'/register')
  #logo-main
  {
    display: none;
  }
  @endif
  .invalid-feedback{
    color:red;
  }
  .invalid-feedback::before{
    content:'*';
  }
</style>
<div class="container">
  <div id="eula" class="modal">
      <div class="modal-content"  style="word-wrap:break-word;text-align:justify !important;;">
        <p >
           {!! \App\Portions::where('portion_1','=',5)->first()->body !!}

        </p>
      </div>
      <div class="modal-footer" >
        <a style="position:absolute;top:0%;right:0%" href="#!" class="modal-close waves-effect  pink btn-floating"> <i class="fa fa-times"></i> </a>
      </div>
    </div>

    <div class="row justify-content-center" style="padding-top:10%">
        <div class="col-md-8">
            <div class="card">
                <div class="card-content">
                  <a href="{{ url()->previous() }}" class="btn btn-floating pink"> <i class="fa fa-arrow-circle-left white-text"></i> </a>
                  <center>
                    <div class="navbar-logo  z-depth-2 hide-on-med-and-down">
                    </div>
                  </center><br>

                    <center>
                      <br>
                      <div class="card-title pink-text"> <h5 style="font-size:14px;">{{ __('Join the best meat seller') }} <br>                      </h5>
                        <div class="divider pink" style="width:10%;">

                        </div>
                     </div>
                   </center>
                    {{-- @if(isset($messages))
                      @foreach ($messages as $message)
                         {{ $message }}
                      @endforeach

                    @endif --}}

                    <form onsubmit="checkForEULA(this,event)" method="POST" action="{{ route('register.post.referal') }}">
                        @csrf
                        <input type="hidden" name="referer_id" value="{{ $referer_id }}">
                        <div class=" row">

                            <div class="input-field col s12 m6 l6">
                              <label for="name">{{ __('Name') }}</label>
                                <input id="name" type="text" class="{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col l6 m6 s12 input-field">
                              <label for="email">{{ __('E-Mail Address') }}</label>
                                <input id="email" type="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class=" row">

                            <div class="input-field col s12 m4 l4">
                              <label for="name">{{ __('Address') }}</label>
                                <input id="address" type="text" class="{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') }}" required>

                                @if ($errors->has('address'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col l4 m4 s12 input-field">
                              <label for="email">{{ __('Landline') }}</label>
                                <input id="landline" type="text" class="{{ $errors->has('landline') ? ' is-invalid' : '' }}" name="landline" value="{{ old('landline') }}" required>

                                @if ($errors->has('landline'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('landline') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col l4 m4 s12 input-field">
                                  <select name="ethnic" required>
                                    <option value="NZ European">NZ European</option>
                                    <option value="Chinese">Chinese</option>
                                    <option value="Arabs ">Arabs</option>
                                    <option value="Pakistanies ">Pakistanies</option>
                                    <option value="Indians ">Indians</option>
                                    <option value="Fiji Indians ">Fiji Indians</option>
                                    <option value="Afghanies ">Afghanies</option>
                                    <option value="Bangalies ">Bangalies</option>
                                    <option value="Pacific Islands">Pacific Islands</option>
                                  </select>
                                  <label>{{ __('Ethnic') }}</label>

                                @if ($errors->has('ethnic'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ethnic') }}</strong>
                                    </span>
                                @endif
                            </div>


                        </div>

                        <div class=" row">

                            <div class="col l6 m6 s12 input-field">
                              <label for="password">{{ __('Password') }}</label>
                                <input id="password" type="password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col m6 l6 s12 input-field">
                              <label for="password-confirm" >{{ __('Confirm Password') }}</label>
                                <input id="password-confirm" type="password" class="" name="password_confirmation" required>
                            </div>
                        </div>
                        <div>
                          <input onchan type="checkbox" name="isAgreementSigned" id="check">
                          <label for="check" >Click here to indicate that you have read and agreed to the terms of the &nbsp;  </label><a  data-target="eula"class="modal-trigger" >End User License Agreement ( EULA )</a>
                        </div>
                        <br>
                        <div class=" row ">
                            <div class="col-md-6 offset-md-4">
                                <button style="margin-bottom:12px;width:210.64px" type="submit" class="btn pink white-text left" >
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>


<style media="screen">
  ul.dropdown-content.select-dropdown li span {
      color: #e91e63; /* no need for !important :) */
  }
</style>


<script type="text/javascript">
  $(document).ready(function(){
    $('select').material_select();
  });
  function checkForEULA($el,event)
  {
    event.preventDefault()
    if(!(document.getElementById('check').checked))
    {
      alert('You Have To Agree To Our Terms And License');
    }
    else
    {
      $el.submit();
    }

  }

</script>











@endsection

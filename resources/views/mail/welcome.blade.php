<img src="{{ URL::to('/') }}/extra/logo.png" style="background-image:url({{URL::to("/extra/logo.png")}});width:250px;height: 250px;background-size:contain;background-repeat: no-repeat;background-position: center center;box-shadow: 1px 1px 2px 2px rgba(0,0,0,.35);border-radius: 100%;background-repeat:no-repeat;" t="">
<p class="flow-text" style="font-size:18px">
  Warm Welcome <b>{{ $user->name }}</b>, <br><br> Thanks for registering with us. <br><br> Premium Meats we are as good as it sounds. Premium Meats is one of fastest growing retail and wholesale meat supplier in Auckland because we offer the service and quality that has never been experienced.
</p><br><br>
<center>
<p style="font-size:18px;font-weight:100" class="flow-text pink-text" >
  Please Verify Your Account, By Clicking the button below
</p><br>
<br>
<a href="{{ route('email.verify',['id' => $user->verification_token]) }}" style="padding:12px 24px;text-decoration:none;background-color:orange;color:white;border:none">Verify Account</a>
<br>
</center>

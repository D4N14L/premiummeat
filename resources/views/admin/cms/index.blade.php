@extends('voyager::master')


@section('page_header')
  <h1 class="page-title">
      <i class="voyager-campfire"></i>
      <p> {{ 'CMS' }}</p>
  </h1>
  <span class="page-description">{{ 'Manage Contents Of Your Site From Here' }}</span>
  @include('toaster')
@endsection


@section('content')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://cdn.ckeditor.com/4.7.1/standard/ckeditor.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.green.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
  <script
    src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
    integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30="
    crossorigin="anonymous"></script>
  <script type="text/javascript" src="{{URL::to('/')}}/js/jquery.touch.handler.sortable.min.js">

  </script>
  <script type="text/javascript" src="{{URL::to('/')}}/js/axios.js"></script>
  <style media="screen">
    .owl-carousel:hover
    {
        cursor: grab !important;
    }
  </style>
  <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
          <h3>Manage Sliders</h3>
          <form onchange="this.submit()" class="pull-right" action="{{ route('cms.slider.add') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
              <input type="file" class="form-control" name="slide" value="">
            </div>
          </form>


        </div>
        <div class="container">
          <a href="{{ route('cms.manage.slides') }}" role="button" class="btn btn-info" > <i class="voyager-pencil"></i> Manage Multiple Slides </a>

        </div>
      </div>
        <div class="owl-carousel owl-theme">
        @foreach (\App\Slider::orderBy('id','desc')->get() as $slider)
          <img  style="object-fit:cover;width:100%;height:300px;box-shadow:1px 1px 1px #000" src="{{ URL::to('/sliders/') }}/{{$slider->image}}" alt="">
        @endforeach
      </div><br>
        {{-- // sortable --}}

      <div class="container">
        <div class="justify-content-center">

          <h3>Sort / Organize Slides</h3>
          <hr style="background-color:#eee;">

          <table class="table table-striped table-hover table-border">
            <tbody id="sortable">
                @foreach (\App\Slider::orderBy('arrange')->get() as $slider)
                  <tr style="border:1px solid #ddd;" data-id="{{$slider->id}}" data-position="{{$slider->arrange}}">
                    <td># {{$slider->arrange}}</td>
                    <td><img  style="float:right;object-fit:cover;width:50px;height:50px;box-shadow:1px 1px 1px #000;" src="{{ URL::to('/sliders/') }}/{{$slider->image}}" alt=""></td>
                </tr>
                @endforeach
            </tbody>
          </table>

        </div>
      </div>

      <div class="container">
        <div class="justify-content-center">

          <h3>Minimum Pirce & Delivery Charges</h3>
          <hr style="background-color:#eee;">
          <?php
                $miscellaneous = \App\Miscellaneous::first();
                $minimumAmount = $miscellaneous->minimumAmount;
                $deliveryCharges = $miscellaneous->deliveryCharges;
           ?>
          <form action="{{ route('updateMiscellaneous') }}" method="post">
            {{ csrf_field() }}
              <div class="form-group col-sm-12 col-lg-4 col-md-4">
                <label for="minimumAmount">Min. Amount For Shopping</label>
                <input id="minimumAmount" class="form-control" type="number" step="0.1" min="0" name="minimumAmount" placeholder="Minmum Amount" value="{{ number_format((float) $minimumAmount,1,'.','') }}">
              </div>
              <div class="form-group col-sm-12 col-lg-4 col-md-4">
                <label for="deliveryCharges">Delivery Charges</label>
                <input id="deliveryCharges" class="form-control" placeholder="Delivery Charges" type="number" step="0.1" min="0" name="deliveryCharges" value="{{ number_format((float) $deliveryCharges,1,'.','') }}">
              </div>
              <div class="form-group col-sm-12 col-lg-4 col-md-4"> <br>
                <button type="submit" class="btn btn-info" name="button">Save Minimum Pirce & Delivery Charges</button>
              </div>

          </form>
        </div>
        <hr>
      </div>
      {{-- sortable  // --}}

      {{-- <hr style="background-color:#000;border:1px solid #ddd"> --}}
      {{-- <hr style="background-color:#000;border:1px solid #ddd"> --}}
      <div class="row" >
        <h3>Manage Colors Of Website</h3>
        <div class="row">
          <div class=" col col-sm-12 col-lg-4 co-md-4" style="margin:0px;text-align:center">
            <h5>Choose Primary Color</h5>
          </div>
          <div class="col-sm-12 col-lg-4 co-md-4" style="margin:0px;text-align:center">
            <h5>Chosen Color</h5>
          </div>

        </div>

        <hr>
        <?php $bg = \App\FrontendCMS::get()->first()->backgroundColor; ?>
          <div class="col-sm-12 col-lg-4 col-md-4">
            <form  action="{{route('cms.website.color')}}" method="post">
              {{ csrf_field() }}
              <input type="text" class="form-control" style="margin-top:10px" placeholder="Enter Text Color Value e.g #aa" name="backgroundColor" value="{{$bg}}">
              <br>
              <button type="submit" style="background-color:{{$bg}}" class="btn btn-primary">Change Color</button>
            </form>
          </div>
          <div class="col-sm-12 col-lg-4 col-md-4" style="border-left:2px solid #eee;
border-left:2px solid #eee;width:200px;margin-left:100px">
          <p style="color:#aaa;font-weight:bold;text-align:center">
            {{ $bg }}
          </p>
            <div style="width:50px;height:50px;margin:0 auto;background-color:{{$bg}};border-radius:100%">

            </div>
          </div>
      </div>
      <div class="row" >
        <hr>

        <h3>Manage Home Page Content</h3>
        <hr>
        <?php
          $portions = \App\Portions::all();
        ?>
          <div class="col-sm-12 col-lg-4 col-md-4">
            <h5>Portion 1</h5>
            <hr>

            <form class="" action="{{ route('cms.update.portion') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" name="portion_id" value="{{ $portions[0]->id }}">
                <div class="form-group">
                  <label for="">Title</label>
                    <input type="text" class="form-control" name="title" placeholder="Enter Title" value="{{ $portions[0]->title }}">
                </div>
                <div class="form-group">
                  <label for="">Body</label>
                    <textarea name="body" class="form-control" rows="8" cols="80">{{ $portions[0]->body }}</textarea>
                </div>
                    <img src="{{ URL::to('/extra') }}/{{$portions[0]->image}}"  height="200" style="object-fit:cover;width:100%" alt="">

                <div class="form-group">
                  <input type="file" class="form-control" name="image" value="">
                </div>
                <button type="submit" class="pull-right btn btn-info"> <i class="voyager-pen"></i> Update</button>
            </form>

          </div>






          <div class="col-sm-12 col-lg-4 col-md-4" style="border-left:1px solid #eee">
            <h5>Portion 2</h5>
            <hr>
            <form class="" action="{{ route('cms.update.portion') }}" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <input type="hidden" name="portion_id" value="{{ $portions[1]->id }}">
              <div class="form-group">
                <label for="">Title</label>
                  <input type="text" class="form-control" name="title" placeholder="Enter Title" value="{{ $portions[1]->title }}">
              </div>
              <div class="form-group">
                <label for="">Body</label>
                  <textarea name="body" class="form-control" rows="8" cols="80">{{ $portions[1]->body }}</textarea>
              </div>
              <img src="{{ URL::to('/extra') }}/{{$portions[1]->image}}"  height="200" style="object-fit:cover;width:100%" alt="">

              <div class="form-group">
                <input type="file" class="form-control" name="image" value="">
              </div>
              <button type="submit" class="pull-right btn btn-info"> <i class="voyager-pen"></i> Update</button>
            </form>

          </div>






          <div class="col-sm-12 col-lg-4 col-md-4" style="border-left:1px solid #eee" >
            <h5>Portion 3</h5>
            <hr>

            <form class="" action="{{ route('cms.update.portion') }}" method="post" enctype="multipart/form-data">
              {{ csrf_field() }}
              <input type="hidden" name="portion_id" value="{{ $portions[2]->id }}">
              <div class="form-group">
                  <label for="">Title</label>
                  <input type="text" class="form-control" name="title" placeholder="Enter Title" value="{{ $portions[2]->title }}">
              </div>
              <div class="form-group">
                <label for="">Body</label>
                  <textarea name="body" class="form-control" rows="8" cols="80">{{ $portions[2]->body }}</textarea>
              </div>
              <i>No Image is Needed For This Portion</i>
<br>
              <button type="submit" class="pull-right btn btn-info"> <i class="voyager-pen"></i> Update</button>
            </form>

          </div>

      </div>

      <div class="row">
        <div class="col-sm-12 col-lg-12 col-md-12" >
          <h5>Manage Footer Data</h5>
          <hr>
          <form action="{{route('cms.manage.post.footer')}}" method="post">
            {{ csrf_field() }}
            <?php
            $footerCMS = \App\FooterCMS::where('id','=','1')->first();

            ?>
          <div class="form-group">
            <textarea name="description" class="form-control" rows="8" cols="80">{{ $footerCMS->description }}</textarea>
          </div>
        </div>
        <div class="col-sm-3 col-md-3 col-lg-3">
            <div class="form-group">
              <label for="address">Address</label>
              <input id="address" type="text" class="form-control" name="address" value="{{ $footerCMS->address }}">
            </div>
        </div>


        <div class="col-sm-3 col-md-3 col-lg-3">
            <div class="form-group">
              <label for="phone">Phone</label>
              <input id="phone" type="text" class="form-control" name="phone" value="{{ $footerCMS->phone }}">
            </div>
        </div>


        <div class="col-sm-3 col-md-3 col-lg-3">
            <div class="form-group">
              <label for="landline">Landline</label>
              <input id="landline" type="text" class="form-control" name="landline" value="{{ $footerCMS->landline }}">
            </div>
        </div>

        <div class="col-sm-3 col-md-3 col-lg-3">
            <div class="form-group">
              <label for="email">Email</label>
              <input id="email" type="text" class="form-control" name="email" value="{{ $footerCMS->email }}">
            </div>
        </div>
        <button type="submit" class="btn btn-primary float-right" >Upadte Footer's Data</button>
      </form>

      </div>


      <div class="row">
        <div class="col-sm-12 col-lg-12 col-md-12" >
          <h5>EULA ( End User License Agreement )</h5>
          <hr>

            <form class="" action="{{ route('cms.update.portion') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" name="portion_id" value="{{ $portions[3]->id }}">
                {{ csrf_field() }}
              <div class="form-group">
                <label for="">Body</label>
                  <textarea id="body-eula" name="body" class="form-control" rows="8" cols="100">{{ $portions[3]->body }}</textarea>
              </div>
              <br>
              <button type="submit" class="pull-right btn btn-info"> <i class="voyager-pen"></i> Update</button>
            </form>
        </div>
      </div>






  </div>


<script type="text/javascript">
$('.owl-carousel').owlCarousel({
    margin:5,
    center:true,
    scrollOverflow:false,
    loop:1,
    responsive:{
        400:{
            items:2
        }
    }
});
CKEDITOR.replace( 'body-eula' );
$('#sortable').sortable({
  update:function(event,ui){
    $(this).children().each(function( index ){
      if($(this).attr('data-position') != (index+1))
      {
        $(this).attr('data-position',(index+1)).addClass('updated');
      }
    });

    saveNewPositions();

  }
});

function saveNewPositions()
{
  let positions = []
  $('.updated').each(function(index){
      positions.push([$(this).attr('data-id'),$(this).attr('data-position')])
      $(this).removeClass('updated');
  });

  axios.post("{{route('cms.update.slider.position')}}",{
    positions,
    _token:"{{csrf_token()}}",
    _method:"post"
  }).then(function(response){
    if(response.data.status == '200')
    {
      toastr.success('Slides Arranged Successfully');
    }else
    {
      toastr.error('Failed to arrange sliders')
    }

  }).catch(function(error){
    toastr.error('Failed to arrange sliders')
  })
}
</script>
@endsection

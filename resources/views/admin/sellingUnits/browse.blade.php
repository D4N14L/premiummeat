@extends('voyager::master')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-pie-chart"></i>
        <p> {{ 'Selling Unit' }}</p>
    </h1>
    <span class="page-description">{{ 'Product Selling Unit' }}</span>

@endsection

@section('content')
<div class="container">
  <div class="page-content">

    <div class="row">
      <form id="submissionForm" class="" action="{{ route('sellingUnit.store') }}" method="post">
          {{ csrf_field() }}
          {{ method_field('post') }}
          <div class="col-sm-12 col-md-6 col-lg-6 form-group">
              <label for="name">Selling Unit Name</label>
              <input id="name" type="text" class="form-control" style="padding:24px" name="name" placeholder="e.g Kg" required/><br>
              <button id="edit-close-btn" type="reset" onclick="switchToAddMode(this)" style="display:none" class="btn btn-danger pull-left"> <i class="voyager-pirate-swords"></i> Cancel </button>

              <button id="add-submit-btn" type="submit" class="btn btn-success pull-left"> <i class="voyager-plus"></i> Add Selling Unit </button>

          </div>

              <input id="checker" type="hidden" name="isEdit" value="0" />
              <input type="hidden" name="sellingUnit_id" id="productID" />
      </form>

    </div>

<style media="screen">
  thead,th,tr,td
  {
    text-align: center;
    font-weight:500 !important;
  }
</style>
<style media="screen">
table{
    font-size:12px !important;
  }

</style>

    <table class="table table-responsive table-striped table-bordered">
      <thead>
        <th>Name</th>
        <th>Action</th>
      </thead>
      <tbody>
        @foreach($sellingUnit as $cat)
          <tr id="edit-{{$cat->id}}" data-id="{{$cat->id}}">
            <td>{{ $cat->name }}</td>
            <td>
              <a data-id="edit-{{ $cat->id }}" onclick="editCategory(this)" class="btn btn-info">Edit</a>
              <a href="#" onclick="redirectAndWarn(this)" data-id="{{ $cat->id }}" class="btn btn-danger">Delete</a>
            </td>

          </tr>
        @endforeach

      </tbody>
    </table>

    <center>
      {!! $sellingUnit->render() !!}

    </center>
  </div>


</div>



    <script type="text/javascript">
        function editCategory($el)
        {
          var category = document.getElementById($el.getAttribute('data-id'));
          var catName = category.childNodes[1].innerHTML;
          var editObj = {
            catName,
            productID:category.getAttribute('data-id')
          }
          switchToEditMode(editObj);
        }

        function switchToEditMode(editObj)
        {
          document.getElementById('name').value = editObj.catName;
          document.getElementById('add-submit-btn').innerHTML = '<i class="voyager-edit"></i> Update Record'
          document.getElementById('add-submit-btn').className = "btn btn-info"
          document.getElementById('checker').value = 1
          document.getElementById('edit-close-btn').style.display = "block"
          document.getElementById('edit-close-btn').style.marginRight = "5px"
          document.getElementById('productID').value = editObj.productID
        }
        function switchToAddMode($el)
        {
           $el.style.display = "none"
           document.getElementById('add-submit-btn').className = "btn btn-success"
           document.getElementById('add-submit-btn').innerHTML = '<i class="voyager-plus"></i> Add Record'
           document.getElementById('checker').value = 0

        }

    </script>
    <script type="text/javascript">
      // ERROR
        function redirectAndWarn($el)
        {
          let url = `{{URL::to('admin/sellingUnits/delete/')}}/${$el.getAttribute('data-id')}`;

              if(window.confirm('Caution! This will delete all the related products'))
              {
                window.location.href =  url
              }

        }
    </script>







@endsection

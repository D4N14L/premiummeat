@extends('voyager::master')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-dot-3"></i>
        <p> {{ 'Service Typ Options' }}</p>
    </h1>
    <span class="page-description">{{ 'Service Type Options' }}</span>
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
      {!! Toastr::render() !!}

@endsection

@section('content')
  <style media="screen">
  table{
      font-size:12px !important;
      font-weight:400 !important;
    }

  </style>
<div class="container">

  <div class="page-content">

    <div class="row">

      <form action="{{ route('serviceTypeOptions.store')  }}" id="add-service-name" method="post">

        {{ csrf_field() }}

      <div class="col-sm-12 col-lg-4 col-md-4  ">

            <div class="form-group">

              <label for="name">Service Option Name</label>

              <input id="name" type="text" name="name" placeholder="Service Option Name" value=""  class="form-control"/>

              <input type="hidden" name="is_edit" value="0" />

            </div>

      </div>

      <div class="col-sm-12 col-lg-4 col-md-4  ">

            <div class="form-group">

              <label for="name">Select Service Type</label>
              <select class="form-control" name="service_type_id">
                <option value="" disabled selected>Select Service Type</option>
                @foreach (\App\ServiceType::orderBy('name','asc')->get() as $st)
                  <option value="{{ $st->id }}">{{ $st->name }}</option>
                @endforeach
              </select>

            </div>

      </div>


      <div class="col-sm-12 col-lg-4 col-md-4">

        <br>

        <div class="form-group">

          <button type="submit"  class="btn btn-success">

            <i class="voyager-plus"></i> Add Service Options Type

          </button>

        </div>

      </div>

    </form>







    <form action="{{ route('serviceTypeOptions.edit')  }}" style="display:none"  id="form-edit" method="post">

      {{ csrf_field() }}

    <div class="col-sm-12 col-lg-4 col-md-4  ">

          <div class="form-group">

            <label for="name">Service Name</label>

            <input id="name-edit" type="text" name="name" placeholder="Edit Service Name" class="form-control" />

          </div>

            <input type="hidden" name="is_edit" value="1" />

            <input type="hidden" id="service_id_input" name="service_id" />
    </div>
    <div class="col-sm-12 col-lg-4 col-md-4  ">

          <div class="form-group">

            <label for="name">Select Service Type</label>
            <select id="service-selector" class="form-control" name="service_type_id">
              <option value="" disabled selected>Select Service Type</option>
              @foreach (\App\ServiceType::orderBy('name','asc')->get() as $st)
                <option value="{{ $st->id }}">{{ $st->name }}</option>
              @endforeach
            </select>

          </div>

    </div>



    <div class="col-sm-12 col-lg-4 col-md-4">

      <br />

      <div class="form-group">

        <button type="submit"  class="btn btn-primary">

          <i class="voyager-pen"></i>&nbsp;Edit Service Type

        </button>

        <button type="button" class="btn btn-primary" onclick="addServiceName()" >

          <i class="voyager-plus"></i>&nbsp;Switch To Add Mode

        </button>

      </div>

    </div>

  </form>


























    </div>

    <hr>
    <div style="float:right">
      {!! $serviceOptionsTypes->render() !!}

    </div>
    <div class="row">

      <h4>

        Viewing Service Options Added By You

      </h4>

      <br>

      <div class="col-sm-12 col-md-12 col-lg-12">

        <table class="table table-responsiver table-bordered table-striped">

          <thead>

            <th>Name</th>
            <th>Service</th>
            <th>Action</th>

          </thead>

          <tbody>
            @if($serviceOptionsTypes->count() == 0 )

              <tr>

                <td colspan="3"> <i style="color:red"> No ServiceType Option Found! </i> </td>

              </tr>

            @else

              @foreach ($serviceOptionsTypes as $st)

                <tr>

                  <td> {{  $st->name   }} </td>
                  <td>
                    {{ $st->getServiceType()->name }}
                  </td>
                  <td>

                    <button type="button" data-service-id="{{ $st->id }}" data-service-type-id="{{ $st->service_type_id }}"  data-service-name="{{ $st->name }}"  onclick="switchToEditMode(this)"  class="btn btn-success">

                      <i class="voyager-pen"></i>

                    </button>

                      <a href="{{ route('serviceTypeOptions.delete' , ['id' => $st->id ]) }}" type="button" style=" text-decoration : none " class="btn btn-danger" role="button"> <i class="voyager-trash"></i> </a>

                  </td>

                </tr>

              @endforeach

            @endif

          </tbody>

        </table>

      </div>

    </div>






  </div>
</div>


  <script type="text/javascript">















      function switchToEditMode($el)
      {

        var formEdit =  document.getElementById('form-edit')
        formEdit.style.display = "block"

        var postService = document.getElementById('add-service-name')
        postService.style.display = "none"

        document.getElementById('name-edit').value = $el.getAttribute('data-service-name')
        document.getElementById('service_id_input').value = $el.getAttribute('data-service-id')

        let selector = document.getElementById('service-selector');
        for (var i = 0; i < selector.options.length; i++) {
          if(parseInt(selector.options[i].value) == parseInt($el.getAttribute('data-service-type-id')) )
          {
            selector.options[i].selected = true;
          }
        }












      }

      function addServiceName()
      {
        var postService = document.getElementById('add-service-name');
        postService.style.display = "block"

        let formEdit =  document.getElementById('form-edit')
        formEdit.style.display = "none"
      }

  </script>


@endsection

@extends('voyager::master')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-category"></i>
        <p> {{ 'Category' }}</p>
    </h1>
    <span class="page-description">{{ 'Product Categories' }}</span>
      @include('toaster')

@endsection

@section('content')
<div class="container">
  <div class="page-content">
    <div class="row">
      <form id="submissionForm" class="" action="{{ route('category.store') }}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          {{ method_field('post') }}
          <div class="col-sm-12 col-md-6 col-lg-6 form-group">
              <label for="name">Category Name</label>
              <input id="name" type="text" class="form-control" style="padding:24px" name="name" placeholder="e.g Chicken" required/><br>
              <button id="edit-close-btn" type="reset" onclick="switchToAddMode(this)" style="display:none" class="btn btn-danger pull-left"> <i class="voyager-pirate-swords"></i> Cancel </button>

              <button id="add-submit-btn" type="submit" class="btn btn-success pull-left"> <i class="voyager-plus"></i> Add Category </button>

          </div>

          <div class="col-sm-12 col-md-3 col-lg-3 form-group" >
            <label for="image">Image</label>
              <input id="imageSubmission" accept="image/*" type="file" onchange="getImage(this)" class="form-control"  name="image" value="" required />
            </div>

            <div class="col-sm-12 col-md-3 col-lg-3 form-group">
              <label>Thumbnail</label>
                <img id="thumbnail" src="https://getuikit.com/v2/docs/images/placeholder_200x100.svg" style="width:100px;height:100px;object-fit:cover;display:block" alt="">

              </div>

              <input id="checker" type="hidden" name="isEdit" value="0" />
              <input type="hidden" name="productId" id="productID" />
      </form>

    </div>

<style media="screen">
  thead,th,tr,td
  {
    text-align: center;
    font-weight:400 !important;

  }
  <style media="screen">
  table{
      font-size:12px !important;
    }

  </style>


</style>

    <table class="table table-responsive table-striped table-bordered">
      <thead>
        <th>Name</th>
        <th>Image</th>
        <th>Action</th>
      </thead>
      <tbody>
        @foreach($category as $cat)
          <tr id="edit-{{$cat->id}}" data-id="{{$cat->id}}">
            <td>{{ $cat->name }}</td>
            <td> <img id="img-{{$cat->id}}" src="{{ URL::to('/assets/category/'.$cat->image) }}" width="60px" height="60px" style="object-fit:cover;" alt=""> </td>
            <td>
              <a data-id="edit-{{ $cat->id }}" onclick="editCategory(this)" class="btn btn-info">Edit</a>
              <a href="{{route('category.delete',['id' => $cat->id])}}" onclick="grantPermission(this,event)" class="btn btn-danger" style="color:white"> Delete </a>
            </td>

          </tr>
        @endforeach

      </tbody>
    </table>

    <center>
      {!! $category->render() !!}

    </center>
  </div>


</div>



    <script type="text/javascript">
        function getImage($el)
        {
          var fileReader = new FileReader();
          fileReader.readAsDataURL($el.files[0])
          fileReader.onload = function()
          {
            document.getElementById('thumbnail').src = fileReader.result;
          }
        }
        function grantPermission($el,event)
        {
          event.preventDefault();
          let confirmation = confirm('Warning! Are You Sure You Want To Delete This Category? In The Result Whole Products Of This Category Will Be Deleted!')
          if(confirmation)
          {
            window.location = $el.href;

          }
        }
        function editCategory($el)
        {
          var category = document.getElementById($el.getAttribute('data-id'));
          var catName = category.childNodes[1].innerHTML;
          var img = document.getElementById(`img-${category.getAttribute('data-id')}`).src;
          var editObj = {
            catName,
            img,
            productID:category.getAttribute('data-id')
          }
          switchToEditMode(editObj);
        }

        function switchToEditMode(editObj)
        {
          window.scrollTo(0,0);
          document.getElementById('name').value = editObj.catName;
          document.getElementById('thumbnail').src = editObj.img;
          document.getElementById('add-submit-btn').innerHTML = '<i class="voyager-edit"></i> Update Record'
          document.getElementById('add-submit-btn').className = "btn btn-info"
          document.getElementById('checker').value = 1
          document.getElementById('edit-close-btn').style.display = "block"
          document.getElementById('edit-close-btn').style.marginRight = "5px"
          document.getElementById('productID').value = editObj.productID
          document.getElementById('imageSubmission').required = false


        }
        function switchToAddMode($el)
        {
           $el.style.display = "none"
           document.getElementById('add-submit-btn').className = "btn btn-success"
           document.getElementById('add-submit-btn').innerHTML = '<i class="voyager-plus"></i> Add Record'
           document.getElementById('thumbnail').src = "https://getuikit.com/v2/docs/images/placeholder_200x100.svg";
           document.getElementById('checker').value = 0
           document.getElementById('imageSubmission').required = true

        }
    </script>







@endsection

@extends('voyager::master')


@section('page_header')
  <h1 class="page-title" >
      <i class="voyager-dashboard"></i>
      <p> {{ 'Dashboard' }}</p>
  </h1>
  <span class="page-description">{{ 'Dashboard Stats' }}</span>
@endsection


@section('content')
<style >
  canvas{
    font-size:12px !important;
    font-weight:400 !important;
  }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css">

<div class="container">
    <div class="page-content" style="font-size:12px">
      <div class="row">
        <div  class="col-sm-12 col-md-6 col-lg-6 card">
          <div class="card-body">
            <h4>Products - Categories Analytics</h4>
            <?php $array = []; ?>
            @foreach ($category as $cat)
              <?php array_push($array,$cat->products()->count()); ?>
            @endforeach


            <canvas id="productsChart" width="300" height="150"></canvas>

          </div>
        </div>
        <div class="col-sm-12 col-md-5 col-lg-5 card" style="margin-left:10px">
          <h4>Users Analytics</h4>
          <?php
          $months = ['1','2','3','4','5','6','7','8','9','10','11','12'];
          $users = [];
          ?>

          @foreach ($months as $month)
              <?php array_push($users,\App\User::whereMonth('created_at','=',$month)->get()->count());  ?>
          @endforeach


          <canvas id="UserChart" width="300" height="200"></canvas>
        </div>

      </div>
      <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 card">
          <div class="card-body">
            <div class="form-group">
              <label for="">Select Category For Inventory Details</label>
              <select class="form-control" onchange="changeCategory(this)" name="cat_id">
                @if(isset($_GET['selectedCategory']))
                  @foreach (\App\ProductCategory::all() as $cat)
                      @if($_GET['selectedCategory'] == $cat->id)
                      <option value="{{ $cat->id }}" selected>{{ $cat->name }}</option>
                    @else
                      <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                    @endif
                  @endforeach
                @else
                  @foreach (\App\ProductCategory::all() as $cat)
                      <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                  @endforeach
               @endif
              </select>

            </div>
            <h4>Inventory Analytics</h4>
            <?php $inventoryArray = ['inventory' => []]; ?>
            @if(!isset($_GET['selectedCategory']))
                @foreach (\App\Inventory::all() as $inventory)
                    <?php   if($inventory->product) {
                      array_push($inventoryArray['inventory'],[$inventory->product->name,$inventory->quantity]);
                    };  ?>
                @endforeach

            @else

                @foreach (\App\Inventory::all() as $inventory)
                <?php
                  if($inventory->product){
                    if($inventory->product->category->id == $_GET['selectedCategory'])
                    {
                        array_push($inventoryArray['inventory'],[$inventory->product->name,$inventory->quantity]);
                    }
                  }
                  ?>
                @endforeach

            @endif




            <canvas id="inventoryChartx" width="300" height="100"></canvas>

          </div>
        </div>

      </div>


















      <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 card">
          <div class="card-body">
            <div class="row">
              <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="form-group">
                  <label for="">Select Category For Sales Analytics</label>
                  <select class="form-control" onchange="changeSaleCategory(this)" name="cat_id">
                    @if(isset($_GET['salesCategory']))
                      @foreach (\App\ProductCategory::all() as $cat)
                          @if($_GET['salesCategory'] == $cat->id)
                          <option value="{{ $cat->id }}" selected>{{ $cat->name }}</option>
                        @else
                          <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                        @endif
                      @endforeach
                    @else
                      @foreach (\App\ProductCategory::all() as $cat)
                          <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                        @endforeach
                   @endif



                  </select>

                </div>
              </div>


              <div class="col-sm-6 col-md-6 co-lg-6">

                <div class="form-group">
                  <label for="date">Date</label>
                  <input type="text" class="form-control" name="dates" />
                </div>



              </div>
            </div>
            <h4>Sales Analytics</h4>

            <canvas id="salesAnalytics" width="300" height="100"></canvas>
            <?php $productsSales = ['sales' => []]; ?>

            <?php
            $order = "";
              // $order = \App\OrderInfo::whereIn('order_id',\App\Order::where('order_status','=',1)->whereMonth('created_at','=','11')->whereYear('created_at','=','2018')->get())->groupBy->get();
              if(isset($_GET['start']) && isset($_GET['end']))
              {
               $SaleOrders = \DB::select('SELECT product_id,count(orders_info.id) as dup_count FROM orders_info  JOIN orders  ON orders.id = orders_info.order_id AND orders.order_status=1 AND orders.created_at  BETWEEN ? AND ? GROUP BY orders_info.product_id',[\Carbon\Carbon::parse($_GET['start'])->toDateTimeString(),\Carbon\Carbon::parse($_GET['end'])->toDateTimeString()]);
              }
              else
              {
                $SaleOrders = \DB::select('SELECT product_id,count(orders_info.id) as dup_count FROM orders_info  JOIN orders  ON orders.id = orders_info.order_id AND orders.order_status=1  GROUP BY orders_info.product_id');
              }
              if(!isset($_GET['salesCategory']))
              {
                foreach ($SaleOrders as $so)
                {
                    $name = \App\Product::where('id','=',$so->product_id)->first()->name;
                    $count = $so->dup_count;
                    array_push($productsSales['sales'],[$name,$count]);
                }

              }
              else
              {
                foreach ($SaleOrders as $so)
                {
                    $result = \App\Product::where('id','=',$so->product_id)->first();
                    if($result->category->id == $_GET['salesCategory'])
                    {
                      $name = $result->name;
                      $count = $so->dup_count;
                      array_push($productsSales['sales'],[$name,$count]);
                    }
                }

              }

            ?>

          </div>
        </div>

      </div>
















    </div>
</div>

































<script type="text/javascript">





$('input[name="dates"]').daterangepicker({},function(start,end,label){
  window.location= '{{ URL::to('/admin') }}'+'?start='+moment.utc(start).format()+'&end='+moment.utc(end).format();

});











  let categories = JSON.parse('{!! json_encode($category->toArray()) !!}').map(function(x){
     return x.name
  });

  let products = JSON.parse('{!! json_encode($array) !!}')


  let ctx = document.getElementById('productsChart').getContext('2d');
  let randomColor = Math.floor(Math.random() * Math.floor(256));
  let randomColor2 = Math.floor(Math.random() * Math.floor(256));
  let randomColor3 = Math.floor(Math.random() * Math.floor(256));
  let chart = new Chart(ctx, {
      // The type of chart we want to create
      type: 'bar',

      // The data for our dataset
      data: {
          labels: categories,
          datasets: [{
              label: "Products - Category",
              backgroundColor: `rgb(${randomColor},${randomColor2},${randomColor2})`,
              borderColor: `rgb(${randomColor},${randomColor2},${randomColor2})`,
              data: products
          }]
      },
      options: {
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
          stepSize:5,
          callback: function(value) {if (value % 1 === 0) {return value;}}
        }
      }]
    }
  }

  });





// ----------------------------------------------------------------------------

//-----------------------------------------------------------------------------





  let inventoryArray = JSON.parse('{!! json_encode($inventoryArray["inventory"]) !!}')


  let Quanities = inventoryArray.map(function(x){
      return x[1];
  });
  let productNames = inventoryArray.map(function(x){
      return x[0];
  });



  var ctx2 = document.getElementById('inventoryChartx').getContext('2d');
  new Chart(ctx2, {

      // The type of chart we want to create
      type: 'bar',

      // The data for our dataset
      data: {
          labels: productNames,
          datasets: [{
              label: "Inventory - Product Quantities",
              backgroundColor: `rgb(${randomColor},${randomColor2},${randomColor2})`,
              borderColor: `rgb(${randomColor},${randomColor2},${randomColor2})`,
              data: Quanities
          }]
      },
      options: {
    scales: {
      yAxes: [{
        ticks: {
          stepSize:50,
          beginAtZero: true,
          callback: function(value) {if (value % 1 === 0) {return value;}}
        }
      }]
    }
  }

  });




















  var users = JSON.parse('{!! json_encode($users) !!}')

  r = Math.floor(Math.random() * 200);
  g = Math.floor(Math.random() * 200);
  b = Math.floor(Math.random() * 200);
  color = 'rgb(' + r + ', ' + g + ', ' + b + ')'
  var ctx3 = document.getElementById('UserChart').getContext('2d');
  new Chart(ctx3, {

      // The type of chart we want to create
      type: 'bar',

      // The data for our dataset
      data: {
          labels:['January','Febraury','March','April','May','June','July','August','September','October','November','December'],
          datasets: [{
              label: "Users",
              backgroundColor: ["#FF4136", "#0074D9", "#2ECC40", "#FF851B", "#7FDBFF", "#B10DC9", "#FFDC00", "#001f3f", "#39CCCC", "#01FF70", "#85144b", "#F012BE", "#3D9970", "#111111", "#AAAAAA"],
              borderColor: `rgb(256,256,256)`,
              data:users
          }]
      },
      options: {
          scales: {
            yAxes: [{
              ticks: {
                beginAtZero: true,
                stepSize:5,
                callback: function(value) {if (value % 1 === 0) {return value;}}
              }
            }]
          }
        }
  });







  // SALES CHART


  var orderSales = JSON.parse(`{!! json_encode($productsSales['sales']) !!}`);
  let productSalesName = orderSales.map(function(x){
      return x[0]
    });

  let productCount = orderSales.map(function(x){
    return x[1];
  });

  var ctx5 = document.getElementById('salesAnalytics').getContext('2d');
  new Chart(ctx5, {

      // The type of chart we want to create
      type: 'bar',

      // The data for our dataset
      data: {
          labels: productSalesName,
          datasets: [{
              label: "Product Sales",
              backgroundColor: `rgb(${randomColor},${randomColor2},${randomColor2})`,
              borderColor: `rgb(${randomColor},${randomColor2},${randomColor2})`,
              data: productCount
          }]
      },

  });





















































  function changeCategory($el)
  {
    let changeCategory = $el.options[$el.selectedIndex].value;
    window.location= '{{ URL::to('/admin') }}'+'?selectedCategory='+changeCategory;

  }

  function changeSaleCategory($el)
  {
      let changeCategory = $el.options[$el.selectedIndex].value;
      window.location= '{{ URL::to('/admin') }}'+'?salesCategory='+changeCategory;

  }















</script>













@endsection

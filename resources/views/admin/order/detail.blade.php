@extends('voyager::master')


@section('page_header')
  <h1 class="page-title">
      <i class="voyager-barbeque"></i>
      <p> {{ 'Order Details' }}</p>
  </h1>
  <span class="page-description">{{ 'Viewing Order Details' }}</span>
  <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

@endsection


@section('content')
  <style media="screen">
  table{
      font-size:12px !important;
      font-weight:400 !important;
    }

  </style>

  <script type="text/javascript" src="{{ URL::to('/js/axios.js') }}"></script>
  <div class="container">
    <div class="page-content">
      <table class="table table-responsive table-striped table-bordered">
        <thead>
          <th>Product Name</th>
          <th>Product Per Unit Price</th>
          <th>Quantity Ordered</th>
          <th>Extra Quantity</th>
          <th>Sub Total ($ NZ) </th>
          <th>Service Type</th>
          <th>Additional Information</th>
        </thead>
        <tbody>
          <?php $i=0; $product_quantities = []; ?>
          @foreach ($OrderInfo as $oi)
            <tr>
              <td>{{ $oi->Product()->name }}</td>
              <td>$ {{ $oi->Product()->price }} / <sup> <b>{{ $oi->Product()->sellingUnit->name }}</td>
              <td id="table-quantity-{{$oi->id}}">
                  {{ $oi->quantity }}
              </td>
              <td>
                <input type="number" class="form-control" oninput="extraQuantityChangeHandler(this)" data-order-detail-id="{{$oi->id}}" data-product-price="{{$oi->product()->price}}"  min="0.0" step="0.1"  value="{{ (float)$oi->extra_quantity}}">
              </td>
              <td class="product-subtotal" id="table-subtotal-{{$oi->id}}">
                {{round(((float) $oi->Product()->price * ((float) $oi->quantity + (float) $oi->extra_quantity)),2)}}
              </td>
                <td>
                @if(count(json_decode($oi->service_type_option)) <= 0)

                    <i style="font-size:12px">{{ 'No Service Requested' }}</i>

                  @else
                    <ol>
                      @foreach (json_decode($oi->service_type_option) as  $serviceOptionItem)
                        <li>
                          {{ \App\ServiceType::where('id','=',$serviceOptionItem)->first()->name  }}</b> </sup>
                        </li>
                      @endforeach
                    </ol>
                @endif
            </td>
              <td>{!! is_null($oi->additional_information) ? '<i style="font-size:12px;">No Additional Info</i>' : $oi->additional_information !!}</td>
            </tr>


            {{-- Previouse Details --}}

            {{-- Original Quantity Of Product --}}
            <input id="prev-quantity-{{$oi->id}}" type="hidden" value="{{$oi->quantity}}">











            <?php $i += round(((float) $oi->Product()->price * ((float) $oi->quantity + (float) $oi->extra_quantity)),2);?>

          @endforeach
          <tr >
            <td colspan="8" style="text-align:right" > <b class="right">Total</b> </td>
          </tr>
          <tr>
            <td colspan="8" style="text-align:right;font-weight:bold" > $ <span id="total-price-holder"><?php echo $i; ?></span> / NZ  </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
@endsection



<script type="text/javascript">

    function extraQuantityChangeHandler($el){

      let detailID = $el.getAttribute('data-order-detail-id');
      let totalPriceHolder = document.querySelector('#total-price-holder');
      let productPirce = $el.getAttribute('data-product-price')
      let ExtraQuantity = parseFloat($el.value);
      let OriginalQuantityElem = document.querySelector(`#prev-quantity-${detailID}`);
      let OriginalQuantity =  parseFloat(OriginalQuantityElem.value);

      let newQuantity = parseFloat(OriginalQuantity + ExtraQuantity).toFixed(2);
      let newProductSubtotal = parseFloat(newQuantity * parseFloat(productPirce)).toFixed(2);

       document.querySelector(`#table-quantity-${detailID}`).innerHTML = newQuantity;
       document.querySelector(`#table-subtotal-${detailID}`).innerHTML = newProductSubtotal
       let recount_subtotal = parseFloat(0);
       document.querySelectorAll('.product-subtotal').forEach(function($ps){
         recount_subtotal += (parseFloat($ps.innerHTML))
       })

       totalPriceHolder.innerHTML = recount_subtotal.toFixed(2);

       let currentOrderID = "{{$OrderInfo->first()->order_id}}";
       axios.post('{{URL::to('/admin/orders/details/')}}/'+currentOrderID,{
         extraQuantity:newQuantity,
         order_detail_id:detailID,
         totalAmount:recount_subtotal,
         currentOrderID:currentOrderID,
         _token:"{{csrf_token()}}",
         _method:"post"
       }).then(({data}) => {
         if(data.status == 200){
           toastr.options.timeOut = 100;
           toastr.success('Total Amount Updated');
         }else {
           toastr.options.timeOut = 100;

           toastr.error('Failed to update the new amount ')
         }
       }).catch(err => {
         toastr.options.timeOut = 100;

         toastr.error('Failed to update the new amount ')
       });





      // OriginalQuantityElem.value = newQuantity;















  }


  // function

</script>

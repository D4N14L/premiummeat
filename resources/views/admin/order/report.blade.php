<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <title>Repport Generation</title>
    <link rel="stylesheet" href="{{public_path()}}/css/materialize.css">
  </head>
  <style media="screen">
  /* thead:before, thead:after { display: none; }
tbody:before, tbody:after { display: none; } */

  .headings
  {
    font-weight: bold;
  }
  tr td
  {
    border:1px solid #aaa;
    text-align:center;
  }
  table
  {
    overflow: hidden;
  }
  </style>
  <body>
    <div style="width:200px;height:200px;border-radius:100%;background-image:url({{ public_path('/extra/logo.png') }});background-size:contain;background-repeat:no-repeat;background-position:center center;">
      <img src="{{ public_path() }}/extra/logo.png" style="width:auto;height:50px;object-fit:contain" alt="">
    </div>

    <div style="margin-top:-100px">
        <center>
          <h5>
            <u>
             Customer Detail
            </u>
  </h5>
        </center>
        <br>
      <table>
        <tr>
          <td><p> <span style="display:block;font-weight:bold">Customer Name:</span> {{ $orders['customer']['name'] }}</p> <br> </td>
          <td><p> <span style="display:block;font-weight:bold">Address:</span> {{ $orders['customer']['address'] }}</p><br></td>
        </tr>
        <tr>
          <td>
            <p> <span style="display:block;font-weight:bold">Contact:</span> {{ $orders['customer']['landline'] }}</p><br>
          </td>
          <td>
            <p> <span style="display:block;font-weight:bold">E-Mail:</span> {{ $orders['customer']['email'] }}</p><br>
          </td>
        </tr>
      </table>
                <br>
                <center>
                  <h5> <u>Order Details</u> </h5>
                </center>
      <br>
      <table border="1" class="table bordered">
        <tr class="headings">
          <td>Product Name</td>
          <td>Order Date & Time</td>
          <td>Delivery timing slot</td>
          <td>Product Unit Price</td>
        </tr>
        <?php $amount=0; ?>
          @foreach ($orders['orderDetails'] as $od)
            <tr>
              <td>{{ \App\OrderInfo::getProduct($od['product_id'])->name }}</td>
              <td>{{ \App\OrderInfo::getOrder($od['order_id'])->created_at->format('D / M / Y, H:M a') }}</td>
              <td>{{ \App\OrderInfo::getProduct($od['product_id'])->timeslot == '0' ? 'General': ucfirst(\App\OrderInfo::getProduct($od['product_id'])->timeslot()->shift)   }}</td>
              {{-- <td>{{ $od['amount']/\App\OrderInfo::getProduct($od['product_id'])->price  }} </td> --}}
              <td>{{ \App\OrderInfo::getProduct($od['product_id'])->price }}</td>
              <?php $amount += (float) $od['amount']; ?>
            </tr>
          @endforeach
          <div class="right" style="text-align:center;font-weight:bold;border:1px solid #aaa;padding:8px 12px 8px 12px;">
            Total Payment <br>
            ${{$amount}}

          </div>
      </table>

    </div>
    <br><br><br>
    <center>
      <a href="http://www.premiummeat.co.nz" style="font-size:14px;">
      <span style="color:black">&copy;</span>&nbsp;<span style="color:#e91e63">premiummeat.co.nz</span>
      </a>

    </center>

  </body>
</html>

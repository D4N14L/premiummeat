@extends('voyager::master')


@section('page_header')
  <h1 class="page-title">
      <i class="voyager-dollar"></i>
      <p> {{ 'Order' }}</p>
  </h1>
  <span class="page-description">{{ 'Charged Orders' }}</span>
  <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>



@section('content')
  <style media="screen">
  table{
      font-size:12px !important;
      font-weight:400 !important;
    }

  </style>

  <style media="screen">
    a{
      text-decoration: none !important;
      outline:none !important;
    }
  </style>
  <script type="text/javascript" src="{{ URL::to('/js/axios.js') }}"></script>
  <div class="container">
    <div class="page-content">
      <div class="row">
        <table class="table table-responsive table-bordered table-striped">
          <thead>
            <th>Customer Name</th>
            <th>Customer Mail</th>
            <th>Customer Address</th>
            <th>Timeslot Requested</th>
            <th>Delivery Status</th>
            <th>Order Date</th>
            {{-- <th>Order Status</th> --}}
            <th>Delivery or Pickup</th>
            <th>Action</th>
            <th>Generate Report</th>
          </thead>
          <tbody>
            @foreach ($order as $o)

              <?php $i=0; ?>
                <tr>
                  <td>{{ $o->User()->name }}</td>
                  <td>{{ $o->User()->email }}</td>
                  <td>{{ $o->User()->address }} </td>
                  <td>
                    {{$o->getTimeSlot()->startTime}} - {{$o->getTimeSlot()->endTime}}&nbsp;<sup style="font-weight:bold">{{$o->getTimeSlot()->shift == "morning" ? "AM" : "PM"}}<sup>

                    {{-- {!! $o->getTimeSlot()->where('id','=',$o->timeslotsSelection)->first() !!} --}}
                  </td>
                  <td>
                    @if($o->order_status == 0)
                      <span style="color:red"> <i>Pending</i> </span>
                    @else
                      <span style="color:green"> <i class="voyager-dollar"></i> <i>Charged</i> </span>
                    @endif
                  </td>
                  <td>
                    {{$o->order_date}}
                  </td>
                  <td>
                    @if($o->isDelivery == 0)
                      <span > <i style="color:orange">Self</i> </span>
                    @else
                      <span> <i style="color:green">Delivery</i> </span>
                    @endif
                  </td>
                  <td>
                    <a class="btn btn-info" href="{{ route('admin.order.details',['id' => $o->id]) }}" > <i class="voyager-eye"></i> Check Order Details </a>
                   </td>
                   <td>
                     <a href="{{ route('admin.order.report.generate',['id' => $o->id]) }}" class="btn btn-danger">
                       <i class="voyager-book"> </i>
                     </a>
                   </td>
                </tr>

            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  @if(session()->has('failed'))
    <script type="text/javascript">
    toastr.error('{!! session()->get('failed') !!}');
    </script>
  @endif
  @if(session()->has('success'))
    <script type="text/javascript">
    toastr.success('{!! session()->get('success') !!}');
    </script>
  @endif
@endsection

@endsection

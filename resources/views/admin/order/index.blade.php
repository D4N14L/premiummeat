@extends('voyager::master')


@section('page_header')
  <h1 class="page-title">
      <i class="voyager-bulb"></i>
      <p> {{ 'Order' }}</p>
  </h1>
  <span class="page-description">{{ 'Browse Order' }}</span>
  <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>



@section('content')
  <style media="screen">
  table{
      font-size:12px !important;
      font-weight:400 !important;
    }

  </style>

  <style media="screen">
    a{
      text-decoration: none !important;
      outline:none !important;
    }
  </style>
  <script type="text/javascript" src="{{ URL::to('/js/axios.js') }}"></script>
  <div class="container">
    <div class="page-content">
      <div class="row">
        <table class="table table-responsive table-bordered table-striped">
          <thead>
            <th>Customer Name</th>
            <th>Customer Mail</th>
            <th>Customer Address</th>
            <th>Timeslot Requested</th>
            <th>Charge Status</th>
            <th>Order Date</th>
            <th>Delivery Type</th>
            <th>Total Amount To Charge</th>
            <th><i class="voyager-credit-cards"></i> Click To Charge</th>
            <th>Order Details</th>
            <th>Order Report</th>
          </thead>
          <tbody>
            @foreach ($order as $o)

              <?php $i=0; ?>
                <tr>
                  <td>{{ $o->User()->name }}</td>
                  <td>{{ $o->User()->email }}</td>
                  <td>{{ $o->User()->address }} </td>
                  <td>
                    {{$o->getTimeSlot()->startTime}} - {{$o->getTimeSlot()->endTime}}&nbsp;<sup style="font-weight:bold">{{$o->getTimeSlot()->shift == "morning" ? "AM" : "PM"}}<sup>

                    {{-- {!! $o->getTimeSlot()->where('id','=',$o->timeslotsSelection)->first() !!} --}}
                  </td>
                  <td>
                    @if($o->order_status == 0)
                      <span style="color:red"> <i>Pending</i> </span>
                    @else
                      <span style="color:green"> <i>Completed</i> </span>
                    @endif
                  </td>
                  <td>
                    {{$o->order_date}}
                  </td>
                  <td>
                    @if($o->isDelivery == 0)
                      <span > <i style="color:orange">Self</i> </span>
                    @else
                      <span> <i style="color:green">Delivery</i> </span>
                    @endif
                  </td>
                  <td>
                    <input id="order_amount_{{$o->id}}" type="number" class="form-control" step="0.1" min="1"  name="total-amount" value="{{$o->total_amount}}">
                  </td>
                  <td>
                    <a onclick="chargeCustomer(this)" data-order-id="{{$o->id}}"  data-customer-id="{{$o->User()->stripeCustomerID}}" class="btn btn-success"> <i class="voyager-credit-cards"></i>&nbsp; Charge </a>
                  </td>
                  <td>
                    <a class="btn btn-info" href="{{ route('admin.order.details',['id' => $o->id]) }}" > <i class="voyager-eye"></i> View </a>
                    {{-- <a role="button" href="{{ route('admin.order.mark',['id' => $o->id]) }}"  class="btn btn-success"> <i class="voyager-check"></i> Mark as Delivered  </a> --}}
                   </td>
                   <td>
                     <a href="{{ route('admin.order.report.generate',['id' => $o->id]) }}" class="btn btn-danger">
                       <i class="voyager-book"> </i>
                     </a>
                   </td>
                </tr>

            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
  @if(session()->has('failed'))
    <script type="text/javascript">
    toastr.error('{!! session()->get('failed') !!}');
    </script>
  @endif
  @if(session()->has('success'))
    <script type="text/javascript">
    toastr.success('{!! session()->get('success') !!}');
    </script>
  @endif
@endsection


<script type="text/javascript">
  function chargeCustomer($el){
      let order_id = $el.getAttribute('data-order-id');
      let customer_id = $el.getAttribute('data-customer-id').trim();
      let total_amount =  document.querySelector(`#order_amount_${order_id}`).value;

      // redirect to charge page
      let toRedirect = '{{ URL::to('/admin/orders/charge/') }}'+`/${customer_id}/${total_amount}/${order_id}`
      window.location.href = toRedirect;

  }
</script>
















@endsection

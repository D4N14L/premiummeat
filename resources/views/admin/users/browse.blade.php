@extends('voyager::master')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-person"></i>
        <p> {{ 'Users' }}</p>
    </h1>
    <span class="page-description">{{ 'Users Information' }}</span>

@endsection

@section('content')
  <style media="screen">
  table{
      font-size:12px !important;
      font-weight:400 !important;
    }

  </style>
<div class="container">
  <div class="page-content">

    {{-- <div class="form-group" >
        <input type="text" class="form-control pull-right" name="" value="" style="width:40%; padding:24px 8px" placeholder="Search User">

        <br>
        <br>

    </div> --}}
    <table class="table table-responsive table-striped table-bordered">
      <thead>
        <th>Name</th>
        <th>Email</th>
        <th>Created At</th>
        <th>Exact Date</th>
        <th>City</th>
        <th>Order</th>
        <th>Order Completed</th>
        <th>Amount Paid</th>
        {{-- <th>Action</th> --}}
      </thead>
        <tbody>
          @foreach($users as $user)
            <tr>
              <td>{{ $user->name }}</td>
              <td>{{ $user->email }}</td>
              <td>{{ \Carbon\Carbon::parse($user->created_at )->diffForHumans()}}</td>
              <td>{{ $user->created_at->format('Y / M / d') }}</td>
              <td>{{ $user->address }}</td>
              <td>{{ $user->getNumberOfOrders() }}</td>
              <td>{{ $user->getOrderCompleted() }}</td>
              <td>$ {{ $user->amountPaid() }} / NZ</td>
              {{-- <td> <a href="{{ route('admin.user.delete',['id' => $user->id]) }}" class="btn btn-danger" style="text-decoration:none;outline:none">Delete</a> </td> --}}
            </tr>

          @endforeach
        </tbody>












    </table>
    <div class="pull-right">
      {!! $users->render() !!}
    </div>
  </div>

</div>
@endsection

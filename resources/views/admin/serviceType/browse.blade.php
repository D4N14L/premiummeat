@extends('voyager::master')

@section('page_header')
    <h1 class="page-title">
        <i class="voyager-polaroid"></i>
        <p> {{ 'Service Type' }}</p>
    </h1>
    <span class="page-description">{{ 'Service Type' }}</span>

@endsection

@section('content')
  <style media="screen">
  table{
      font-size:12px !important;
      font-weight:400 !important;
    }

  </style>
<div class="container">

  <div class="page-content">

    <div class="row">

      <form action="{{ route('serviceType.store')  }}" id="add-service-name" method="post">

        {{ csrf_field() }}

      <div class="col-sm-12 col-lg-4 col-md-4 col-md-offset-2 col-lg-offset-2 ">
        <div class="row">
          <div class="col-sm-12 col-lg-6 col-md-6">
            <div class="form-group">

              <label for="name">Service Name</label>

              <input id="name" type="text" name="name" placeholder="Service Name" value=""  class="form-control"/>

              <input type="hidden" name="is_edit" value="0" />

            </div>
          </div>
          <div class="col-sm-12 col-lg-6 col-md-6">
            <div class="form-group">

              <label for="name">Service For Category</label>
              <select class="form-control" required name="cat_id" >
                <option value="" disabled>Choose Service For Category</option>
                @foreach (\App\ProductCategory::orderBy('name','asc')->get() as $category)
                  <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
              </select>
            </div>

          </div>
        </div>

      </div>

      <div class="col-sm-12 col-lg-4 col-md-4">

        <br>

        <div class="form-group">

          <button type="submit"  class="btn btn-success">

            <i class="voyager-plus"></i> Add Service Type

          </button>

        </div>

      </div>

    </form>







    <form action="{{ route('serviceType.edit')  }}" style="display:none"  id="form-edit" method="POST">

      {{ csrf_field() }}

    <div class="col-sm-12 col-lg-4 col-md-4 col-md-offset-2 col-lg-offset-2 ">

      <div class="row">
        <div class="col-sm-12 col-lg-6 col-md-6">
          <div class="form-group">

            <label for="name">Service Name</label>

            <input id="name-edit" type="text" name="name" placeholder="Edit Service Name" class="form-control" />

          </div>

        </div>
            <div class="col-sm-12 col-lg-6 col-md-6">
              <div class="form-group">

                <label for="name">Service For Category</label>
                <select class="form-control" required name="cat_id" id="service_category_choose">
                  <option value="" disabled>Choose Service For Category</option>
                  @foreach (\App\ProductCategory::orderBy('name','asc')->get() as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                  @endforeach
                </select>
              </div>

            </div>

      </div>









            <input type="hidden" name="is_edit" value="1" />

            <input type="hidden" id="service_id_input" name="service_id" />
    </div>

    <div class="col-sm-12 col-lg-4 col-md-4">

      <br />

      <div class="form-group">

        <button type="submit"  class="btn btn-primary">

          <i class="voyager-pen"></i>&nbsp;Edit Service Type

        </button>

        <button type="button" class="btn btn-primary" onclick="addServiceName()" >

          <i class="voyager-plus"></i>&nbsp;Switch To Add Mode

        </button>

      </div>

    </div>

  </form>


























    </div>

    <hr>

    <div class="row">
      <div style="float:right">
        {!! $serviceTypes->render() !!}

      </div>
      <h4>

        Viewing Services Added By You

      </h4>

      <br>

      <div class="col-sm-12 col-md-12 col-lg-12">

        <table class="table table-responsiver table-bordered table-striped">

          <thead>

            <th>Name</th>
            <th>Service Realted Category</th>
            <th>Action</th>

          </thead>

          <tbody>
            @if($serviceTypes->count() == 0 )

              <tr>

                <td colspan="2"> <i> No ServiceType Found! </i> </td>

              </tr>

            @else

              @foreach ($serviceTypes as $st)

                <tr>

                  <td> {{  $st->name   }} </td>
                  <td>{{ $st->getCategory()->name }}</td>
                  <td>

                    <button type="button" data-service-id="{{ $st->id }}" data-service-cat="{{$st->getCategory()->id}}" data-service-name="{{ $st->name }}"  onclick="switchToEditMode(this)"  class="btn btn-success">

                      <i class="voyager-pen"></i>

                    </button>

                      <a href="{{ route('serviceType.delete' , ['id' => $st->id ]) }}" type="button" style=" text-decoration : none " class="btn btn-danger" role="button"> <i class="voyager-trash"></i> </a>

                  </td>


                </tr>

              @endforeach

            @endif

          </tbody>

        </table>

      </div>

    </div>






  </div>
</div>


  <script type="text/javascript">

      function switchToEditMode($el)
      {

        var formEdit =  document.getElementById('form-edit')
        formEdit.style.display = "block"

        var postService = document.getElementById('add-service-name')
        postService.style.display = "none"
        //
        let categorySelector = document.getElementById('service_category_choose')
        let catID = $el.getAttribute('data-service-cat')

        //
        for (var i = 1; i < categorySelector.options.length; i++) {
          if(categorySelector.options[i].value == catID)
          {
            categorySelector.options[i].selected = true
          }
        }
        console.log()
        //

        document.getElementById('name-edit').value = $el.getAttribute('data-service-name')
        document.getElementById('service_id_input').value = $el.getAttribute('data-service-id')

      }

      function addServiceName()
      {
        var postService = document.getElementById('add-service-name');
        postService.style.display = "block"

        let formEdit =  document.getElementById('form-edit')
        formEdit.style.display = "none"
      }

  </script>


@endsection

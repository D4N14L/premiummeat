@extends('voyager::master')


@section('page_header')
  <h1 class="page-title">
      <i class="voyager-bag"></i>
      <p> {{ 'Products' }}</p>
  </h1>
  <span class="page-description">{{ 'Browse Products' }}</span>
    @include('toaster')
@endsection



@section('content')
  <style media="screen">
  table{
      font-size:12px !important;
      font-weight:400 !important;
    }

  </style>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script
    src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"
    integrity="sha256-T0Vest3yCU7pafRw9r+settMBX6JkKN06dqBnpQ8d30="
    crossorigin="anonymous"></script>
  <script type="text/javascript" src="{{URL::to('/')}}/js/jquery.touch.handler.sortable.min.js"></script>
  <script type="text/javascript" src="{{ URL::to('/js/axios.js') }}"></script>
  <style media="screen">
    #sortable:hover{
      cursor: grab;
    }
  </style>
  <div class="container">
    <div class="page-content">
      <a href="{{ route('products.create') }}" class="btn btn-success btn-lg pull-right"> <i class="voyager-plus"></i> Add Product </a>
      <div class="row">
        <div class="col-sm-12 col-lg-12 col-md-12">
          <div class="form-group pull-right">
            <br>

            <select class="form-control" name="cat_id" onchange="redirectToCategory(this)">
              <option value="" disabled selected>Show Product By Category</option>
              @foreach (App\ProductCategory::all() as $category)
                <option value="{{ $category->id }}">{{ $category->name }}</option>
              @endforeach
            </select>
          </div>

        </div>
      </div>
      <table class="table table-responsive table-bordered table-striped">
        <thead>
          <th>Arrange</th>
          <th>Name</th>
          <th>Description</th>
          <th>Availability Shift</th>
          <th>Availability Time</th>
          <th>Per Unit Price</th>
          <th>Image</th>
          <th>Category</th>
          <th>Enable / Disable</th>
          <th>Action</th>
        </thead>
        <tbody  id="{{$products->count() > 0 ? "sortable" : ""}}">
          @if($products->count() <= 0)
            <tr >
              <td colspan="10" style="text-align:center">Cannot Find Products Of <b>{{$cat}}</b> </td>
            </tr>
          @endif


          @foreach($products as $product)
            <tr data-id="{{$product->id}}" data-position="{{$product->arrange}}">
                <td># {{$product->arrange}}  </td>
                <td>{{ ucfirst($product->name) }}</td>
                <td>{{ substr($product->description,0,100) }}</td>
                @if($product->timeslot != 0)
                  <td>{{ ucfirst($product->timeslot()->shift) }}</td>
                  <td>
                      @if($product->timeslot()->shift == 'evening')
                    {{ $product->timeslot()->startTime }} <sup>PM</sup> - {{ $product->timeslot()->endTime }} <sup>PM</sup>
                      @else
                        {{ $product->timeslot()->startTime }} <sup>AM</sup> - {{ $product->timeslot()->endTime }} <sup>AM</sup>
                      @endif

              @else
                  <td>{{ ucfirst('general') }}</td>
                  <td>
                      <h5>Morning</h5>
                    <hr>
                    <ul class="list-group">

                    @foreach (\App\GeneralTimeslot::where('isActive','=','1')->get() as $generalTime)
                        <li class="list-item" style="list-style-type:none">
                          @if($generalTime->getTimeSlot()->shift == 'evening')
                              {{ $generalTime->getTimeSlot()->startTime }} <sup>PM</sup> - {{ $generalTime->getTimeSlot()->endTime }} <sup>PM</sup>
                          @endif
                        </li>
                    @endforeach
                  </ul>
                    <h5>Evening</h5>
                    <hr>
                    <ul class="list-group">
                    @foreach (\App\GeneralTimeslot::where('isActive','=','1')->get() as $generalTime)
                        <li class="list-item" style="list-style-type:none">
                        @if($generalTime->getTimeSlot()->shift == 'morning')
                            {{ $generalTime->getTimeSlot()->startTime }} <sup>PM</sup> - {{ $generalTime->getTimeSlot()->endTime }} <sup>PM</sup>
                        @endif
                      </li>
                    @endforeach
                  </ul>


                  </td>
              @endif






                <td>{{ $product->price }} / {{ $product->sellingUnit->name }}</td>
                {{-- <td> <b>$ &nbsp;{{ $product->price }}</b> </td> --}}
                <td> <img src="{{ URL::to('/assets/products/'.$product->image) }}" width="80px" height="80px" style="object-fit:cover" alt=""> </td>
                <td>{{ $product->category->name }}</td>
                <td>
                  @if($product->status == 1)
                    <button type="button" name="button" data-product-id="{{ $product->id }}" data-status="{{ $product->status }}" onclick="manageStatus(this)" class="btn btn-danger" > <i class="voyager-pirate-swords"></i></button>
                  @else
                    <button type="button" name="button" data-product-id="{{ $product->id }}" data-status="{{ $product->status }}" onclick="manageStatus(this)" class="btn btn-success" > <i class="voyager-check"></i></button>
                  @endif
                </td>
                <td>
                  <a href="{{ route('products.edit',['id' => $product->id]) }}" class="btn btn-info btn-lg">Edit</a>
                  <a href="{{ route('products.delete',['id' => $product->id]) }}" onclick="grantPermission(this,event)" class="btn btn-info btn-danger">Delete</a>

                </td>
            </tr>


          @endforeach

        </tbody>


      </table>

      {{-- <div class="pull-right">
          {!! $products->render() !!}
      </div> --}}
    </div>

    <script type="text/javascript">
    function redirectToCategory($el)
    {
      let element = $el.value;
      let route = `{{ URL::to('/') }}/admin/products/${element}`
      window.location = route;
    }
    function manageStatus($el)
    {
        axios.post("{{ URL::to('/')}}/admin/product/state/manage",{
            _token:"{{ csrf_token() }}",
            product_id:$el.getAttribute('data-product-id'),
            status:$el.getAttribute('data-status')
        }).then(response => {
            var res = response.data;
            if(res.message == "200")
            {
              if(res.status == 0)
              {
                $el.className = "";
                $el.className = "btn btn-success";
                $el.innerHTML = `<i class="voyager-check"></i>`
                $el.setAttribute('data-status',0)
                toastr.success('Status Set To Disabled');

              }
              if(res.status == 1)
              {
                $el.className = "";
                $el.className = "btn btn-danger";
                $el.innerHTML = `<i class="voyager-pirate-swords"></i>`
                $el.setAttribute('data-status',1)
                toastr.success('Status Set To Enable');

              }

            }
            else
            {
                toastr.error('Stataus Set To Disabled');
            }
          })
    }

    function grantPermission($el,event)
    {
      event.preventDefault();
      let confirmation = confirm('Warning! Are You Sure You Want To Delete This Category? In The Result Whole Products Of This Category Will Be Deleted!')
      if(confirmation)
      {
        window.location = $el.href;

      }
    }



    $('#sortable').sortable({
      update:function(event,ui){
        $(this).children().each(function( index ){
          if($(this).attr('data-position') != (index+1))
          {
            $(this).attr('data-position',(index+1)).addClass('updated');
          }
        });

        saveNewPositions();

      }
    });

    function saveNewPositions()
    {
      let positions = []
      $('.updated').each(function(index){
          positions.push([$(this).attr('data-id'),$(this).attr('data-position')])
          $(this).removeClass('updated');
      });

      axios.post("{{route('product.arrange.positions')}}",{
        positions,
        _token:"{{csrf_token()}}",
        _method:"post"
      }).then(function(response){
        if(response.data.status == '200')
        {
          toastr.success('Slides Arranged Successfully');
          setTimeout(function () {
              location.reload()
          }, 300);
        }else
        {
          toastr.error('Failed to arrange sliders')
        }

      }).catch(function(error){
        toastr.error('Failed to arrange sliders')
      })
    }
























    </script>























  </div>
@endsection

@extends('voyager::master')


@section('page_header')
  <h1 class="page-title">
      <i class="voyager-bag"></i>
      <p> {{ 'Products' }}</p>
  </h1>
  <span class="page-description">{{ 'Add New Products' }}</span>
  @include('toaster')

@endsection



@section('content')
  <style media="screen">
  table{
      font-size:12px !important;
      font-weight:400 !important;
    }

  </style>

  <div class="container">
      <div class="page-content">
        <div class="row">
          <form  action="{{ route('products.store') }}" enctype="multipart/form-data" method="post">

            {{ csrf_field() }}
            {{ method_field('post') }}

            <div class="form-group col-sm-12 col-md-12 col-lg-12">
              <label for="">Product Name</label>
              <input required type="text" name="name" value="" class="form-control" />
            </div>


            <div class="form-group col-sm-12 col-md-12 col-lg-12">
              <label for="">Product Description</label>
              <textarea name="description" class="form-control" required rows="8" cols="80"></textarea>
            </div>



            <div class="form-group col-sm-12 col-md-12 col-lg-12">
              <label for="cat_id">Select Category</label>
              <select required id="cat_id" class="form-control" name="cat_id">
                <option value="">Select Category</option>
                @foreach (\App\ProductCategory::all() as $category)
                  <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
              </select>
            </div>


            <div class="form-group col-sm-12 col-md-4 col-lg-4">
              <label for="timeslots">Select Delievery Availability</label>
              <select id="timeslots" class="form-control" name="timeslot">
                <option value="">Select Delievery Availability Slots</option>
                <optgroup label="General">
                  <option value="0">General</option>
                </optgroup>
                  <optgroup label="Morning">
                    @foreach (\App\TimeSlot::where('shift','=','morning')->get() as $timeslot)
                      <option value="{{ $timeslot->id }}">{{ $timeslot->startTime }} - {{ $timeslot->endTime }}</option>
                    @endforeach
                  </optgroup>
                  <optgroup label="Evening">
                    @foreach (\App\TimeSlot::where('shift','=','evening')->get() as $timeslot)
                      <option value="{{ $timeslot->id }}">{{ $timeslot->startTime }} - {{ $timeslot->endTime }}</option>
                    @endforeach
                  </optgroup>
              </select>
            </div>
            <div class="form-group col-sm-12 col-md-4 col-lg-4">
              <label for="sellingUnit">Select Selling Unit</label>
              <select id="sellingUnit" onchange="togglePieceAvgWeight(this)" class="form-control" name="sellingUnit_id">
                <option value="">Select Selling Unit</option>
                @foreach (\App\SellingUnit::all() as $su)
                  <option value="{{ $su->id }}">{{ $su->name }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group col-sm-12 col-md-4 col-lg-4" id="piece-average-weight" style="display:none">
              <div class="form-group">
                <label for="selling_unit_amount">Average Weight Of Piece</label>
                <input id="selling_unit_amount" disabled type="text" required placeholder="Average Weight Of Per Piece e.g 2.5 kg" name="avg_weight_piece" class="form-control" required value="">
              </div>
            </div>


            <div class="form-group col-sm-12 col-md-4 col-lg-4">
              <div class="form-group">
                <label for="price">Price</label>
                <input id="price" type="text" placeholder="Total Price" name="price" class="form-control" required value="">
              </div>
            </div>


            <div class="form-group col-sm-12 col-md-6 col-lg-6">
              <label for="product_image">Product Image</label>
              <input required id="product_image" onchange="getImage(this)" type="file" accept="image/*" name="image" class="form-control" />
            </div>

            <div class="form-group col-sm-12 col-md-6 col-lg-6">
              <img id="thumbnail" src="https://getuikit.com/v2/docs/images/placeholder_200x100.svg" width="100px" height="100px" style="object-fit:cover" alt="" />
            </div>

              <button type="submit" class="btn btn-success btn-lg pull-right"> <i class="voyager-plus"></i> Save Product </button>


          </form>
















        </div>
      </div>
  </div>

  <script type="text/javascript">
  function getImage($el)
  {
    var fileReader = new FileReader();
    fileReader.readAsDataURL($el.files[0])
    fileReader.onload = function()
    {
      document.getElementById('thumbnail').src = fileReader.result;
    }
  }


  function togglePieceAvgWeight($el){
      let avgWeight = document.querySelector('#piece-average-weight');
      let avgWeightInput = document.querySelector('#selling_unit_amount');
      let selectedIndex = $el.options.selectedIndex;
      let selectedIndexValue = $el.options[selectedIndex].innerHTML;
      if(selectedIndexValue.toLowerCase() == "pieces" || selectedIndexValue.toLowerCase() == "piece"){
          avgWeight.style.display = "block";
          avgWeightInput.disabled = false;
      }else {
        avgWeight.style.display = "none";
        avgWeightInput.disabled = true;

      }

  }











  </script>

@endsection

@extends('voyager::master')


@section('page_header')
  <h1 class="page-title">
      <i class="voyager-people"></i>
      <p> {{ 'Referal Information of User ' }} <b>{{ $user->name }}</b> </p>
  </h1>
  <span class="page-description">{{ 'Browse  Referals of ' }} <b> {{ $user->name }} </b> </span>
@endsection



@section('content')
  <style media="screen">
  table{
      font-size:12px !important;
      font-weight:400 !important;
    }

  </style>
  <script type="text/javascript" src="{{ URL::to('/js/axios.js') }}"></script>
  <div class="container">
    <div class="page-content">
      <table class="table table-responsive table-striped table-bordered">
        <thead>
            <th>Referal Name</th>
            <th>Referal Email</th>
            <th>Referal Registered On</th>
            <th>Referal Last Order ( ago )</th>
            <th>Referal Last Order ( date )</th>
        </thead>
        <tbody>
          @foreach ($user->getRefarals() as $referals)
            <tr>
              <td>
                {{ $referals->getReferalInfo()->name }}
              </td>
              <td>
                {{ $referals->getReferalInfo()->email }}
              </td>
              <td>
                {{ $referals->getReferalInfo()->created_at->format('d, l, F ') }}
              </td>
              <td>
                {{ $referals->getLastOrder() ? "No Orders From This Users" : \Carbon\Carbon::parse($referals->getLastOrder())->diffForHumans() }}
              </td>
              <td>
                {{ $referals->getLastOrder() ? "No Orders From This Users" : \Carbon\Carbon::parse($referals->getLastOrder())->format('d, l, F') }}

              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
  </div>
  </div>
@endsection

@extends('layouts.app')

@section('content')
  <script type="text/javascript" src="{{ URL::to('/js/axios.js') }}"></script>

  <?php $backgroundColor = \App\FrontendCMS::get()->first()->backgroundColor; ?>

<style>
@import url(https://fonts.googleapis.com/css?family=Roboto:400,900,700,500);

body {
font-weight: 600;
overflow-x: hidden;
color:{{$backgroundColor}} !important;
}
::placeholder{
  color:{{$backgroundColor}} !important;
}

.container-custom
{
  background-color: #fff !important;
  /* padding: 60px !important; */
  /* padding-left:0px !important; */
  margin: 0 auto !important;
  width: 600px !important;
  height: auto;
  margin-top: 50px !important;
}
.body-text {
padding: 0 20px 30px 20px !important;
font-family: "Roboto" !important;
font-size: 1em !important;
color: #333 !important;
text-align: center !important;
line-height: 1.2em !important;
}
.form-container {
flex-direction: column !important;
justify-content: center !important;
align-items: center !important;
}
.card-wrapper {
background-color: #fff !important;
width: 100% !important;
display: flex !important;

}
.personal-information {
background-color: #fff !important;
color: #fff !important;
padding: 1px 0 !important;
text-align: center !important;
}
h1 {
font-size: 1.3em !important;
letter-spacing: normal;
font-weight: 600;
color:{{$backgroundColor}} !important;
}
input {
margin: 1px 0 !important;
padding-left: 3% !important;
font-size: 14px !important;
}
input[type="text"]{
display: block !important;
height: 50px !important;
width: 97% !important;
border: none !important;
}
input[type="email"]{
display: block !important;
height: 50px !important;
width: 97% !important;
border: none !important;
}
input[type="submit"]{
display: block !important;
height: 60px !important;
width: 100% !important;
border: none !important;
background-color: #3C8DC5 !important;
color: #fff !important;
margin-top: 2px !important;
curson: pointer !important;
font-size: 0.9em !important;
text-transform: uppercase !important;
font-weight: bold !important;
cursor: pointer !important;
}
input[type="submit"]:hover{
background-color: #6FB7E9 !important;
transition: 0.3s ease !important;
}
#column-left {
width: 46.8% !important;
float: left !important;
margin-bottom: 2px !important;
}
#column-right {
width: 46.8% !important;
float: right !important;
}

@media only screen and (max-width: 480px){
body {
  width: 100% !important;
  margin: 0 auto !important;
}
.form-container {
  margin: 0 auto !important;
}
input {
  font-size: 1em !important;
}
#input-button {
  width: 100% !important;
}
#input-field {
  width: 96.5% !important;
}
h1 {
  font-size: 1.2em !important;
}
input {
  margin: 2px 0 !important;
}
input[type="submit"]{
  height: 50px !important;
}
label{
  color:{{$backgroundColor}} !important;
}
#column-left {
  width: 96.5% !important;
  display: block !important;
  float: none !important;
}
#column-right {
  width: 96.5% !important;
  display: block !important;
  float: none !important;
}
  @media only screen and (max-width:768px){
      .container-custom{
        margin: 0px !important;
        width: auto !important;
      }
  }

}

</style>
<style media="screen">
  .disabled-row {
    background-color:#eee;
  }
  .enabled-row{
    background-color:#fff;
  }
  #toDisable{
    display:none;
  }
</style>

  {{--
      // Address Styles
   --}}
{{-- <style media="screen">
  .address-flex
  {
    display: flex;
    justify-content: center;
    align-items: center;
  }
</style> --}}






  {{-- <div class="body-text">Write your name in the right fields. Also write your imaginary card number. By clicking CCV field card will turn.</div> --}}
    <div class="container-custom z-depth-3">
      <form action="{{route('payment.pay')}}" onsubmit="submitHandler()" method="post">
        {{ csrf_field() }}
        <div class="form-container">
          <div class="personal-information">
            {{-- <img src="{{ Voyager::image(Voyager::setting('site.logo'))}}" style="width:auto;height:70px;" alt=""> --}}
            <h1 style="color:{{$backgroundColor}} !important">Payment Information</h1>
            <br/>

            <i style="font-size:14px;font-weight:bold !important;color:{{$backgroundColor}}"> <sup>*</sup> Extra charges will be deducted as per required to make delivery </i>

            <center>
              <br>

              <div class="divider" style="background:{{$backgroundColor}}">

            </center>
            </div>
          </div>
          <!-- end of personal-information -->

                    <input required style="color:black;border-bottom:1px solid {{$backgroundColor}} !important" id="input-field" type="text" name="first-name" value="{{Auth::user()->name}}" placeholder="Name"/>
                    <input required style="color:black;border-bottom:1px solid {{$backgroundColor}} !important" id="input-field" type="text" name="number" placeholder="Card Number"/>
                    <input required style="color:black;border-bottom:1px solid {{$backgroundColor}} !important" id="column-left" type="text" name="expiry" placeholder="MM / YY"/>
                    <input required style="color:black;border-bottom:1px solid {{$backgroundColor}} !important" id="column-right" type="text" name="cvc" placeholder="CCV"/>

              <div class="card-wrapper"></div>
              <center>
                <div class="divider" style="color:black;background:{{$backgroundColor}}">

              </center>
              <div style="padding:10px">
                Verify Address
                <button id="toEnable" type="button" class="btn white-text right" style="background-color:{{$backgroundColor}};font-size:12px"  onclick="showAddressHandler()"> <i class="fa fa-pencil" style="font-size:12px"  ></i>&nbsp; Edit Delivery Address </button>
                <button id="toDisable" type="button" class="btn white-text blue right" style="font-size:12px"  onclick="hideAddressHandler()"> <i class="fa fa-check" style="font-size:12px"  ></i>&nbsp; Done Editing </button> <br><br>

                <i style="font-size:12px;font-weight:bold !important"> <sup>*</sup> This address will be used for shipping, Please verify or click to edit to make changes</i>
              </div >
                  <div class="row disabled-row" id="rowColorToggle"  style="padding:4px">
                    {{-- CITY --}}

                    <div class="col s12 m4 l4">
                      <span for="" style="font-size:14px;color:black">City</span>
                      <div class="input-field" style="border-bottom:1px solid {{$backgroundColor}};">
                        <select id="cityList"    onchange="updateSuburbList(this)" >
                          <option value="0" style="color:{{$backgroundColor}}" disabled >Choose City</option>
                          <?php
                          $city_id = 0;
                          if(Auth::user()->suburb_id != 0)
                          {
                            $city_id = \App\Area::where('id','=',\App\Suburb::where('id','=',Auth::user()->suburb_id)->first()->areas_id)->first()->id;
                          }
                            ?>
                          @foreach (\App\Area::orderBy('cityName','asc')->get() as $city)
                            @if($city_id == $city->id)
                              <option value="{{$city->id}}" selected>{{$city->cityName}}</option>
                            @else
                              <option value="{{$city->id}}">{{$city->cityName}}</option>
                            @endif
                          @endforeach
                        </select>


                        @if ($errors->has('suburb_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>Choose Your City</strong>
                            </span>
                        @endif

                      </div>
                    </div>



                      {{-- SUBURB LIST --}}


                    <div class="col s12 m4 l4">
                      <span for="" style="font-size:14px;color:black">Suburb</span>
                      <div class="input-field" style="border-bottom:1px solid {{$backgroundColor}};">
                        <select id="suburbList" name="suburb_id" >
                            <option value="0"  disabled >Choose Suburbs</option>
                            @foreach (\App\Suburb::orderBy('suburbName','asc')->get() as $suburb)
                              @if(Auth::user()->suburb_id == $suburb->id)
                                <option value="{{$suburb->id}}" selected>{{$suburb->suburbName}}</option>
                              @else
                                <option value="{{$suburb->id}}">{{$suburb->suburbName}}</option>
                              @endif
                            @endforeach
                        </select>
                        @if ($errors->has('suburb_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('suburb_id') }}</strong>
                            </span>
                        @endif

                      </div>
                    </div>

                      <div class="col s12 m4 l4">
                        <span for="" style="font-size:14px;color:black">Street, House Address</span>

                        <div class="input-field" style="border-bottom:1px solid {{$backgroundColor}};">
                            <input id="address" type="text"  class="{{ $errors->has('address') ? ' is-invalid' : '' }}" name="streetaddress" value="{{Auth::user()->address}}" >

                            @if ($errors->has('address'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('address') }}</strong>
                                </span>
                            @endif

                        </div>
                      </div>
                  </div>




                    {{-- <input required style="color:black;border-bottom:1px solid {{$backgroundColor}} !important" id="input-field" type="text" name="streetaddress" required="required" autocomplete="on" maxlength="45"  value="{{ Auth::user()->address }}" placeholder="Address"/> --}}
                    {{-- <input required style="color:black;border-bottom:1px solid {{$backgroundColor}} !important" id="column-left" type="text" name="city" required="required" autocomplete="on" maxlength="20" placeholder="City"/> --}}
                    {{-- <input  required style="color:black;border-bottom:1px solid {{$backgroundColor}} !important" id="column-left" type="text" name="zipcode" required="required" autocomplete="on" pattern="[0-9]*" maxlength="5" placeholder="ZIP code"/> --}}
                    {{-- <input required style="color:black;border-bottom:1px solid {{$backgroundColor}} !important" id="column-right" type="email" name="email" value="{{Auth::user()->email}}" required="required" autocomplete="on" maxlength="40" placeholder="Email"/> --}}
                    <input required style="color:black;border-bottom:1px solid {{$backgroundColor}} !important;background:{{$backgroundColor}} !important;font-size:32px;font-family:'Raleway'" id="input-button" type="submit" value="Pay ${{session()->get('total')}} /NZ"/>
                  </div>
        </form>
    </div>

  <script type="text/javascript" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/121761/card.js"></script>
  <script type="text/javascript" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/121761/jquery.card.js"></script>
<script type="text/javascript">
$('form').card({
  container: '.card-wrapper',
  width: 280,

  formSelectors: {
      nameInput: 'input[name="first-name"], input[name="last-name"]'
  }
});
</script>



  <script type="text/javascript">

  // Disabled Addresses To Edit


  function hideAddress($el){
    $el.disabled = true;
    return function($el2){
      $el2.disabled = true;
      return function($el3){
        $el3.disabled = true;
        document.querySelector('#toEnable').style.display = "block";
        document.querySelector('#toDisable').style.display = "none";
        document.querySelector('#rowColorToggle').className = "row disabled-row";
      }
    }
  }


function hideAddressHandler(){
  hideAddress(document.querySelector('#cityList'))(document.querySelector('#address'))(document.querySelector('#suburbList'));
}

// Enable Addresses To Edit

function showAddress($el){
  $el.disabled = false;
  return function($el2){
    $el2.disabled = false;
    return function($el3){
      $el3.disabled = false;
      document.querySelector('#toEnable').style.display = "none";
      document.querySelector('#toDisable').style.display = "block";
      document.querySelector('#rowColorToggle').className = "row enabled-row";
    }
  }
}


function showAddressHandler(){
showAddress(document.querySelector('#cityList'))(document.querySelector('#address'))(document.querySelector('#suburbList'));
}











    function updateSuburbList($el){
      let area_id = $el.options[$el.selectedIndex].value;
      let suburbList =   document.querySelector('#suburbList');
          suburbList.innerHTML = "<option disabled selected>Loading Suburbs...</option>";
          $('select').material_select();
            axios.get(`{{route('get.suburb.by.area')}}`,{
              params:{
                area_id
              }
            }).then( ({data}) => {
                suburbList.innerHTML = "";
                data.forEach( suburb => {
                    let option = document.createElement('option');
                    option.value = suburb.id;
                    option.innerHTML = suburb.suburbName;
                    document.querySelector('#suburbList').appendChild(option);
                });
                $('select').material_select();
            });
    }
  </script>


  <script type="text/javascript">
    function submitHandler(){
      showAddressHandler();
    }
  </script>



@endsection

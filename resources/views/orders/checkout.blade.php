@extends('layouts.app')
@section('content')
  <?php $backgroundColor = \App\FrontendCMS::get()->first()->backgroundColor; ?>
  <script type="text/javascript" src="{{ URL::to('/js/axios.js') }}"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.23.0/moment.js"></script>

<style media="screen">
    .page-heading
    {
      padding-top:5%
    }
[type="radio"]:checked+label:after, [type="radio"].with-gap:checked+label:before, [type="radio"].with-gap:checked+label:after{
  border:2px solid {{$backgroundColor}};
}
[type="radio"]:checked+label:after, [type="radio"].with-gap:checked+label:after{
  background: {{$backgroundColor}};
}
    @media only screen and (max-width: 768px)
    {
      .page-heading
      {
        padding-top:12%;
      }
    }
    body
    {
      overflow-x: none;
    }
    .grid{
      display: grid;
      grid-template-areas: "proceed delivery extra";

    }

    .grid > div {
      border:1px dashed {{$backgroundColor}};
      padding:12px;
    }
    .proceed{
      grid-area: proceed;
    }
    .delivery{
      grid-area: delivery;
    }
    .extra{
      grid-area: extra;
    }
    @media screen and (max-width:768px) {
      .grid{
        grid-template-areas:
        "proceed"
        "delivery"
        "extra";

      }
    }
</style>
  <div class="row page-heading" >
    <center>
      <div class="">
        <br><br>
        <p class="flow-text pink-text" style="color:{{$backgroundColor}} !important">
          Checkout Page
          <div class="divider" style="width:25%;height:1px;border-radius:15px;background-color:{{$backgroundColor}}"></div>
        </p>
      </div>
    </center>
  </div>
  <input type="hidden" id="deliveryCharges" name="" value="{{\App\Miscellaneous::first()->deliveryCharges}}">
  <div class="row card">
    <div class="card-content" >
      <form action="{{ route('payment.from') }}"  method="post">
        {{ csrf_field() }}

        <div class="grid">





            <div class="proceed">
              <button type="button"  style="display:block;padding:8px;width:auto;margin:auto;;border:1px solid {{$backgroundColor}};background: {{$backgroundColor}};color:white;"> <b >Estimated Amount: $ <span style="font-family:Helvetica">{{$total}}</span>  </b> <span style="font-weight:bold;font-family:Helvetica" id="deliveryChargesHolder"> + {{\App\Miscellaneous::first()->deliveryCharges}}</span>  <b>/ NZ </b>
                <br>
              </button><br>
              <center>
                <a href="#" class="btn"   onclick="document.getElementById('form-delivery').click()" style="color:white;background-color:{{$backgroundColor}};border-radius:25px;font-size:12px"> <b>Proceed to checkout &nbsp; <i class="fa fa-shopping-cart" style="font-size:14px"></i> </b> </a>
              </center>
            </div>





            <div class="delivery">
              <input type="hidden" name="total" id="Total" value="{{(float) $total + (float) \App\Miscellaneous::first()->deliveryCharges}}">
              <p class="flow-text" style="font-size:16px;color:{{$backgroundColor}};font-weight:600">
                Choose Order Shipment Method
              </p><br>
              <p style="margin-top:10px">
                <input id="delivery" class="with-gap" name="delivery" type="radio" onclick="addDeliveryCharges()" checked value="1" required />
                <label style="color:{{$backgroundColor}};font-weight:bold" for="delivery">Delivery ($ {{\App\Miscellaneous::first()->deliveryCharges}} NZ will be added to the total amount  )</label>
              </p>
              <p style="margin-top:10px">
                <input id="pickup" class="with-gap" name="delivery" type="radio" onclick="removeDeliveryCharges()" value="0" required />
                <label style="color:{{$backgroundColor}};font-weight:bold" for="pickup">Self Pickup ( No delivery charges will be deducted )</label>
              </p>
            </div>






            <div class="extra">
              <div class="input-field">
                <label for="orderDate" style="font-weight:bold;font-size:15px;color:{{$backgroundColor}}">Choose Order Date,</label>
                <input type="text" onchange="changeTimeslot(this)" name="orderDate" id="orderDate" class="datepicker"  />
              </div>

              <label  style="font-size:15px;font-weight:bold;display: block;margin-top:10px;margin-bottom:10px;;color:{{$backgroundColor}}" >Choose Timeslot</label>
                <i id="timeslot-not-found" style="font-weight:bold;font-size:12px;">No Slots Are Available For Today, Select Next Date&nbsp;!</i>

                @foreach (\App\GeneralTimeslot::all() as $general)
                  <?php
                  if($general->getTimeSlot()->shift == "morning")
                  {
                    $time = "AM";
                  }
                  else
                  {
                    $time = "PM";
                  }
                   ?>
                     <p id="{{strtolower($general->getTimeSlot()->shift)}}">
                       <input class="with-gap"  name="timeslot" value="{{$general->timeslot_id}}" type="radio" id="{{$general->id}}" />
                       <label for="{{strtolower($general->id)}}">{{ $general->getTimeSlot()->startTime }} - {{ $general->getTimeSlot()->endTime }} <sup>{{ $time }}</sup></label>

                     </p>
                 @endforeach
            </div>



        </div>

        <button type="submit" id="form-delivery" style="display:none"></button>
      </form>

      <br>
      <div class="col s12 m12 l12">

        <table class="table responsive-table bordered hover">
          <thead>
            <th>#</th>
            <th>Name</th>
            <th>Per Unit Price</th>
            <th>Quantity</th>
            <th>Sub Total</th>
            <th>Service Type</th>
            <th>Action</th>
          </thead>
          <tbody>
            @foreach ($cart as $item)
              <?php
                $product = \App\Product::where('id','=',$item[0])->first();
                static $i = 0;
              ?>

              <tr>
                <td>{{ $i }}</td>
                <td>{{ $product->name }}</td>
                <td> <b>$  &nbsp; <span style="font-family:Helvetica">{{ $product->price }}</span> NZ / {{ ucfirst( $product->sellingUnit->name ) }} </b>
                </td>
                <td>
                   <b>
                    &nbsp; {{ $item['quantity'] }} &nbsp; {{ ucfirst( $product->sellingUnit->name ) }}
                   </b>
                </td>
                <td>
                   <b>NZ $ &nbsp; <span style="font-family:Helvetica">{{ $item['priceMade'] }}</span>
</b>
                </td>
                @if(count($item['serviceOption']) == 0)
                  <td style="font-weight:bold">No Service Needed!</td>
                @else
                  <td>
                    <ol>
                    @foreach ($item['serviceOption'] as $id)
                        <li style="font-weight:bold">{{ \App\ServiceType::where('id','=',$id)->first()->name }}</li>
                    @endforeach
                  </ol>
                  </td>
                @endif
                <td> <button type="button" data-product-id="{{ $product->id }}" class="btn btn-floating red waves-effect" onclick="deleteOrder(this)"> <i class="fa fa-trash"></i> </button> </td>
              </tr>

              <?php $i++; ?>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>














<script type="text/javascript">

  $(document).ready(function(){



    $('.datepicker').pickadate({
      onSet: function(){
          this.close();
        },
        selectMonths: true, // Creates a dropdown to control month
          selectYears: 15, // Creates a dropdown of 15 years to control year
          format: 'mm-dd-yyyy',
          min:new Date(moment()),
          max:new Date(moment().add(2,'days'))
      });
      var datepickerx = document.getElementsByClassName('datepicker');
      for (var i = 0; i < datepickerx.length; i++) {
         datepickerx[i].value = moment().format('D MMMM,YYYY')
      }

        // calculate timeslots
        calculateTimeslots();

















  });
function calculateTimeslots(){

  // timeslot selectors

  let morningOption = document.querySelector('#morning');
  let eveningOption = document.querySelector('#evening');
  let timeslotNotificationError = document.querySelector('#timeslot-not-found');

  // timeslot calculation error

  let morningHour = parseInt(9);
  let eveningHour = parseInt(15);
  let currentHour = new Date().getHours();

  morningOption.style.display = "none";
  eveningOption.style.display = "none";
  timeslotNotificationError.style.display = "none";
  //timeslot logic

  if(currentHour < morningHour && currentHour > eveningHour){
    if(Math.abs(morningHour - currentHour) <= 3  ){
      morningOption.style.display = "block";
      eveningOption.style.display = "block";
    }
    else if(Math.abs(morningHour - currentHour) > 3  ) {
      morningOption.style.display = "none";
      eveningOption.style.display = "block";
    }
  }
if(currentHour > morningHour && currentHour < eveningHour){

    if(Math.abs(eveningHour - currentHour) <= 3 ){
        morningOption.style.display = "none";
        eveningOption.style.display = "block";
      }else if(Math.abs(eveningHour - currentHour) > 3  ){
          morningOption.style.display = "none";
          eveningOption.style.display = "none";
          timeslotNotificationError.style.display = "block"

      }else {
        morningOption.style.display = "none";
        eveningOption.style.display = "none";
        timeslotNotificationError.style.display = "block"
      }

  }


  if(morningOption.style.display == "none" && eveningOption.style.display == "none"){
    timeslotNotificationError.style.display = "block";
  }else {
    timeslotNotificationError.style.display = "none"
  }
}
function changeTimeslot($el)
{
  let currentDate = moment().format('M-D-YYYY').split('-');
  let selectedDate = $el.value.split('-');
  let currentDateSum = (parseInt(currentDate[0]) + parseInt(currentDate[1]) + parseInt(currentDate[2]) ) ;
  let selectedDateSum  = (parseInt(selectedDate[0]) + parseInt(selectedDate[1]) + parseInt(selectedDate[2]) )
  let timeslotNotificationError = document.querySelector('#timeslot-not-found');

  if(currentDateSum == selectedDateSum){
      calculateTimeslots();
  }else {
    timeslotNotificationError.style.display = "none";
    let morningOption = document.querySelector('#morning').style.display = "block";
    let eveningOption = document.querySelector('#evening').style.display = "block";
  }










}
function deleteOrder($el)
{
  let product_id = $el.getAttribute('data-product-id');
  axios.post('{{ URL::to('/deleteSingleOrder') }}',{
      product_id:product_id,
      _token:"{{ csrf_token() }}"
  }).then(response => {

      toastr.success('Deleted From Cart');
      setTimeout(function () {
        location.reload();

      }, 200);
  })
    .catch(err => {
      toastr.error('Failed to delete this product from cart');
    })
}
</script>

  <script type="text/javascript">
  let totalChargesWithDelivery = document.querySelector('#Total').value
    function addDeliveryCharges(){
      let dCharges = document.querySelector('#deliveryCharges').value
      document.querySelector('#deliveryChargesHolder').innerHTML =  ` + ${dCharges}`
      document.querySelector('#Total').value = totalChargesWithDelivery
    }
    function removeDeliveryCharges()
    {
      document.querySelector('#deliveryChargesHolder').innerHTML =  ``
      document.querySelector('#Total').value = {{$total}}
    }













  </script>








@endsection

<?php $admin_logo_img = Voyager::setting('site.logo'); ?>

<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        @include('toaster')
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
	      <meta name="google-site-verification" content="L1-CIHVtDLBsreI9YiimEGBwAVWziuQ5MY5bBzC1TLM" />
        <title>Premium Meat</title>
        <link rel="shortcut icon" href="{{URL::to("/extra/logo.png")}}" />
{{--
        <link rel="shortcut icon" href="{{Voyager::image($admin_logo_img)}}" /> --}}

        <!-- Fonts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.green.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.7/jquery.fullpage.css">
        <link href="https://fonts.googleapis.com/css?family=Great+Vibes|Raleway" rel="stylesheet">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.9.7/jquery.fullpage.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://yarnpkg.com/en/package/normalize.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
        <!-- Styles -->

        <style>
        /* body,html
        {
          margin:0px !important;
          padding:0px !important;
        } */
        #dadas:hover
        {
          background-color: #c21e56 !important;
        }
        #dadas2:hover
        {
          background-color: #c21e56 !important;
        }

        nav ul li:hover {
   background-color: white !important;
}
           .no_transition
           {
               transition: none !important;
           }
            #login:hover
            {
              color:#c21e56 !important;
            }
            .hover-custom
            {
              font-weight: 600;
            }
              .hover-custom:hover
              {
                text-shadow:0px 0px 10px #c21e56;
              }
            .product-listings
            {
                height: 205px;
                width: 100%;
                padding:0% 10% 0% 10%;
                display:flex;
                flex-flow:column;
                flex-direction: column;
            }
            .product-listings > *
            {
                flex:1 1 80px;
            }
            .title-category
            {
                font-size:18px;
                font-weight:550;
            }
            .svg-icon
            {
                width:80px;height:80px;object-fit:cover;
            }
            .svg-icon:hover
            {
                cursor: pointer;
            }
            html, body
            {
                height: 100%;
                margin:0;
                font-family: 'Raleway';
                min-height:100%;
                background: #f7f7f7 url('https://futurenature.net/img/noisy-texture-80x80-o3-d18-c-f7f7f7-t1.png');
            }
            .footer-black{
                 background-color:#dc3545;
            }
            .premium-logo
            {
                background-image:url('{{Voyager::image($admin_logo_img)}}');
                width:250px;
                height: 250px;
                background-size:contain;
                background-repeat: no-repeat;
                background-position: center center;
                box-shadow: 1px 1px 2px 2px rgba(0,0,0,.35);
                border-radius: 100%;
            }
            .navbar-logo
            {
                background-image:url('{{Voyager::image($admin_logo_img)}}');
                width:100px;
                line-height:70px;
                background-color:white;
                height: 100px;
                border-radius: 100%;
                background-position: center center;
                background-repeat: no-repeat;
                background-size: contain;

            }
            nav{
                background-color: transparent !important;
                box-shadow: none;
            }

            nav ul li a:hover {
                background-color: transparent !important;
            }
            nav ul li a {
                color:#c21e56 !important;

            }
            .nav-wrapper{
                padding:0px 24px 42px 24px !important;
                background-color: white !important;
            }
            ul li
            {
                list-style-type: none;
            }
            .sausage-back
            {
                background-image:url('{{URL::to("/extra/sausage.jpg")}}');
                background-size:cover;
                background-repeat: no-repeat;
                height: 80%;
                /* background-attachment: fixed; */
                background-position: center right right right ;
                width: auto;
            }
            .steak-back
            {
                background-image:url('{{URL::to("/extra/steak.jpg")}}');
                background-size:cover;
                background-repeat: no-repeat;
                height: 80%;
                /* background-attachment: fixed; */
                background-position: center right right ;
                width: auto;
            }


            #fp-nav ul li a span
            {
                background-color:#c21e56 !important;
            }
            #slide1
            {
             background-image:url('{{URL::to("/sliders/slide1.jpg")}}')   ;
             background-size:contain;
             /* width:100%; */
             background-repeat:no-repeat;

            }
            /* #
            {
                background-image:url('{{Voyager::image($admin_logo_img)}}');
                width:100px;
                line-height:70px;
                background-color:white;
                height: 100px;
                border-radius: 100%;
                background-position: center center;
                background-repeat: no-repeat;
                background-size: contain;
            } */
            #slide2
            {
             background-image:url('{{URL::to("/sliders/slide2.jpg")}}')   ;
             background-size:contain;
             background-repeat:no-repeat;

            }
            #slide3
            {
             background-image:url('{{URL::to("/sliders/slide3.jpg")}}')   ;
             background-size:contain;
             background-repeat:no-repeat;

            }
            @keyframes scroll {
            0% {
                transform: translateY(0);
            }
            30% {
                transform: translateY(60px);
            }
        }

            .owl-item:hover
            {
                cursor: grab;
            }

            .card-image  img
            {
                height:300px !important;
            }
            svg #wheel {
                animation: scroll ease 2s infinite;
            }

            // Default stuff.
            *,
            *::before,
            *::after {
                box-sizing: border-box;
                -webkit-backface-visibility: hidden;
                -webkit-transform-style: preserve-3d;
            }

            #slide4
            {
             background-image:url('{{URL::to("/sliders/slide4.jpg")}}')   ;
             background-size:contain;
             background-repeat:no-repeat;

            }
            #sliders
            {
                width:100%;

            }
            /* .overlay
            {
             width:100%;
             height:20px;
             top:0px;
             background-color: rgba(0,0,100,0.2);
             position: relative;;
            } */
            #sausage-defination
            {
                color:white;font-size:18px;font-weight:50;font-family:Raleway;word-wrap:break-word;
            }
            #fullpage
            {
                margin-top:-2%;
            }
            .footer-last
            {
                background: {{\App\FrontendCMS::get()->first()->backgroundColor}};
                /* fallback for old browsers */
                /* background: -webkit-linear-gradient(to right, #c31432, #240b36);  /* Chrome 10-25, Safari 5.1-6 */
                /* background: linear-gradient(to right, #c31432, #240b36); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */ */ */

            }
            .card-margin
            {
                margin:5px;
            }
            .fp-show-active
            {
                top:80% !important;

            }
            .scroll-down
            {
              display: block;
            }

            #fp-nav ul li a span, .fp-slidesNav ul li a span
            #fp-nav ul li a span
            {
                background-color:#c21e56 !important;
                border:2px solid yellow;
                box-shadow: 0 5px 5px 0 rgba(0,0,0,0.14), 0 9px 12px 0 rgba(0,0,0,0.12), 0 6px 4px -6px rgba(0,0,0,0.2);
            }
                            #product-intro
                            {
                              margin-top: 0px;
                            }
            @media only screen and (max-width: 768px)
            {
                #fp-nav ul li a span, .fp-slidesNav ul li a span
                #fp-nav ul li a span
                {
                    background-color:#c21e56 !important;
                    border:2px solid yellow;

                    box-shadow: 0 5px 5px 0 rgba(0,0,0,0.14), 0 9px 12px 0 rgba(0,0,0,0.12), 0 6px 4px -6px rgba(0,0,0,0.2);
               }
                .scroll-down
                {
                  display: none;
                }
                #meat-board
                {
                    display: none;
                }
                .footer-black
                {
                    height: 80%;
                }
                .cat-display
                {
                    margin-left:-60px;
                }
                .cat-display > div
                {
                    margin:0px 10px 0px 10px;

                }
                .cat-display > div > img
                {
                    width: 50px;
                    height: 50px;
                }
                #cat-display > div > p
                {
                    font-size:12px;
                }

                .overlay
                {
                    display: none;
                }
                #product-intro{
                    margin-bottom: -100px;
                    margin-top:-60px;
                }
                #sausage-defination
                {
                    width: 300px !important;
                    text-align: justify;
                }
                #steak-defination
                {
                    width: 300px !important;
                    text-align: justify;
                }
                .navbar-logo
                {
                    width: 120px;
                    height: 120px;
                }
                .opx
                {
                    padding:0px;
                    margin-top:-100px;
                }
                #navbar-subscription-logo
                {
                    width:50px;
                    height:50px;
                }
                #premium-plan-title
                {
                    font-size:14px;
                }
                #meat-board-text-font
                {
                    font-size:16px;
                }

            }
            #meat-board-text-font
                {
                    font-size:16px;
                }

            .product-listing-gallery
            {
                height:180px;
                width:100%;
                margin-top:60px;
            }
            body
            {
              overflow-x: hidden;
            }
            .menu
            {
              text-shadow: 1px 1px rgba(0,0,0,.3);
              width: auto;
              padding: 12px 0px;
              position: absolute;
              top:52%;
              right:2%;
              text-align: right;
              z-index:1000;
            }
            .menu-item:hover
            {
              cursor: pointer;
            }
            .menu-item
            {
              transition-duration: .2s;
              list-style-type: none;
              margin:10px;
              font-size:18px;
              text-align: justify;
            }
            .active
            {
               transition-timing-function:linear;
              transition-duration: .2s;
              font-size:20px;
            }


#fp-nav ul li a span, .fp-slidesNav ul li a span
{
   background-color:red !important;
   border:2px solid white !important;
   box-shadow: 0 5px 5px 0 rgba(0,0,0,0.14), 0 9px 12px 0 rgba(0,0,0,0.12), 0 6px 4px -6px rgba(0,0,0,0.2) !important;
   padding-left:4px;
   padding-top:4px;
}

#fp-nav ul li a.active span, .fp-slidesNav ul li a.active span, #fp-nav ul li:hover a.active span, .fp-slidesNav ul li:hover a.active span
{
  content:"";
  background-image: url("{{URL::to('/')}}/extra/dot-inner-2.png") !important;
  width:12px;
  background-size: cover;
  display: block;
  height: 12px;
}



        </style>
    </head>
    <body>
      {{-- <div class="fixed-action-btn button-collapse  hide-on-large-only" id="right-dots" data-activates="slide-left">
        <a class="btn-floating btn-large red">
          <i class="large material-icons">mode_edit</i>
        </a>
      </div>

 --}}

      {{-- <ul  class="menu show-on-large hide-on-med-and-down" style="background-color:rgba(255,255,255,.4);display:none">
          <li class="menu-item">
            <a class="pink-text menu-item-child" onclick="makeActive(this)" data-href="#carousel" data-name="1"> <i class="fa fa-clone fa-fw"></i> Home</a>
          </li>
          <li class="menu-item">
            <a class="pink-text menu-item-child" onclick="makeActive(this)" data-href="#products" data-name="2"> <i class="fa fa-fw fa-cubes"></i> Products</a>
          </li>
          <li class="menu-item">
            <a class="pink-text menu-item-child" onclick="makeActive(this)" data-href="#additional-products" data-name="3"><i class="fa fa-fw fa-line-chart"></i> Special</a>
          </li>
          <li class="menu-item">
            <a class="pink-text menu-item-child" onclick="makeActive(this)" data-href="#sausages" data-name="4"> <i class="fa fa-fw fa-cutlery"></i> Sausage</a>
          </li>
          <li class="menu-item">
            <a class="pink-text menu-item-child" onclick="makeActive(this)" data-href="#steak" data-name="5"> <i class="fa fa-fw fa-sign-language "></i> Steak</a>
          </li>
          <li class="menu-item">
            <a class="pink-text menu-item-child" onclick="makeActive(this)" data-href="#footer" data-name="6"> <i class="fa fa-fw fa-graduation-cap"></i> About Us </a>
          </li>
          <li class="menu-item">
            <a class="pink-text menu-item-child" onclick="makeActive(this)" data-href="#footer-last" data-name="7"> <i class="fa fa-fw fa-microphone"></i> Contact Us</a>
          </li>




      </ul> --}}
      @include('navbar')


        <div id="fullpage" >
            @include('carousel')
            @include('categories')
            {{-- @include('additional-products') --}}
            @include('sausage')
            @include('steak')
            @include('footer')
            @include('footer-black')
















        {{-- <div id="modal1" class="modal">
                <div class="modal-content">

                        <div class="col s12 m8 offset-m2 l6 offset-l3">
                                <div class=" ">
                                  <div class="row valign-wrapper">
                                    <div class="col s2">
                                      <div id="navbar-subscription-logo" class="navbar-logo"></div>
                                    </div>
                                    <div class="col s10" style="text-align:center">
                                            <span class="black-text"  >
                                                <p class="flow-text" id="premium-plan-title" >
                                                        The Premium's Plan

                                                    </p>
                                            </span>
                                            <a class="waves-effect waves-teal pink accent-3 btn-float white-text btn-lg right" style="width:100%;text-align:center;border-radius:25px";>&times;  </a>

                                        </div>
                                  </div>
                                </div>
                              </div>
                              <hr>
                              <p style="text-align:center;color:#c21e56" >
                                    Sign up to The Premium's Block for weekly meat specials. emailed straight to your inbox.

                              </p>

                              <div class="row">
                                <div class="input-field col s12 m6 l6">
                                        <input  id="first_name" type="text" class="validate">
                                        <label for="first_name">First Name</label>
                                </div>
                                <div class="input-field col s12 m6 l6">
                                        <input  id="Last_name" type="text" class="validate">
                                        <label for="Last_name">Last Name</label>
                                    </div>
                                    <div class="input-field col s12 m6 l6">
                                            <input  id="Email" type="text" class="validate">
                                            <label for="Email">Email</label>
                                        </div>




                    </div>
                <div class="modal-footer">
                  <a href="#!" class=" modal-action modal-close waves-effect waves-black black white-text btn-flat">Close</a>
                </div>
              </div>   --}}


        {{-- <script src="https://unpkg.com/micromodal/dist/micromodal.min.js"></script> --}}

        @include('script')
        @include('toaster')
        @if(session()->has('failed'))
          <script type="text/javascript">
          toastr.error('{!! session()->get('failed') !!}');
          </script>
        @endif
        @if(session()->has('success'))
          <script type="text/javascript">
          toastr.success('{!! session()->get('success') !!}');
          </script>
        @endif

</body>
</html>

<?php

use Illuminate\Database\Seeder;

class Miscellaneous extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      \App\Miscellaneous::insert([
        'minimumAmount' => 0.0,
        'deliveryCharges' => 0.0
      ]);
    }
}

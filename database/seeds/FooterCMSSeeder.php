<?php

use Illuminate\Database\Seeder;

class FooterCMSSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\FooterCMS::insert([
          'description' => 'Premium Meats we are as good as it sounds. Premium Meats is one of fastest growing retail and wholesale meat supplier in Auckland because we offer the service and quality that has never been experienced.',
          'address' => '5/64 Stoddard Road, Mt. Roskill, Auckland, New Zealand.',
          'phone' => '(+649) 629-6209',
          'landline' => '(+649) 629-6209',
          'email' => 'info@premiummeat.co.nz'
        ]);
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteOrderDateFromOrderInfoAndAddItToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders_info', function (Blueprint $table) {

            $table->dropColumn('order_date');
        });
        Schema::table('orders', function (Blueprint $table) {

            $table->string('order_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('order_info_and_add_it_to_orders', function (Blueprint $table) {
        //     //
        // });
    }
}

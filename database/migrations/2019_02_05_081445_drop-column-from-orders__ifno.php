<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnFromOrdersIfno extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders_info',function(Blueprint $table){
            $table->dropColumn('timeslotsSelection');
        });
        Schema::table('orders',function(Blueprint $table){
            $table->integer('timeslotsSelection');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAvgWeightColumnToTableProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
          $table->increments('id');
          $table->text('description')->nullable();
          $table->integer('cat_id')->unsigned();
          $table->integer('sellingUnit_id')->unsigned();
          $table->float('price');
          $table->string('image')->nullable();
          $table->integer('timeslot')->unsigned();
          $table->integer('status')->default(0);
          $table->integer('arrange')->default(0);
          $table->float('avg_weight')->default(0);
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
          $table->dropColumn('avg_weight')->default(0);
        });
    }
}

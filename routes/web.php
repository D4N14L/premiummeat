<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your     application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/delayed/charge',function(){
//
// });








Route::prefix('api/v1/')->group(function(){

  Route::prefix('/products')->group(function(){
      Route::get('/','ApiController@getProducts');
      Route::get('/{id}','ApiController@getProductById');

  });

  Route::prefix('/cat')->group(function(){
    Route::get('/','ApiController@getCategories');
    Route::get('/{id}','ApiController@getCategoriesById');

  });

});









Route::get('/', 'WelcomeController@welcome');
Route::get('/user/verification/{verifcation_token}','UserController@emailVerification')->name('email.verify');

Auth::routes();


// API ROUTES















 Route::get('/logout' , 'Auth\LoginController@logout')->name('user.logout');


 Route::post('/change-passowrd','UserController@changePassword')->middleware('auth')->name('change.password');


  /* -Ajax
     -- Cities
     --- Suburbs
  */

  Route::get('/suburbs/area','WelcomeController@getSuburbByAreaId')->name('get.suburb.by.area');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/product/{id}','ProductController@byCategory')->name('user.browse.by.category');

  //Add To Cart
  Route::post('/addToCart','CartController@addToCart');
  Route::get('/checkout','CartController@checkout')->name('checkout');
  Route::post('/payment','StripeController@pay')->name('payment');
  Route::post('/deleteSingleOrder','CartController@deleteSingleOrder');
  Route::post('/payment/form','StripeController@getPayForm')->name('payment.from');
  Route::post('/pay','StripeController@paymentRequest')->name('payment.pay');
  Route::get('/payment/procedure','StripeController@getPayFormAfterLogin')->name('payment.pay.after.login');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::post('logout','CustomLogoutController@logout')->middleware('PreventRedirectBack')->name('voyager.logout');

    //Dashboard
    Route::get('/','DashboardController@dashboard')->middleware('admin.user')->name('voyager.dashboard');
    Route::get('/users','UserController@browse')->name('voyager.users.index');
    // Route::get('/users/delete/{id}','UserController@destroy')->name('admin.user.delete');

    //Products CRUD Operation
      //Products ajax manage status
      Route::post('/product/state/manage','ProductController@manageStatus')->middleware('admin.user');
      Route::post('/products/state/arrange','ProductController@managePosition')->middleware('admin.user')->name('product.arrange.positions');
    Route::get('/product/create','ProductController@create')->middleware('admin.user')->name('products.create');
    Route::post('/product/store','ProductController@store')->middleware('admin.user')->name('products.store');
    Route::get('/products','ProductController@browse')->middleware('admin.user')->name('products.browse');
    Route::get('/products/{category}','ProductController@browseByCategory')->middleware('admin.user')->name('products.browse.category');
    Route::get('/products/edit/{id}','ProductController@edit')->middleware('admin.user')->name('products.edit');
    Route::get('/products/delete/{id}','ProductController@delete')->middleware('admin.user')->name('products.delete');
    Route::post('/product/update','ProductController@update')->middleware('admin.user')->name('products.update');

    //selling unit CRUD Operation

    Route::get('/sellingUnits','SellingUnitController@browse')->middleware('admin.user')->name('sellingUnit.browse');
    Route::post('/sellingUnits/create','SellingUnitController@store')->middleware('admin.user')->name('sellingUnit.store');
    Route::get('/sellingUnits/delete/{id}','SellingUnitController@destroy')->middleware('admin.user')->name('sellingUnit.delete');


    //Delivery Time ( TIME SLOTS ) Slots CRUD Operation
    Route::get('/timeslots','TimeSlotController@browse')->middleware('admin.user')->name('timeslots.browse');
    Route::post('/timeslots/store','TimeSlotController@store')->middleware('admin.user')->name('timeslots.store');
          //Delivery Time ( TIME SLOTS ) Slots AJAX Operation
          Route::post('/manageGeneralState','TimeSlotController@manageGeneralState');



    //Categories CRUD Operation
    Route::get('/category/create','CategoryController@create')->middleware('admin.user');
    Route::post('/category/store','CategoryController@store')->middleware('admin.user')->name('category.store');
    Route::post('/category/edit','CategoryController@edit')->middleware('admin.user')->name('category.edit');
    Route::get('/category/delete/{id}','CategoryController@delete')->middleware('admin.user')->name('category.delete');


    // Inventory
    Route::get('/inventory/','InventoryController@browse')->middleware('admin.user');
    Route::post('/inventory/add/quantity','InventoryController@addQuantity')->middleware('admin.user');
    Route::post('/inventory/remove/quantity','InventoryController@removeQuantity')->middleware('admin.user');
    Route::post('/inventory/manage/status','InventoryController@manageStatus')->middleware('admin.user');


    // Manage Service Areas
    Route::get('/areas','ServiceAreaController@index')->middleware('admin.user');
    Route::get('/areas/delete/{id}','ServiceAreaController@deleteArea')->name('area.delete')->middleware('admin.user');
    Route::post('/areas/update','ServiceAreaController@updateArea')->name('area.update')->middleware('admin.user');
    Route::post('areas/add/city','ServiceAreaController@addCity')->middleware('admin.user');

        // Manage Suburbs
        Route::post('areas/add/suburb','ServiceAreaController@addSuburb')->middleware('admin.user');
        Route::get('areas/delete/suburb/{id}','ServiceAreaController@deleteSuburb')->name('suburb.delete')->middleware('admin.user');
        Route::post('areas/update/suburb','ServiceAreaController@updateSuburb')->name('suburb.update')->middleware('admin.user');


    //CMS
    Route::get('/cms','CMSController@index')->middleware('admin.user')->name('admin.cms.index');
    Route::post('/cms/add/slider','CMSController@addSlider')->middleware('admin.user')->name('cms.slider.add');
    Route::post('/cms/update/portion','CMSController@updatePortion')->middleware('admin.user')->name('cms.update.portion');
    Route::get('/cms/mange/slides','CMSController@manageSlides')->middleware('admin.user')->name('cms.manage.slides');
    Route::post('/cms/mange/slides','CMSController@postManageSlides')->middleware('admin.user')->name('cms.manage.post.slides');
    Route::post('/cms/manage/footer','CMSController@updateFooter')->middleware('admin.user')->name('cms.manage.post.footer');
    Route::post('/cms/manage/website/page','CMSController@updateWebsiteColor')->middleware('admin.user')->name('cms.website.color');
    Route::post('/cms/update/slider/position','CMSController@updatePosition')->middleware('admin.user')->name('cms.update.slider.position');
    Route::post('/cms/update/miscellaneous','CMSController@updateMiscellaneous')->middleware('admin.user')->name('updateMiscellaneous');

    //Order

    Route::get('/orders','OrderController@index')->middleware('admin.user')->name('admin.order.index');
    Route::get('/orders/charged','OrderController@chargedOrders')->middleware('admin.user')->name('admin.order.index.charged');
    Route::get('/orders/details/{id}','OrderController@details')->middleware('admin.user')->name('admin.order.details');
      // manage extra quantity of product
      Route::post('/orders/details/{id}','OrderController@manageDetails')->middleware('admin.user')->name('admin.order.details');
    Route::get('/order/status/check/{id}','OrderController@markStatus')->middleware('admin.user')->name('admin.order.mark');
    Route::get('/order/generateReport/{order_id}','OrderController@generateReport')->middleware('admin.user')->name('admin.order.report.generate');

    // order charge
    Route::get('/orders/charge/{customer_id}/{amount}/{order_id}','OrderController@customerCharge')->name('admin.charge.customer');

   //promotions

   Route::get('/promotions','PromotionsController@getUsersPromotions')->middleware('admin.user');
   Route::get('/promotions/info/{id}','PromotionsController@getUserReferalInformation')->middleware('admin.user')->name('promotions.info.users');
   Route::post('/post/promotion','PromotionsController@postPromotion')->middleware('admin.user')->name('promotions.promote');





   //Service Type

   Route::get('/serviceTypes','ServiceTypeController@browser')->middleware('admin.user')->name('serviceType.browse');
   Route::post('/serviceTypes','ServiceTypeController@store')->middleware('admin.user')->name('serviceType.store');
   Route::post('/serviceTypes/edit','ServiceTypeController@store')->middleware('admin.user')->name('serviceType.edit');
   Route::get('/serviceTypes/delete/{id}','ServiceTypeController@destroy')->middleware('admin.user')->name('serviceType.delete');

   //Service Type Options

   Route::get('/serviceTypeOptions','ServiceTypeOptionsController@browse')->middleware('admin.user')->name('serviceTypeOptions.browse');
   Route::post('/serviceTypeOptions','ServiceTypeOptionsController@store')->middleware('admin.user')->name('serviceTypeOptions.store');
   Route::post('/serviceTypeOptions/edit/serviceoptions','ServiceTypeOptionsController@store')->middleware('admin.user')->name('serviceTypeOptions.edit');
   Route::get('/serviceTypeOptions/delete/{id}','ServiceTypeOptionsController@destroy')->middleware('admin.user')->name('serviceTypeOptions.delete');


});

  Route::get('/chicken/products','ProductController@chickenCategory');
  Route::get('/green/chicken/products','ProductController@greenChickenCategory');


  Route::get('/user/manager','UserController@getUserManager')->middleware('auth')->name('user.getUserManager');
  Route::post('/user/manager/update/profile','UserController@updateProfile')->middleware('auth')->name('user.edit.profile');
  Route::get('/user/manager/profile/edit/form','UserController@getEditForm')->middleware('auth')->name('user.get.edit.profile');

  //Referrals

  Route::get('/refer/{referer_id}','ReferalController@register')->name('register.referal');
  Route::post('/register/referer','ReferalController@submit')->name('register.post.referal');
  //ajax getServiceTypes
  Route::get('/getServiceTypes','ServiceTypeOptionsController@getServiceTypes');

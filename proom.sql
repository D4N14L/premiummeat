-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 12, 2019 at 06:27 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `proom`
--

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '', 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '', 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '', 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '', 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, '', 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\"}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'locale', 'text', 'Locale', 0, 1, 1, 1, 1, 0, '', 12),
(12, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '', 12),
(13, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(14, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(15, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 3),
(16, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 4),
(17, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(18, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(19, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 3),
(20, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 4),
(21, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '', 5),
(22, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, '', 9);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', '', '', 1, 0, NULL, '2018-10-30 07:36:22', '2018-10-30 07:36:22'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2018-10-30 07:36:22', '2018-10-30 07:36:22'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2018-10-30 07:36:23', '2018-10-30 07:36:23');

-- --------------------------------------------------------

--
-- Table structure for table `ethnic`
--

CREATE TABLE `ethnic` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `footer_cms`
--

CREATE TABLE `footer_cms` (
  `id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `landline` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `footer_cms`
--

INSERT INTO `footer_cms` (`id`, `description`, `address`, `phone`, `landline`, `email`, `created_at`, `updated_at`) VALUES
(1, 'Premium Meats we are as good as it sounds. Premium Meats is one of fastest growing retail and wholesale meat supplier in Auckland because we offer the service and quality that has never been experienced.', '5/64 Stoddard Road, Mt. Roskill, Auckland, New Zealand.', '(+649) 629-6209', '(+649) 629-6209', 'info@premiummeat.co.nz', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `frontend_cms`
--

CREATE TABLE `frontend_cms` (
  `id` int(10) UNSIGNED NOT NULL,
  `backgroundColor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '#A61531',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `frontend_cms`
--

INSERT INTO `frontend_cms` (`id`, `backgroundColor`, `created_at`, `updated_at`) VALUES
(1, '#A61531', NULL, '2019-01-04 08:22:09');

-- --------------------------------------------------------

--
-- Table structure for table `general_timeslot`
--

CREATE TABLE `general_timeslot` (
  `id` int(11) UNSIGNED NOT NULL,
  `shit_name` varchar(255) NOT NULL DEFAULT 'general',
  `timeslot_id` int(11) NOT NULL,
  `isActive` smallint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_timeslot`
--

INSERT INTO `general_timeslot` (`id`, `shit_name`, `timeslot_id`, `isActive`) VALUES
(5, 'general', 7, 1),
(6, 'general', 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `inventory`
--

CREATE TABLE `inventory` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT '0',
  `sold` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `inventory`
--

INSERT INTO `inventory` (`id`, `product_id`, `quantity`, `sold`, `created_at`, `updated_at`) VALUES
(1, 2, 75, 0, '2018-11-03 17:24:26', '0000-00-00 00:00:00'),
(2, 4, 0, 0, '2018-11-03 17:24:26', '0000-00-00 00:00:00'),
(3, 5, 10, 0, '2018-11-03 18:16:59', '0000-00-00 00:00:00'),
(4, 6, 218, 18, '2018-11-03 18:22:05', '0000-00-00 00:00:00'),
(5, 7, 0, 0, '2019-01-01 08:06:25', '2019-01-01 08:06:25'),
(6, 8, 0, 0, '2019-01-08 05:29:51', '2019-01-08 05:29:51');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2018-10-30 07:36:24', '2018-10-30 07:36:24');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '/admin', '_self', 'voyager-boat', '#000000', NULL, 1, '2018-10-30 07:36:24', '2018-11-04 04:07:12', NULL, ''),
(3, 1, 'Manage Users', 'admin/users', '_self', 'voyager-person', '#000000', NULL, 6, '2018-10-30 07:36:24', '2018-11-25 05:29:29', NULL, ''),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2018-10-30 07:36:24', '2018-11-28 05:05:36', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2018-10-30 07:36:24', '2018-10-30 14:08:05', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2018-10-30 07:36:24', '2018-10-30 14:08:05', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2018-10-30 07:36:24', '2018-10-30 14:08:05', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2018-10-30 07:36:24', '2018-10-30 14:08:05', 'voyager.bread.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2018-10-30 07:36:30', '2018-11-28 05:05:36', 'voyager.hooks', NULL),
(13, 1, 'Manage Products', 'admin/products', '_self', 'voyager-bag', '#000000', 20, 2, '2018-10-30 14:09:32', '2018-11-25 05:25:18', NULL, ''),
(14, 1, 'Manage Category', 'admin/category/create', '_self', 'voyager-categories', '#000000', 20, 1, '2018-10-30 14:10:30', '2018-11-25 05:25:12', NULL, ''),
(15, 1, 'Manage Selling Units', 'admin/sellingUnits', '_self', 'voyager-pie-chart', '#000000', 20, 3, '2018-10-31 00:53:57', '2018-11-25 05:25:19', NULL, ''),
(16, 1, 'Delivery Time Slots', 'admin/timeslots', '_self', 'voyager-alarm-clock', '#000000', 20, 4, '2018-10-31 02:47:49', '2018-11-25 05:25:29', NULL, ''),
(17, 1, 'Manage Inventory', '/admin/inventory', '_self', 'voyager-backpack', '#000000', NULL, 4, '2018-11-02 12:42:54', '2018-11-25 05:29:29', NULL, ''),
(18, 1, 'Manage CMS', 'admin/cms', '_self', 'voyager-campfire', '#000000', NULL, 7, '2018-11-03 04:15:50', '2018-11-25 05:29:29', NULL, ''),
(19, 1, 'Manage Orders', '/admin/orders', '_self', 'voyager-barbeque', '#000000', NULL, 5, '2018-11-04 00:53:10', '2018-11-25 05:29:29', NULL, ''),
(20, 1, 'Products', '', '_self', 'voyager-bread', '#000000', NULL, 2, '2018-11-25 05:24:36', '2018-11-25 05:28:20', NULL, ''),
(21, 1, 'Promotions', 'admin/promotions', '_self', 'voyager-bomb', '#000000', NULL, 3, '2018-11-25 05:29:21', '2018-11-25 05:29:29', NULL, ''),
(22, 1, 'Service Types', '', '_self', 'voyager-anchor', '#000000', NULL, 8, '2018-11-28 05:02:49', '2018-11-28 05:05:36', NULL, ''),
(23, 1, 'Services', '/admin/serviceTypes', '_self', 'voyager-polaroid', '#000000', 22, 1, '2018-11-28 05:03:29', '2018-11-28 05:03:35', NULL, ''),
(24, 1, 'Service Options', '/admin/serviceTypeOptions', '_self', 'voyager-dot-3', '#000000', 22, 2, '2018-11-28 05:04:29', '2018-11-28 05:04:37', NULL, ''),
(26, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2018-12-13 06:22:57', '2018-12-13 06:22:57', 'voyager.media.index', NULL),
(27, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2018-12-13 06:22:57', '2018-12-13 06:22:57', 'voyager.users.index', NULL),
(28, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2018-12-13 06:22:57', '2018-12-13 06:22:57', 'voyager.roles.index', NULL),
(29, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2018-12-13 06:22:57', '2018-12-13 06:22:57', 'voyager.settings.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_05_19_173453_create_menu_table', 1),
(6, '2016_10_21_190000_create_roles_table', 1),
(7, '2016_10_21_190000_create_settings_table', 1),
(8, '2016_11_30_135954_create_permission_table', 1),
(9, '2016_11_30_141208_create_permission_role_table', 1),
(10, '2016_12_26_201236_data_types__add__server_side', 1),
(11, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(12, '2017_01_14_005015_create_translations_table', 1),
(13, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(14, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(15, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(16, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(17, '2017_08_05_000000_add_group_to_settings_table', 1),
(18, '2017_11_26_013050_add_user_role_relationship', 1),
(19, '2017_11_26_015000_create_user_roles_table', 1),
(20, '2018_03_11_000000_add_user_settings', 1),
(21, '2018_03_14_000000_add_details_to_data_types_table', 1),
(22, '2018_03_16_000000_make_settings_value_nullable', 1),
(23, '2018_12_22_051621_add-footer-cms', 2),
(24, '2019_01_04_081210_create-website-frontend-cms', 3),
(25, '2019_01_12_113636_add-order-no-slides', 4);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `order_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `created_at`, `updated_at`, `order_status`) VALUES
(3, 4, '2019-01-01 13:24:51', '2018-11-04 09:38:54', 1),
(4, 8, '2018-11-24 23:59:01', '2018-11-24 23:59:01', 0),
(5, 8, '2019-01-01 13:21:24', '2019-01-01 08:21:24', 1),
(6, 8, '2018-12-07 09:12:10', '2018-12-07 04:12:10', 1),
(7, 8, '2019-01-01 13:21:21', '2019-01-01 08:21:21', 1),
(8, 8, '2018-12-07 09:12:08', '2018-12-07 04:12:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders_info`
--

CREATE TABLE `orders_info` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `service_type_option` int(1) NOT NULL,
  `additional_information` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders_info`
--

INSERT INTO `orders_info` (`id`, `product_id`, `order_id`, `amount`, `service_type_option`, `additional_information`) VALUES
(72, 2, 3, 102, 0, NULL),
(73, 4, 3, 102, 0, NULL),
(74, 5, 3, 102, 0, NULL),
(75, 6, 3, 102, 0, NULL),
(76, 2, 3, 102, 0, NULL),
(77, 4, 3, 102, 0, NULL),
(78, 6, 3, 102, 0, NULL),
(79, 4, 4, 234, 0, NULL),
(80, 4, 4, 234, 0, NULL),
(81, 4, 4, 234, 0, NULL),
(82, 4, 4, 234, 0, NULL),
(83, 4, 4, 234, 0, NULL),
(84, 5, 4, 250, 0, NULL),
(85, 5, 4, 250, 0, NULL),
(86, 5, 4, 250, 0, NULL),
(87, 5, 4, 250, 0, NULL),
(88, 6, 4, 12, 0, NULL),
(89, 6, 4, 12, 0, NULL),
(90, 6, 4, 12, 0, NULL),
(91, 4, 7, 234, 5, NULL),
(92, 5, 7, 250, 4, NULL),
(93, 4, 8, 234, 5, 'klj'),
(94, 4, 8, 234, 5, NULL),
(95, 5, 8, 250, 4, 'klklj'),
(96, 5, 8, 250, 4, 'klklj'),
(97, 5, 8, 250, 4, 'klklj'),
(98, 5, 8, 250, 4, 'klklj'),
(99, 5, 8, 250, 4, 'klklj'),
(100, 5, 8, 250, 4, 'klklj'),
(101, 5, 8, 250, 4, 'klklj');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2018-10-30 07:36:25', '2018-10-30 07:36:25'),
(2, 'browse_bread', NULL, '2018-10-30 07:36:25', '2018-10-30 07:36:25'),
(3, 'browse_database', NULL, '2018-10-30 07:36:25', '2018-10-30 07:36:25'),
(4, 'browse_media', NULL, '2018-10-30 07:36:25', '2018-10-30 07:36:25'),
(5, 'browse_compass', NULL, '2018-10-30 07:36:25', '2018-10-30 07:36:25'),
(6, 'browse_menus', 'menus', '2018-10-30 07:36:25', '2018-10-30 07:36:25'),
(7, 'read_menus', 'menus', '2018-10-30 07:36:25', '2018-10-30 07:36:25'),
(8, 'edit_menus', 'menus', '2018-10-30 07:36:25', '2018-10-30 07:36:25'),
(9, 'add_menus', 'menus', '2018-10-30 07:36:25', '2018-10-30 07:36:25'),
(10, 'delete_menus', 'menus', '2018-10-30 07:36:25', '2018-10-30 07:36:25'),
(11, 'browse_roles', 'roles', '2018-10-30 07:36:25', '2018-10-30 07:36:25'),
(12, 'read_roles', 'roles', '2018-10-30 07:36:25', '2018-10-30 07:36:25'),
(13, 'edit_roles', 'roles', '2018-10-30 07:36:26', '2018-10-30 07:36:26'),
(14, 'add_roles', 'roles', '2018-10-30 07:36:26', '2018-10-30 07:36:26'),
(15, 'delete_roles', 'roles', '2018-10-30 07:36:26', '2018-10-30 07:36:26'),
(16, 'browse_users', 'users', '2018-10-30 07:36:26', '2018-10-30 07:36:26'),
(17, 'read_users', 'users', '2018-10-30 07:36:26', '2018-10-30 07:36:26'),
(18, 'edit_users', 'users', '2018-10-30 07:36:26', '2018-10-30 07:36:26'),
(19, 'add_users', 'users', '2018-10-30 07:36:26', '2018-10-30 07:36:26'),
(20, 'delete_users', 'users', '2018-10-30 07:36:26', '2018-10-30 07:36:26'),
(21, 'browse_settings', 'settings', '2018-10-30 07:36:26', '2018-10-30 07:36:26'),
(22, 'read_settings', 'settings', '2018-10-30 07:36:26', '2018-10-30 07:36:26'),
(23, 'edit_settings', 'settings', '2018-10-30 07:36:27', '2018-10-30 07:36:27'),
(24, 'add_settings', 'settings', '2018-10-30 07:36:27', '2018-10-30 07:36:27'),
(25, 'delete_settings', 'settings', '2018-10-30 07:36:27', '2018-10-30 07:36:27'),
(26, 'browse_hooks', NULL, '2018-10-30 07:36:30', '2018-10-30 07:36:30');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1);

-- --------------------------------------------------------

--
-- Table structure for table `portion`
--

CREATE TABLE `portion` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `portion`
--

INSERT INTO `portion` (`id`, `name`) VALUES
(1, 'sausage'),
(2, 'steak'),
(3, 'footer'),
(4, 'footer'),
(5, 'eula');

-- --------------------------------------------------------

--
-- Table structure for table `portions`
--

CREATE TABLE `portions` (
  `id` int(11) NOT NULL,
  `portion_1` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `portions`
--

INSERT INTO `portions` (`id`, `portion_1`, `image`, `title`, `body`) VALUES
(1, '1', 'sausage.jpg', 'Sausages', 'Sausage making is an outcome of efficient butchery. Traditionally, sausage makers would salt various tissues and organs such as scraps, organ meats, blood, and fat to help preserve them. They would then stuff them into tubular casings made from the cleaned intestines of the animal, producing the characteristic cylindrical shape. Hence, sausages, puddings, and salami are among the oldest of prepared foods, whether cooked and eaten immediately or dried to varying degrees.'),
(2, '2', 'steak.jpg', 'Steaks', 'A steak is a meat generally sliced across the muscle fibers, potentially including a bone. Exceptions, in which the meat is sliced parallel to the fibers, include the skirt steak cut from the plate, the flank steak cut from the abdominal muscles, and the Silver finger steak cut from the loin and includes three rib bones. In a larger sense, there are also fish steaks, ground meat steaks, pork steak and many more varieties of steak'),
(3, '3', '', 'Learn More About Us', 'A place made with love we are as good as it sounds. Premium Meats is one of fastest growing retail and wholesale meat supplier in Auckland because we offer the service and quality that has never been experienced. We are 100% halal and specialized in beef, Lamb, goat and poultry. We are well known for our high quality meat and hygiene standards. We source our meat and poultry only from local NZ meat market. Because of our bulk buying we are able to offer best quality product in very reasonable price.'),
(4, '5', '', 'EULA', '<h2>End-User License Agreement (EULA) of premiummeat.co.nz</h2>\r\n\r\n<p>This End-User License Agreement (\"EULA\") is a legal agreement between you and <span class=\"company_name\">Premiummeat</span></p>\r\n\r\n<p>This EULA agreement governs your acquisition and use of our <span class=\"app_name\">premiummeat.co.nz</span> software (\"Software\") directly from <span class=\"company_name\">Premiummeat</span> or indirectly through a <span class=\"company_name\">Premiummeat</span> authorized reseller or distributor (a \"Reseller\").</p>\r\n\r\n<p>Please read this EULA agreement carefully before completing the installation process and using the <span class=\"app_name\">premiummeat.co.nz</span> software. It provides a license to use the <span class=\"app_name\">premiummeat.co.nz</span> software and contains warranty information and liability disclaimers.</p>\r\n\r\n<p>If you register for a free trial of the <span class=\"app_name\">premiummeat.co.nz</span> software, this EULA agreement will also govern that trial. By clicking \"accept\" or installing and/or using the <span class=\"app_name\">premiummeat.co.nz</span> software, you are confirming your acceptance of the Software and agreeing to become bound by the terms of this EULA agreement.</p>\r\n\r\n<p>If you are entering into this EULA agreement on behalf of a company or other legal entity, you represent that you have the authority to bind such entity and its affiliates to these terms and conditions. If you do not have such authority or if you do not agree with the terms and conditions of this EULA agreement, do not install or use the Software, and you must not accept this EULA agreement.</p>\r\n\r\n<p>This EULA agreement shall apply only to the Software supplied by <span class=\"company_name\">Premiummeat</span> herewith regardless of whether other software is referred to or described herein. The terms also apply to any <span class=\"company_name\">Premiummeat</span> updates, supplements, Internet-based services, and support services for the Software, unless other terms accompany those items on delivery. If so, those terms apply.</p>\r\n\r\n<h3>License Grant</h3>\r\n\r\n<p><span class=\"company_name\">Premiummeat</span> hereby grants you a personal, non-transferable, non-exclusive licence to use the <span class=\"app_name\">premiummeat.co.nz</span> software on your devices in accordance with the terms of this EULA agreement.</p>\r\n\r\n<p>You are permitted to load the <span class=\"app_name\">premiummeat.co.nz</span> software (for example a PC, laptop, mobile or tablet) under your control. You are responsible for ensuring your device meets the minimum requirements of the <span class=\"app_name\">premiummeat.co.nz</span> software.</p>\r\n\r\n<p>You are not permitted to:</p>\r\n\r\n<ul>\r\n<li>Edit, alter, modify, adapt, translate or otherwise change the whole or any part of the Software nor permit the whole or any part of the Software to be combined with or become incorporated in any other software, nor decompile, disassemble or reverse engineer the Software or attempt to do any such things</li>\r\n<li>Reproduce, copy, distribute, resell or otherwise use the Software for any commercial purpose</li>\r\n<li>Allow any third party to use the Software on behalf of or for the benefit of any third party</li>\r\n<li>Use the Software in any way which breaches any applicable local, national or international law</li>\r\n<li>use the Software for any purpose that <span class=\"company_name\">Premiummeat</span> considers is a breach of this EULA agreement</li>\r\n</ul>\r\n\r\n<h3>Intellectual Property and Ownership</h3>\r\n\r\n<p><span class=\"company_name\">Premiummeat</span> shall at all times retain ownership of the Software as originally downloaded by you and all subsequent downloads of the Software by you. The Software (and the copyright, and other intellectual property rights of whatever nature in the Software, including any modifications made thereto) are and shall remain the property of <span class=\"company_name\">Premiummeat</span>.</p>\r\n\r\n<p><span class=\"company_name\">Premiummeat</span> reserves the right to grant licences to use the Software to third parties.</p>\r\n\r\n<h3>Termination</h3>\r\n\r\n<p>This EULA agreement is effective from the date you first use the Software and shall continue until terminated. You may terminate it at any time upon written notice to <span class=\"company_name\">Premiummeat</span>.</p>\r\n\r\n<p>This EULA was created by <a href=\"https://eulatemplate.com\">EULA Template</a> for <span class=\"app_name\">premiummeat.co.nz</span></p>\r\n\r\n<p>It will also terminate immediately if you fail to comply with any term of this EULA agreement. Upon such termination, the licenses granted by this EULA agreement will immediately terminate and you agree to stop all access and use of the Software. The provisions that by their nature continue and survive will survive any termination of this EULA agreement.</p>\r\n\r\n<h3>Governing Law</h3>\r\n\r\n<p>This EULA agreement, and any dispute arising out of or in connection with this EULA agreement, shall be governed by and construed in accordance with the laws of <span class=\"country\">nz</span>.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `product-category`
--

CREATE TABLE `product-category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product-category`
--

INSERT INTO `product-category` (`id`, `name`, `image`) VALUES
(1, 'Beef', '1540936208.svg'),
(2, 'Lamb', '1540936223.svg'),
(3, 'Goat', '1540936237.svg'),
(4, 'Chicken', '1540936250.svg'),
(5, 'Free Range Chicken', '1544289876.svg'),
(7, 'Whole Bird', '1543139680.jpeg'),
(8, 'pieces', '1543139693.jpeg'),
(9, 'Small Goods', '1540936265.svg'),
(10, 'Green Chicken Cut Pieces', '1544289933.jpeg'),
(11, 'Green Chicken Whole Bird', '1544289950.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `sellingUnit_id` int(11) NOT NULL,
  `price` int(11) NOT NULL DEFAULT '0',
  `image` varchar(255) NOT NULL,
  `timeslot` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `cat_id`, `sellingUnit_id`, `price`, `image`, `timeslot`, `status`, `created_at`, `updated_at`) VALUES
(2, 'dlkasj', '<kjdsalkj', 1, 1, 102, '1541248705.jpg', 0, 1, '2018-12-12 17:31:39', '2018-11-03 07:41:19'),
(4, 'meat', 'Yummy!', 3, 1, 234, '1541248806.jpg', 0, 1, '2018-12-12 17:31:37', '2018-11-03 07:40:06'),
(5, 'Product 4', 'Thats Cool!', 3, 2, 250, '1541248837.jpg', 8, 0, '2018-11-06 08:02:53', '2018-11-06 03:02:53'),
(6, 'Product 5', '<Beef product', 7, 2, 12, '1541248871.jpg', 0, 0, '2018-12-08 19:19:46', '2018-12-08 14:19:46'),
(7, 'kljkjl', 'lkjdasdskja', 1, 1, 123, '1546347985.png', 0, 1, '2019-01-01 08:06:25', '2019-01-01 08:06:25'),
(8, 'das', 'dsa', 1, 1, 12, '1546943391.jpg', 0, 1, '2019-01-08 05:29:51', '2019-01-08 05:29:51');

-- --------------------------------------------------------

--
-- Table structure for table `referal`
--

CREATE TABLE `referal` (
  `id` int(11) UNSIGNED NOT NULL,
  `referer_id` int(11) NOT NULL,
  `referal_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `referal`
--

INSERT INTO `referal` (`id`, `referer_id`, `referal_id`, `created_at`, `updated_at`) VALUES
(1, 10, 14, '2018-11-25 02:30:51', '2018-11-25 02:30:51');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2018-10-30 07:34:54', '2018-10-30 07:34:54'),
(2, 'user', 'Normal User', '2018-10-30 07:36:25', '2018-10-30 07:36:25');

-- --------------------------------------------------------

--
-- Table structure for table `SellingUnits`
--

CREATE TABLE `SellingUnits` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `SellingUnits`
--

INSERT INTO `SellingUnits` (`id`, `name`) VALUES
(1, 'Kg'),
(2, 'Lbs');

-- --------------------------------------------------------

--
-- Table structure for table `service_type`
--

CREATE TABLE `service_type` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `cat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_type`
--

INSERT INTO `service_type` (`id`, `name`, `cat_id`) VALUES
(4, 'Pieces', 2),
(5, 'dsadaskj', 1),
(6, 'asd', 3);

-- --------------------------------------------------------

--
-- Table structure for table `service_type_options`
--

CREATE TABLE `service_type_options` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `service_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_type_options`
--

INSERT INTO `service_type_options` (`id`, `name`, `service_type_id`) VALUES
(4, 'hello', 5),
(5, 'world', 4),
(7, 'sada', 6);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Premium Meats', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Voyager. The Missing Admin for Laravel', '', 'text', 3, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 2, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) UNSIGNED NOT NULL,
  `image` varchar(255) NOT NULL,
  `arrange` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `image`, `arrange`) VALUES
(1, 'slide1.jpg', 4),
(2, 'slide2.jpg', 3),
(3, 'slide3.jpg', 1),
(9, 'slide4.jpg', 2);

-- --------------------------------------------------------

--
-- Table structure for table `timeslots`
--

CREATE TABLE `timeslots` (
  `id` int(11) NOT NULL,
  `shift` varchar(255) NOT NULL,
  `startTime` varchar(255) NOT NULL,
  `endTime` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `timeslots`
--

INSERT INTO `timeslots` (`id`, `shift`, `startTime`, `endTime`) VALUES
(7, 'morning', '03:30', '06:30'),
(8, 'evening', '18:20', '10:25');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `ethnic` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'NZ European',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referral_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `verification_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isVerified` int(1) NOT NULL DEFAULT '0',
  `promotion` int(11) NOT NULL DEFAULT '0',
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `landline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `isAgreementSigned` smallint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `ethnic`, `name`, `referral_link`, `email`, `verification_token`, `isVerified`, `promotion`, `avatar`, `address`, `landline`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`, `isAgreementSigned`) VALUES
(4, 1, '0', 'admin', '', 'metalcool07@yahoo.com', '', 0, 0, 'users/default.png', '', '', NULL, '$2y$10$u5OxwEkJZaOYscCc66IK0eW.THg4S.bHZuxTBOcplb/lrTkfevqpq', 'lRQnGGkLfmgsbLE3zw6bYfFgcjNmIjKdnfDEYa1XHQsBuGiWLqF3TXp7XqAc', NULL, '2018-10-30 07:38:25', '2018-10-30 07:38:26', 0),
(5, 2, '0', 'named', '', 'name@app.com', '', 0, 0, 'users/default.png', 'address', '1234', NULL, '$2y$10$siAztyX8CfiloX/HodkSweRH9zSbpUWYEkJs843ti.ftKJdO0IX06', 'JyrRHb1Hb7xtcgKODWpnCNlSfD4u6fZKPCoNnUTQGuHXf3ThVamrsvZfjSqq', NULL, '2018-11-04 00:13:09', '2018-11-04 00:13:09', 0),
(6, 2, '0', 'jk', '', 'da@dd.com', '', 0, 0, 'users/default.png', '123', '321', NULL, '$2y$10$6m5RITzj3vK4JkhE/NWqtOUANDQ93KtzfN3cNs48FK6c.6r/VUlwC', 'K26nremvqzhAhNaxQ1WIogrfeOTdqHsNaOsdfbG2fdEtAnNwQkWWcszyT0zh', NULL, '2018-11-05 07:41:14', '2018-11-05 07:41:14', 1),
(7, 2, 'NZ European', 'asdjk', '', 'das@dad.com', '', 0, 0, 'users/default.png', '123123', '12313', NULL, '$2y$10$edS43ndk9ds./hSCbqVlregw5mbmejfoGM4Rpw2l4O5NIPfAARjkS', 'wBe9uQx1sqSaARmfZ7lcW9GRgDyuX36HdHSgF9S14RrFfJs4dgvTyZaheusg', NULL, '2018-11-06 08:51:21', '2018-11-06 08:51:21', 1),
(8, 2, 'Chinese', 'Danial', '', 'user@user.com', '', 0, 0, 'users/default.png', 'dsasaddsasd', '23123213', NULL, '$2y$10$US227OjD7xh5ffPdt2hpJuQMbNfHEaPdwZn7fgKtMaA0lhleQBDme', '85iYfLBTBAU4AQ1LbWWyrLTXlTFBcnGm4rhCOVG0XpDbtM0z9119qLameiar', NULL, '2018-11-24 04:53:45', '2018-12-01 03:44:36', 1),
(9, 2, 'Arabs', 'DanielOX', '$2y$10$F9udyiEj293jInV5FMRcE.2fiPLPmSl4Rf7Ouj.seA6JpauV3Q5gu', 'cool@app.com', '', 0, 20, 'users/default.png', 'eeqwewqe1', '12312312', NULL, '$2y$10$3Wl96/BTuHw1Fg6pst2RSeFnj8CZMRSQUvlgLAQkO7ldicMEs3.yi', NULL, NULL, '2018-11-25 01:02:46', '2018-11-25 07:06:30', 0),
(10, 2, 'Pakistanies', 'awais', '$2y$10$5FzPNcNz5gGusOfTSL/AWuXIlOYqURkBlpiOewNLX9Hjy3ZNeP41O', 'awais@baloch.com', '', 0, 0, 'users/default.png', 'dsadasa', '2131312', NULL, '$2y$10$IZBY7RtcfvW8BysH0A1KXOHLxfP9EJt4uYezq2m8qfp41j3AmgOiO', '4Yo27Krd1p6v1019M04TTrnoAz2CpnOwzATTbOEMcrMMv7KmGO5IQjyYUG9e', NULL, '2018-11-25 01:09:35', '2018-11-25 01:09:35', 1),
(14, 2, 'Chinese', 'jack', '$2y$10$qZ101LdbqtJB.6zoaREvjecur9kWRE445SNgMjp3y7WaoDiOCcaS2', 'jack@gmail.com', '', 0, 10, 'users/default.png', 'dada', 'dsads', NULL, '$2y$10$ZzIyZnvUY6khH5qyBwLM..gtYTJxU7CknWBJDnOXR9jaBtASAmxSC', 'Y2MgjDJzw8w8UjassOxTBPhsadzpDmyIYjpKcn1cvhbl0ezSYL34j4oGczN0', NULL, '2018-11-25 02:30:51', '2018-11-25 07:06:27', 0),
(15, 2, 'NZ European', 'DanielOX', '$2y$10$pAeN51XgJ2mfTjaveBDHLeT11DDdAWerSU9Cs78tPdv8qVOukhpJK', 'ddsasad@ccs.com', '', 0, 0, 'users/default.png', 'sdaasd', '3212312', NULL, '$2y$10$lUWO1uSJZC8hpGw19XD71eZ7aH4FvvC.AVCvUSTQxbySAmmR150oe', NULL, NULL, '2018-12-02 01:40:19', '2018-12-02 01:40:19', 1),
(16, 2, 'NZ European', 'DanielOX', '$2y$10$r0AR3pV2ylZM230I9qt0euYt6tkPllZGOyamAwuTWkRAsExRyyA7S', 'ddd@ddd.com', '', 0, 0, 'users/default.png', 'sdaasd', '3212312', NULL, '$2y$10$1ds3x2p.nZvXIz.JSCwkru8wZ6hmniFlHOsSXKupfx5ptwvKyPV9y', NULL, NULL, '2018-12-02 01:41:05', '2018-12-02 01:41:05', 1),
(17, 2, 'NZ European', 'DanielOX', '$2y$10$BjtJ3TB7YQPsnyEJuVPxuepe1C4kHmg95muvB8swS20./l9FbAvBW', 'ddd@dddx.com', '', 0, 0, 'users/default.png', 'sdaasd', '3212312', NULL, '$2y$10$eO2F6.vaXZe6jIRKT84w7uQZYMs/PGVpBh/BaMFaCjyNuLdQ91A7K', 'iEC2tn0UhlKiQjGHPD6zHZ8pD5tJWdTFJQ3wQH1hex88fLsJBmO89R7lZxn5', NULL, '2018-12-02 01:45:10', '2018-12-02 01:45:10', 1),
(18, 2, 'NZ European', 'asklmdslkasd', '$2y$10$Rx33ausvAjx5Hr6lPQYGc.v8wgg64X3AIYRileB6eW7frkSyN2sJC', 'dsaadsas@dasds.com', '', 0, 0, 'users/default.png', 'as', '123123132', NULL, '$2y$10$t5GXGDs1lQ4sgs7xKp8XR.cSslyr7ewcvATOhkoYdMPrPyyNQlvjy', 'lW0VWWQNQQfUhNxr2j28xkNIYGgIZ3IO8cfRzcugKNUUBB45smIQgbCmuPlY', NULL, '2018-12-02 01:48:06', '2018-12-02 01:48:07', 1),
(19, 2, 'NZ European', 'odpadapso', '$2y$10$/kG7lxCEvRFCsvxLmDE98.LXypfe8VmzCKnMvkBEz6Jc.RC8eC7hK', 'peeps@peeps.com', '', 0, 0, 'users/default.png', 'dasads', '123', NULL, '$2y$10$2WVzU5Kt58ydefSMVYtev..YyZkG3vo/DLJfhJbawvn26g0O4dOve', 'uieRrkCOsk2pEb50E2132oRhxLOvtV4UgvPsmQDMCCWGtT1vofikuyLgkUEj', NULL, '2018-12-02 01:49:56', '2018-12-02 01:49:56', 1),
(20, 2, 'NZ European', 'sadsa', '$2y$10$Ag8tVONWU9IYEP55vy9iVOiTGHvucr/IWJp.Tfng1/dIepdTExkrq', 'dasdas@ccc.com', '', 0, 0, 'users/default.png', 'asdasd', '123123', NULL, '$2y$10$hPvYerSepIjoKsP5u38Hbu5/moRyhX8Q2X5O3etaku0vVIibGHkxO', 'nY9GB05mNKMl8DGUKgLm0MOP3UK7eNEdnkg1q5DCWnFvt9j8b4MOMkYOpQAv', NULL, '2018-12-02 01:53:23', '2018-12-02 01:53:23', 1),
(21, 2, 'NZ European', 'dsads', '$2y$10$0UB.VJKZ9DIntDSPtbPxReni3Cnnypl2iQxhWn6Z3y7uFHBB1wrQO', 'ads@dddddd.com', '', 0, 0, 'users/default.png', 'sada', '12312', NULL, '$2y$10$1Gfp31v0S5xxoVBZqnatXuh1igBFnCbXeV2mpygopiBG7K9djKJ9q', NULL, NULL, '2018-12-02 01:55:51', '2018-12-02 01:55:51', 1),
(22, 2, 'NZ European', 'dasds', '$2y$10$VwNTMj13/NLx63bkv8BcNuLzSa5bPT7H37k1bepcRcBVaNin3L/CW', 'saddsa@sdaads.com', '', 0, 0, 'users/default.png', 'asdasad', '123', NULL, '$2y$10$8t1dQVyy68a3UwYrRZtoLeHvE7GqM1QL7Ii0ppvkVOdDalpveXaV.', '1P2s8EZeCS4EZda5PhqvEZObmjc5OggSsTlpKyI5eKholImuKGh0SwOhRBuB', NULL, '2018-12-02 01:56:19', '2018-12-02 01:56:19', 1),
(23, 2, 'NZ European', 'test', '$2y$10$hgWsVCY.T1UtosE7DB9Ue.Drfu.zc0aCoEPPQLf2ZZnRD67ZWk8LW', 'test@user.com', '', 0, 0, 'users/default.png', 'test', 'test', NULL, '$2y$10$tpc/cmMydms3h253Irv.iu5WLzb4GJz6omoLLzmi1LteMWmMsVaPm', '8bEgXLFYmtjmzDmF5EdlbpYeIJw1wZI5k4Xi7XkLQq8l0YntgInOhynwWxV9', NULL, '2018-12-02 01:58:10', '2018-12-02 01:58:10', 1),
(24, 2, 'NZ European', 'password', '$2y$10$CAFMjsU5h9H23YVsNs1YF.7vhZn1ohachT6/odod6CM3XqSRWRxMa', 'password@password.com', '', 0, 0, 'users/default.png', 'asdsad', '12321', NULL, '$2y$10$OXqxwOrCs/mMBSC3q0/vguZwjTq49Os4X91APXtBgN0tBwXws8nlu', 'WRJ3qARiQxtcORQLNEw4Yg5xXT29jP7zMPs9H050XL3Rj1nqKN1ZpvAo3OBG', NULL, '2018-12-02 02:00:23', '2018-12-02 02:00:23', 1),
(25, 2, 'NZ European', 'dddd', '$2y$10$sEzb5SZjhgq9ZYd0LATwuerIJc2Sh.KXM8l7ECCpbQ2yTK8FLSL8u', 'dddddddd@dddddddd.com', '', 0, 0, 'users/default.png', 'adsaasd', '123123', NULL, '$2y$10$3X.luujdSksAHgC5fgCSfuK9rPdQwCxMKPq4Pv6rnxmaGtoRcQOgK', 'KKFjXmxipkJAkLyEuqkjvGRciclBr2wMctjarO2BnYz52PxvCr2bAO0sJGiN', NULL, '2018-12-02 02:02:35', '2018-12-02 02:02:35', 1),
(26, 2, 'NZ European', 'xxxx', '$2y$10$JQRWLokQVLuN0uo86oXtKuhZg/VxzI8rJyX39yaKHdPCig2VaIHrG', 'xxx@xxx.com', '', 0, 0, 'users/default.png', 'sadsaasd', '123', NULL, '$2y$10$gcIqJkO7RebNQ.jqtGqUgug2yiNQaoh3O3S9YPpqkeQQi7zy5X96i', 'flpYWlTS0wxP1dTXTLHffyfwFswHM7grX87ngDCSwI1dXiIuqaOtBUT8yCDI', NULL, '2018-12-02 02:04:49', '2018-12-02 02:04:49', 1),
(27, 2, 'NZ European', 'passwordx', '$2y$10$DYyGtl8ycPUBQj8Q3I9rsewXte9YK7ktNSb13ACC7jDwsVto62sv2', 'passx@pass.com', '', 0, 0, 'users/default.png', 'asdasdads', '133122', NULL, '$2y$10$8GPqtlOoClXgmpxpb2C5vO94/5fGZ.QDX4mMu0Kbo4V1WCUhGdNwa', 'WUdQBKAdzv6FOYujdqgplfUrfKkgtelFJwRVEeMlrjQxt0gQ8e8GXQbJMFK0', NULL, '2018-12-02 02:05:43', '2018-12-02 02:05:43', 1),
(28, 2, 'NZ European', 'danielox', '$2y$10$j33J2iX4EcB6z.S9IqVgsu8FIsl3BhbF3T7pWyKGuZ1oUrpPbQDXK', 'danialmaluk7@gmail.com', '', 0, 0, 'users/default.png', 'sdassda', '1321', NULL, '$2y$10$8hdHuk7.WnSXqQskElqsAu9tM2plo0tJiV6dFDKa1H92yzDWCgwBu', 'IjIRzE7bp9HSuImzthZ5fvTrvBz4eMxMgPOM28emlahaeSOzz0xx576lDcbK', NULL, '2018-12-02 02:50:05', '2018-12-02 02:50:05', 1),
(29, 2, 'NZ European', 'faizan', '$2y$10$gNczvPl8APViPMSE0NUHEOg5uS.aS.wkwAI.jD6bM/MMZYEUDxWFm', 'faizikhan1626@gmail.com', '', 0, 0, 'users/default.png', '32123312', '21312313', NULL, '$2y$10$z2tEYOwHGwYj2xOzHF4T3uWZXd.OMjmhm2YOyqhfVMmh9/IyFsCou', 'iQCAGPxOjCDDWQ5LcgmsZLnHkXytTfFypEnoPYO8ZIhIaIGSIIouw3Cl1Q7W', NULL, '2018-12-02 02:53:07', '2018-12-02 02:53:07', 1),
(30, 2, 'NZ European', 'danielx', '$2y$10$KwBL/vfPjfJYQbqQ8UCVqeViIWXnr2iMnhmdBds6dX5m.2u3AgYWm', 'xxx@xxxz.com', 'W', 0, 0, 'users/default.png', 'sdsaads', 'ddsa', NULL, '$2y$10$7PH0O2WwiWJIFYACpPGwsO76a2sIeElQLfVFhJ3tRZdAV2eSQ004K', '4TWc2s5oNvkoDtvdZZ6k5GmfyBivjmp3MqKFY4MjS8b9FOMyVTMNdE0YBy9P', NULL, '2018-12-02 03:55:09', '2018-12-02 03:55:09', 1),
(31, 2, 'NZ European', 'danial', '$2y$10$xr2B8A2MRO4TX.Im21o1GOEGWsBDdM9NOPO81/KgE8fH4TvjeYvQa', 'xxxxxxxx@cxcc.com', 'U', 0, 0, 'users/default.png', 'adsdsad', '213', NULL, '$2y$10$JxSrvU.X9/ecEUJMZ8nsGOFYqrhL8HMVFkkdYSXT31Hd7sudV3fgi', 'O8xJEsn7cMA18VzbQvU1JGOHIs7lVInx3bzhSvjiyo0g98xfGsOFpGvhVGYR', NULL, '2018-12-02 04:03:52', '2018-12-02 04:03:52', 1),
(32, 2, 'NZ European', 'dasdads', '$2y$10$zP3kCpL5cbp/4ajW/Q.QZu3h6qyXQE6ftCbGUCRqCESVSplCXD0eK', 'xzaw@xz.com', 'vApNVw6u5Hp14Wh1ILalTCrB6lzcPpCeM7hfnYPX', 0, 0, 'users/default.png', 'sdadas', '23123', NULL, '$2y$10$8ZYdpvbijQqRTVhmw9M94uQzQW4yDxXvufAYkr8Ovv.zrFsXlX0Hy', 'izrOOR5WxuqNry9Wcj91GV8OEYuXDaHP5Kd9Z4iyXBWWK7luem1LJUWxJ60I', NULL, '2018-12-02 04:05:00', '2018-12-02 04:05:01', 1),
(33, 2, 'NZ European', 'sadaadds', '$2y$10$RMjWTDICtmeaCEjBvrXM1O4naWPzIvOd6DEpLWN92O.pxKwdHwMVK', 'dasasdssasadda@dasdasdas.com', '5BtL4QahEBp9PzLDelO7b5HwwNYYrFZelTL0syLV', 0, 0, 'users/default.png', 'asdads', 'dsad', NULL, '$2y$10$P5k9GbbhWyv6B28Wxqc2kO5cpV2VnjG.lbtIMWc7ZfsnXv2PiztcS', 'rLwm7KoaFDukfT84Z1Lz7evvSmLAFvaoOoeTGmO91J7a9HEQcZ2JIXbbhkdN', NULL, '2018-12-02 04:13:41', '2018-12-02 04:13:41', 1),
(34, 1, 'NZ European', 'daniel', NULL, 'admin@admin.com', NULL, 0, 0, 'users/default.png', NULL, NULL, NULL, '$2y$10$I8nqNor6pypJmAeiTY4oMOgzkdu2OEZKqgNy5OgejO2jtPrVuIm7K', 'yipwRvGU0JWll8jHE4AvRDCxua6agtFeVDlGNB0zzlXPB2vFxoIZdQY0xc2v', NULL, '2019-01-04 08:10:19', '2019-01-04 08:10:19', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `ethnic`
--
ALTER TABLE `ethnic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `footer_cms`
--
ALTER TABLE `footer_cms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `frontend_cms`
--
ALTER TABLE `frontend_cms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_timeslot`
--
ALTER TABLE `general_timeslot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders_info`
--
ALTER TABLE `orders_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `portion`
--
ALTER TABLE `portion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `portions`
--
ALTER TABLE `portions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product-category`
--
ALTER TABLE `product-category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `referal`
--
ALTER TABLE `referal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `SellingUnits`
--
ALTER TABLE `SellingUnits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_type`
--
ALTER TABLE `service_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_type_options`
--
ALTER TABLE `service_type_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timeslots`
--
ALTER TABLE `timeslots`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ethnic`
--
ALTER TABLE `ethnic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `footer_cms`
--
ALTER TABLE `footer_cms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `frontend_cms`
--
ALTER TABLE `frontend_cms`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `general_timeslot`
--
ALTER TABLE `general_timeslot`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `orders_info`
--
ALTER TABLE `orders_info`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `portion`
--
ALTER TABLE `portion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `portions`
--
ALTER TABLE `portions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product-category`
--
ALTER TABLE `product-category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `referal`
--
ALTER TABLE `referal`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `SellingUnits`
--
ALTER TABLE `SellingUnits`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `service_type`
--
ALTER TABLE `service_type`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `service_type_options`
--
ALTER TABLE `service_type_options`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `timeslots`
--
ALTER TABLE `timeslots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
